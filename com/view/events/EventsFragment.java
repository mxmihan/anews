package anews.com.views.events;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;

import anews.com.R;
import anews.com.analytic.Analytics;
import anews.com.interfaces.OnHelpClickListener;
import anews.com.interfaces.OnPostDataClickListener;
import anews.com.model.events.EventsInfo;
import anews.com.model.events.dto.EventPushData;
import anews.com.model.news.dto.PostData;
import anews.com.model.posts.PostDataLoaderInfo;
import anews.com.network.ModelBase;
import anews.com.network.ModelData;
import anews.com.network.ModelError;
import anews.com.preferences.GlobalPreferences;
import anews.com.utils.AppStateFragment;
import anews.com.utils.AppUtils;
import anews.com.utils.adapters.AbsSwipeableVH;
import anews.com.utils.adapters.SwipeableItemTouchCallback;
import anews.com.utils.ui.SpacesItemDecoration;
import anews.com.views.events.adapters.EventsCursorAdapter;
import anews.com.views.events.adapters.EventsEmptyVHItem;
import anews.com.views.main.MainActivity;
import anews.com.views.news.FullNewsActivity;

public class EventsFragment extends AppStateFragment implements SwipeRefreshLayout.OnRefreshListener, OnHelpClickListener, OnPostDataClickListener, SwipeableItemTouchCallback.RecyclerItemTouchHelperListener {
    public static final String TAG = EventsFragment.class.getSimpleName();
    public static final String EVENT_BROADCAST_MESSAGE = "anews.com.NEW_PUSH";
    private EventsInfo mEventsInfo = getModel().getEventsInfo();
    private PostDataLoaderInfo mPostDataLoaderInfo = getModel().getPostDataLoaderInfo();
    private RecyclerView mRecyclerVertical;
    private EventsCursorAdapter mEventsAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout mEmptyView;
    private View mProgressBar;

    private BroadcastReceiver mEventsBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, android.content.Intent intent) {
            onRefresh();
        }
    };

    public static EventsFragment newInstance() {
        Bundle args = new Bundle();
        EventsFragment fragment = new EventsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_events, container, false);
        mRecyclerVertical = view.findViewById(R.id.rv_events);
        mProgressBar = view.findViewById(R.id.progress_bar_horizontal);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mEmptyView = view.findViewById(R.id.empty_view);
        mEmptyView.setVisibility(View.GONE);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setEnabled(false);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.black);
        mEventsAdapter = mEventsInfo.getPreparedEventsCursorAdapter();
        mEventsAdapter.setOnHelpListener(this);
        mEventsAdapter.setOnPostClickListener(this);
        mRecyclerVertical.setLayoutManager(linearLayoutManager);
        mRecyclerVertical.addOnScrollListener(mRecyclerScrollListener);
        mRecyclerVertical.setAdapter(mEventsAdapter);

        ItemTouchHelper touchHelper = new ItemTouchHelper(new SwipeableItemTouchCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, this));
        touchHelper.attachToRecyclerView(mRecyclerVertical);
        mRecyclerVertical.addItemDecoration(new SpacesItemDecoration(AppUtils.dpToPx(8)));
        setHasOptionsMenu(true);
        updateView();
        return view;
    }

    @Override
    public void onPostClicked(PostData postData) {
        Analytics.trackEvent(getContext(), Analytics.NEWS_CLICK, Analytics.CATEGORY_NEWS_PREVIEW, Analytics.ACTION_CLICK, postData.getLinkForTack(),
                Analytics.buildParamsForPostSelected(getContext(), postData));
        Intent intent = new Intent(getActivity(), FullNewsActivity.class);
        intent.putExtra(FullNewsActivity.EXTRA_EVENTS_POSTS_VIEW, true);
        intent.putExtra(FullNewsActivity.EXTRA_ANNOUNCE_ID, postData.getId());
        getActivity().startActivity(intent);
    }

    @Override
    public void saveState() {
        Bundle bundle = new Bundle();
        setSaveState(bundle);
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mEventsBroadcastReceiver, new IntentFilter(EVENT_BROADCAST_MESSAGE));
        registerModelListener(mEventsInfo, mEventsListener);
        registerModelListener(mPostDataLoaderInfo, mPostLoaderListener);
        super.onResume();
        setActionBarTitle(R.string.str_sm_events);
        if (getActivity() != null && getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).disableAlphaToolbar(true);
            GlobalPreferences.getInstance().setUpdateAdapter(false);

            Analytics.openPage(Analytics.OPEN_SCREEN_EVETNS);
        }

        @Override
        public void onPause () {
            super.onPause();
            mEventsInfo.removeListener(mEventsListener, true);
            mPostDataLoaderInfo.removeListener(mPostLoaderListener, false);
            mSwipeRefreshLayout.setRefreshing(false);
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mEventsBroadcastReceiver);
        }

        @Override
        public void onRefresh () {
            mEventsInfo.updateEventsAdapter(mEventsAdapter);
        }

        private ModelBase.Listener mEventsListener = new ModelBase.Listener<ArrayList<PostData>, Void>() {

            @Override
            public void onStateChanged(ModelData<ArrayList<PostData>, Void> data) {
            }

            @Override
            public void onError(@NonNull ModelError error) {
            }
        };

        @Override
        public void onHideHelp () {
            GlobalPreferences.getInstance().removeEventHelp();
            mEventsAdapter.notifyItemRemoved(0);
            updateView();
        }

        @Override
        public void updateView () {
            mEmptyView.setVisibility(mEventsAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
            mRecyclerVertical.setVisibility(mEventsAdapter.getItemCount() != 0 ? View.VISIBLE : View.GONE);
        }


        @Override
        public void onCreateOptionsMenu (Menu menu, MenuInflater inflater){
            inflater.inflate(R.menu.menu_events_item, menu);
            super.onCreateOptionsMenu(menu, inflater);
        }

        @Override
        public boolean onOptionsItemSelected (MenuItem item){
            switch (item.getItemId()) {
                case R.id.events_clear:
                    clearEvents();
                    return true;
                default:
                    break;
            }
            return false;
        }

        public void clearEvents () {
            Analytics.trackEvent(getContext(), Analytics.ACTION_CLEAN_EVENTS, Analytics.CATEGORY_ACTIVITY, Analytics.ACTION_CLEAN_EVENTS, "all");
            updateView();
        }

        private Handler mHandler = new Handler();

        private Runnable mRunnable = new Runnable() {
            @Override
            public void run() {
                if (mEventsAdapter != null) {
                    SparseArray<EventsEmptyVHItem> vh = mEventsAdapter.getVisibleVHItems();
                    for (int i = 0; i < vh.size(); i++) {
                        mPostDataLoaderInfo.addToQueue(vh.valueAt(i).getData().getPostId());
                    }

                }
            }
        };

        private RecyclerView.OnScrollListener mRecyclerScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                mHandler.removeCallbacks(mRunnable);
                mHandler.postDelayed(mRunnable, 150);
            }

        };

        private ModelBase.Listener mPostLoaderListener = new ModelBase.Listener<PostData, Void>() {

            @Override
            public void onStateChanged(ModelData<PostData, Void> data) {
                if (mPostDataLoaderInfo.isUpdating()) {
                    mProgressBar.setVisibility(View.VISIBLE);
                } else if (mPostDataLoaderInfo.isNext()) {
                    mEventsInfo.updateEventsAdapter(mEventsAdapter);
                } else if (mPostDataLoaderInfo.isCompleted()) {
                    mProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(@NonNull ModelError error) {
                mProgressBar.setVisibility(View.GONE);
            }
        };

        @Override
        public void onSwiped (RecyclerView.ViewHolder viewHolder,int direction, int position){
            if (viewHolder instanceof AbsSwipeableVH) {
                EventPushData postData = (EventPushData) ((AbsSwipeableVH) viewHolder).getData();
                mEventsInfo.removeEvent(postData);
                mEventsInfo.updateEventsAdapter(mEventsAdapter);
                updateView();
                Analytics.trackEvent(getContext(), Analytics.ACTION_CLEAN_EVENTS, Analytics.CATEGORY_ACTIVITY, Analytics.ACTION_REMOVE_EVENTS, postData.getLinkForTack());

            }
        }

        @Override
        public void startSwipe () {

        }

        @Override
        public void stopSwipe () {

        }
    }