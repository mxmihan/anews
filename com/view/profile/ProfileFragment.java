package anews.com.views.profile;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;

import java.io.File;
import java.io.IOException;

import anews.com.App;
import anews.com.R;
import anews.com.analytic.Analytics;
import anews.com.interfaces.OnProfileAdapterClickListener;
import anews.com.model.DBHelperFactory;
import anews.com.model.profile.ChangeUserAvatarInfo;
import anews.com.model.profile.dto.UserProfileData;
import anews.com.network.ModelBase;
import anews.com.network.ModelData;
import anews.com.network.ModelError;
import anews.com.preferences.GlobalPreferences;
import anews.com.preferences.ProfilePreferences;
import anews.com.utils.AppStateFragment;
import anews.com.utils.AppUtils;
import anews.com.views.dialogs.LogOutDialogFragment;
import anews.com.views.main.MainActivity;
import anews.com.views.profile.adapters.ProfileAdapter;
import anews.com.views.profile.adapters.ProfileAdapterItem;
import anews.com.views.splash.SplashActivity;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends AppStateFragment implements View.OnClickListener, LogOutDialogFragment.LogOutDialogListener, OnProfileAdapterClickListener {
    public static final String TAG = ProfileFragment.class.getSimpleName();
    private static final int REQUEST_CODE_CAMERA = 1678;
    private static final int REQUEST_CODE_GALLERY = 1680;
    public static final String SAVED_IS_URI = "is_uri";
    public static final String SAVED_IS_UPDATE = "is_update";
    public static final String SAVED_IMAGE_FILE_PATH = "image_file_path";
    private static final String PROVIDER = "anews.com.android.provider";
    private ProfileAdapter mProfileAdapter;
    private View mProgressBar;
    private RecyclerView mRecyclerViewProfile;
    private ChangeUserAvatarInfo mChangeUserAvatarInfo;
    private boolean isUri;
    private String mImageFilePath;
    public static final String[] CAMERA_AND_WRITE_STORAGE_PERMISSION = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    public static final String[] READ_AND_WRITE_STORAGE_PERMISSION = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public static ProfileFragment newInstance() {
        Bundle args = new Bundle();
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        setActionBarTitle(R.string.str_fragment_title_profile);

        if (mChangeUserAvatarInfo == null) {
            mChangeUserAvatarInfo = getModel().getChangeUserAvatarInfo();
        }
        mProgressBar = view.findViewById(R.id.progress_bar_horizontal);
        mRecyclerViewProfile = view.findViewById(R.id.recycler_profile);
        LinearLayoutManager manager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerViewProfile.setLayoutManager(manager);
        mProfileAdapter = new ProfileAdapter(this);
        mRecyclerViewProfile.setAdapter(mProfileAdapter);
        return view;
    }

    @Override
    public void onResume() {
        registerModelListener(mChangeUserAvatarInfo, mChangeUserAvatarListener);
        super.onResume();
        updateView();

    }

    @Override
    public void updateView() {
        if (!mChangeUserAvatarInfo.isUpdating()) {
            mProfileAdapter.updateHeader();
            mProfileAdapter.notifyDataSetChanged();
        }
        mProgressBar.setVisibility(!mChangeUserAvatarInfo.isUpdating() ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onPause() {
        super.onPause();
        mChangeUserAvatarInfo.removeListener(mChangeUserAvatarListener, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.container_create_photo:
                openCameraIntent();
                break;
            case R.id.container_load_from_gallery:
                openGalleryIntent();
                break;
        }
    }


    private void openCameraIntent() {
        if (AppUtils.isGetCameraPermissionGranted(getActivity())
                && AppUtils.isGetWriteStoragePermissionGranted(getActivity())) {
            try {
                Intent imageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (imageIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = AppUtils.createImageFile(getContext());
                        mImageFilePath = photoFile.getAbsolutePath();
                    } catch (IOException ignored) {

                    }

                    if (photoFile != null) {
                        Uri photoURI = FileProvider.getUriForFile(getContext(), PROVIDER, photoFile);
                        imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(imageIntent, REQUEST_CODE_CAMERA);
                    }
                }
            } catch (ActivityNotFoundException e) {
                getApp().showToast(getString(R.string.str_dont_camera), false);
            }
        } else {
            requestPermissions(CAMERA_AND_WRITE_STORAGE_PERMISSION, REQUEST_CODE_CAMERA);
        }
    }

    private void openGalleryIntent() {
        if (AppUtils.isGetWriteStoragePermissionGranted(getActivity()) && AppUtils.isGetReadStoragePermissionGranted(getActivity())) {
            Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
            openGalleryIntent.setType("image/*");
            openGalleryIntent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(openGalleryIntent, REQUEST_CODE_GALLERY);
        } else {
            requestPermissions(READ_AND_WRITE_STORAGE_PERMISSION, REQUEST_CODE_GALLERY);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE_CAMERA:
                if (AppUtils.isGetCameraPermissionGranted(getActivity())
                        && AppUtils.isGetWriteStoragePermissionGranted(getActivity())) {
                    openCameraIntent();
                }
                break;
            case REQUEST_CODE_GALLERY:
                if (AppUtils.isGetWriteStoragePermissionGranted(getActivity())
                        && AppUtils.isGetReadStoragePermissionGranted(getActivity())) {
                    openGalleryIntent();
                }
                break;
        }
    }

    private void signOut() {
        LogOutDialogFragment.newInstance().show(getChildFragmentManager(), LogOutDialogFragment.TAG);
    }

    @Override
    public void logout() {
        Analytics.trackEvent(getContext(), Analytics.ACTION_LOGOUT, Analytics.CATEGORY_ACTIVITY, Analytics.ACTION_LOGOUT);
        new SharedPrefsCookiePersistor(App.getInstance()).clear();
        ProfilePreferences.getInstance().wipeAllSavedData();
        GlobalPreferences.getInstance().wipeAllSavedData();
        App.getInstance().initRetrofit();
        DBHelperFactory.getHelper().clearDB();
        startActivityForResult(new Intent(getContext(), MainActivity.class), MainActivity.REQUEST_CODE_FINISH_ACTIVITY);
        startActivity(new Intent(getContext(), SplashActivity.class));
    }


    @Override
    public void OnProfileAdapterClickItem(ProfileAdapterItem item) {
        switch (item.getType()) {
            case HEADER_ITEM:
                showBottomSheetDialogFragment();
                break;
            case BUTTON_ITEM:
                switch (item.getTitleResId()) {
                    case R.string.str_edit_avatar:
                        showBottomSheetDialogFragment();
                        break;
                    case R.string.str_edit_user_name:
                        ((ProfileActivity) getActivity()).showFragmentByTag(ProfileNameFragment.TAG);
                        break;
                    case R.string.str_edit_password:
                        ((ProfileActivity) getActivity()).showFragmentByTag(ProfilePasswordFragment.TAG);
                        break;
                    case R.string.str_logout_account:
                        signOut();
                        break;
                }
        }
    }

    private void showBottomSheetDialogFragment() {
        BottomDialogFragment bottomSheetDialog = BottomDialogFragment.getInstance();
        bottomSheetDialog.show(getChildFragmentManager(), BottomDialogFragment.TAG);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_CODE_CAMERA) {
                isUri = false;
            }
            if (requestCode == REQUEST_CODE_GALLERY) {
                isUri = true;
                mImageFilePath = data.getData().toString();
            }
            Bundle args = new Bundle();
            args.putBoolean(ProfileFragment.SAVED_IS_URI, isUri);
            args.putString(ProfileFragment.SAVED_IMAGE_FILE_PATH, mImageFilePath);
            ((ProfileActivity) getActivity()).showFragmentByTag(CorpImageFragment.TAG, args);
        }
    }

    @Override
    public void saveState() {

    }


    private ModelBase.Listener mChangeUserAvatarListener = new ModelBase.Listener<UserProfileData, Void>() {
        @Override
        public void onStateChanged(ModelData<UserProfileData, Void> data) {
            updateView();
            if (!mChangeUserAvatarInfo.isUpdating()) {
                if (data != null && data.getData() != null) {
                    App.getInstance().showToast(R.string.str_change_name_successful, true);
                }

            }
        }

        @Override
        public void onError(@NonNull ModelError error) {
            updateView();
            switch (error) {

                case BadNetworkConnection:
                    getApp().showToast(getString(R.string.str_connection_error), false);
                default:
                    getApp().showToast(getString(R.string.error_unknown), false);
                    break;
            }
        }
    };

}

