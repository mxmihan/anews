package anews.com.model.profile;

import java.util.HashMap;

import anews.com.App;
import anews.com.analytic.Analytics;
import anews.com.model.profile.dto.UserProfileData;
import anews.com.network.ModelError;
import anews.com.network.SimpleModel;
import anews.com.preferences.ProfilePreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateAccountInfo extends SimpleModel<UserProfileData, Void> {


    public void createNewAccount(String firstName, String email, String password) {
        startNewRequest();
        HashMap<String, String> params = new HashMap<>();
        params.put("first_name", firstName);
        params.put("email", email);
        params.put("password", password);
        getRestApi().registerNewAccount(params).enqueue(new Callback<UserProfileData>() {
            @Override
            public void onResponse(Call<UserProfileData> call, Response<UserProfileData> response) {
                if (response.isSuccessful() && response.body() != null) {
                    UserProfileData profileData = response.body();
                    if (profileData.getCode() == null) {
                        App.getInstance().initRetrofit();
                        ProfilePreferences.getInstance().saveUserProfileData(response.body());
                        ProfilePreferences.getInstance().setIsNeedFirstSubscribe(true);
                        setData(response.body());
                        Analytics.trackEvent(App.getInstance(), Analytics.ACTION_AUTH_CREATE_NEW_ACCOUNT, Analytics.CATEGORY_ACTIVITY, Analytics.ACTION_AUTH_CREATE_NEW_ACCOUNT);
                    } else if (profileData.getCode() == 105) {
                        setError(ModelError.ExistingEmail);
                    } else if (profileData.getCode() == 109) {
                        setError(ModelError.CheckEmail);
                    } else if (profileData.getCode() == 998) {
                        setError(ModelError.IncorrectParams);
                    } else {
                        setError(ModelError.Unknown);
                    }
                } else {
                    setError(ModelError.Unknown);
                }
            }

            @Override
            public void onFailure(Call<UserProfileData> call, Throwable t) {
                setError(ModelError.Unknown);
            }
        });
    }
}
