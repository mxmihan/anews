package anews.com.preferences;

import android.content.Context;

import anews.com.model.preferences.dto.FontsSize;
import anews.com.model.preferences.dto.NewsReadMode;

public class GlobalPreferences extends AbsPreferences {
    private static final String GLOBAL_PREFERENCES_FILE = "global_preferences";
    //    private static final String USER_REGION = "user_region";
    private static final String GLOBAL_LAST_TIME_CLEAR_CACHE = "last_time_clear_cache";
//    private static final String USER_READ_MODE = "read_mode";
    private static final String USER_IS_COMPLETE_SELECTED_CATEGORY = "is_complete_selected_category";
    private static final String USER_IS_COMPLETE_FIRST_SHOW_SPLASH = "is_complete_first_show_splash";
    private static final String USER_IS_SHOW_SUBS_HELP = "is_show_subs_help";
    private static final String USER_IS_EVENT_HELP_REMOVED = "is_event_help_removed";
    private static final String USER_IS_BOOKMARKS_HELP_REMOVED = "is_bookmarks_help_removed";
    private static final String USER_IS_ANNOUNCES_SUBS_REMOVED = "is_announces_subs_removed";
    private static final String IS_NEED_UPDATE_BOOKMARKS = "is_need_update_bookmarks";
    private static final String IS_ANNOUNCES_ITEMS_CHANGES = "is_announces_items_changes";
    public static final String USER_IS_NEED_UPDATE = "user_is_need_update_font_size";

    private volatile static GlobalPreferences mInstance;

    private GlobalPreferences(Context context) {
        super(context, GLOBAL_PREFERENCES_FILE);
    }

    public static void initialize(Context ctx) {
        if (mInstance == null) {
            synchronized (GlobalPreferences.class) {
                if (mInstance == null) {
                    mInstance = new GlobalPreferences(ctx);
                }
            }
        }
    }

    public static GlobalPreferences getInstance() {
        if (mInstance == null) {
            throw new IllegalStateException("Must Initialize GlobalPreferences before using getInstance()");
        } else {
            return mInstance;
        }
    }


    public boolean isNeedUpdate() {
        return getBooleanPrefs(USER_IS_NEED_UPDATE);
    }

    public void setUpdate(boolean update) {
        saveBooleanPrefs(USER_IS_NEED_UPDATE, update);
    }

//    public void saveUserReadMode(NewsReadMode newsReadMode) {
//        saveStringPrefs(USER_READ_MODE, newsReadMode.toString());
//    }

//    public boolean isNormalReadMode() {
//        return NewsReadMode.getTypeMode(getStringPrefs(USER_READ_MODE)) == NewsReadMode.NORMAL;
//    }

    public void categorySelected() {
        saveBooleanPrefs(USER_IS_COMPLETE_SELECTED_CATEGORY, true);
    }

    public boolean isCategorySelected() {
        return getBooleanPrefs(USER_IS_COMPLETE_SELECTED_CATEGORY);
    }

    public void setCompleteShowedSplash() {
        saveBooleanPrefs(USER_IS_COMPLETE_FIRST_SHOW_SPLASH, true);
    }

    public boolean isSplashShowed() {
        return getBooleanPrefs(USER_IS_COMPLETE_FIRST_SHOW_SPLASH);
    }

    public long getLastTimeClearCache() {
        return getLongPrefs(GLOBAL_LAST_TIME_CLEAR_CACHE);
    }

    public void setLastTimeClearCache(long timeInMillis) {
        saveLongPrefs(GLOBAL_LAST_TIME_CLEAR_CACHE, timeInMillis);
    }

    public boolean isNeedShowSubsHelp() {
        return getBooleanPrefs(USER_IS_SHOW_SUBS_HELP);
    }

    public void showSubsHelp() {
        saveBooleanPrefs(USER_IS_SHOW_SUBS_HELP, true);
    }

    public void removeSubsHelp() {
        saveBooleanPrefs(USER_IS_SHOW_SUBS_HELP, false);
    }

    public boolean isEventHelpRemoved() {
        return getBooleanPrefs(USER_IS_EVENT_HELP_REMOVED);
    }

    public void removeEventHelp() {
        saveBooleanPrefs(USER_IS_EVENT_HELP_REMOVED, true);
    }

    public boolean isBookmarksHelpRemoved() {
        return getBooleanPrefs(USER_IS_BOOKMARKS_HELP_REMOVED);
    }

    public void removeBookmarksHelp() {
        saveBooleanPrefs(USER_IS_BOOKMARKS_HELP_REMOVED, true);
    }

//    public boolean isAnnouncesSubsHelpRemoved() {
//        return getBooleanPrefs(USER_IS_ANNOUNCES_SUBS_REMOVED);
//    }
//
//    public void removeAnnouncesSubsHelp() {
//        saveBooleanPrefs(USER_IS_ANNOUNCES_SUBS_REMOVED, true);
//    }

//    public boolean isNeedUpdateBookmarks() {
//        return getBooleanPrefs(IS_NEED_UPDATE_BOOKMARKS);
//    }

//    public void setIsNeedUpdateBookmarks(boolean isNeedUpdate) {
//        saveBooleanPrefs(IS_NEED_UPDATE_BOOKMARKS, isNeedUpdate);
//    }

    public boolean isAnnouncesItemsChanges() {
        return getBooleanPrefs(IS_ANNOUNCES_ITEMS_CHANGES);
    }

    public void setIsAnnouncesItemsChanges(boolean isNeedUpdate) {
        saveBooleanPrefs(IS_ANNOUNCES_ITEMS_CHANGES, isNeedUpdate);
    }
}
