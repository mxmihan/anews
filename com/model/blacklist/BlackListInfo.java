package anews.com.model.blacklist;


import com.j256.ormlite.stmt.PreparedQuery;

import java.util.ArrayList;
import java.util.List;

import anews.com.App;
import anews.com.analytic.Analytics;
import anews.com.model.DBHelperFactory;
import anews.com.model.news.dto.FeedData;
import anews.com.model.blacklist.dto.BlackListData;
import anews.com.model.blacklist.dto.BlackListSyncState;
import anews.com.network.RestApi;
import anews.com.network.SimpleModel;
import anews.com.utils.AppUtils;
import anews.com.views.settings.adapters.vertical.BlackListCursorAdapter;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Function4;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class BlackListInfo extends SimpleModel<Object, Void> {

    private Disposable mDeleteObserver;
    private Disposable mAddObserver;
    private Disposable mFullSyncObserver;

    public BlackListCursorAdapter getPreparedBlackListCursorAdapter() {
        PreparedQuery<BlackListData> preparedQuery = DBHelperFactory.getHelper().getBlackListDao().getBlackListPreparedQuery();
        return new BlackListCursorAdapter(DBHelperFactory.getHelper().getBlackListDao().getBlackListCursor(preparedQuery), preparedQuery);
    }

    public void updateBlackListAdapter(BlackListCursorAdapter adapter) {
        PreparedQuery<BlackListData> preparedQuery = DBHelperFactory.getHelper().getBlackListDao().getBlackListPreparedQuery();
        adapter.changeCursor(DBHelperFactory.getHelper().getBlackListDao().getBlackListCursor(preparedQuery), preparedQuery);
    }

    public void getBlackList() {
        if (AppUtils.isAuthorized()) {
            startFullSync();
        } else {
            stopRequest();
        }
    }
    private void startFullSync() {
        if (mFullSyncObserver != null && !mFullSyncObserver.isDisposed()) {
            mFullSyncObserver.dispose();
            mFullSyncObserver = null;
        }
        startNewRequest();
        Observable.zip(getBlackListOnServerObserver(), getDeleteBlackListObserver(), getAddBlackListObserver(), getFeedByIdOnServerObserver(),
                new Function4<Boolean, Boolean, Boolean, Boolean, Boolean>() {
                    @Override
                    public Boolean apply(Boolean aBoolean, Boolean aBoolean2, Boolean aBoolean3, Boolean aBoolean4) {
                        if (aBoolean && aBoolean2 && aBoolean3) {
                            DBHelperFactory.getHelper().getBlackListDao().deleteMarkedBlackList();
                            return true;
                        }
                        return false;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mFullSyncObserver = d;

                    }

                    @Override
                    public void onNext(Boolean topBlackList) {
                        setOnNextData(null);
                    }

                    @Override
                    public void onError(Throwable e) {
                        setError(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void addBlackList(int feedId, FeedData feedData) {
        DBHelperFactory.getHelper().getBlackListDao().addNewBlackList(feedId, feedData);
        Analytics.trackEvent(App.getInstance(), Analytics.FEED_ADDED_TO_BLACKLIST, Analytics.CATEGORY_ACTIVITY,
                Analytics.ACTION_ADD_TO_BLACKLIST, Analytics.LABEL_FEED + feedId);
        if (AppUtils.isAuthorized()) {
            addBlackListOnServer();
        }

    }

    public void deleteBlackList(int feedId) {
        boolean isAuthorized = AppUtils.isAuthorized();
        DBHelperFactory.getHelper().getBlackListDao().delleteFromBlackList(feedId, isAuthorized);
        Analytics.trackEvent(App.getInstance(), Analytics.REMOVE_FROM_BLACKLIST, Analytics.CATEGORY_ACTIVITY,
                Analytics.ACTION_REMOVE_FROM_BLACKLIST, Analytics.LABEL_FEED + feedId);
        if (isAuthorized) {
            deleteBlackListOnServer();
        }
    }



    private void deleteBlackListOnServer() {
        if (mDeleteObserver != null && !mDeleteObserver.isDisposed()) {
            mDeleteObserver.dispose();
            mDeleteObserver = null;
        }
        Observable.empty()
                .concatWith(getDeleteBlackListObserver())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Object>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mDeleteObserver = d;
                    }

                    @Override
                    public void onNext(Object o) {
                        setData(o);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    private void addBlackListOnServer() {
        if (mAddObserver != null && !mAddObserver.isDisposed()) {
            mAddObserver.dispose();
            mAddObserver = null;
        }
        Observable.empty()
                .concatWith(getAddBlackListObserver())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Object>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mAddObserver = d;
                    }

                    @Override
                    public void onNext(Object o) {
                        setData(o);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    private Observable<Boolean> getAddBlackListObserver() {
        return Observable.just(getRestApi())
                .map(new Function<RestApi, Boolean>() {
                    @Override
                    public Boolean apply(RestApi api) throws Exception {
                        List<BlackListData> blackListDataList = DBHelperFactory.getHelper().getBlackListDao().getNewBlackListForAdd();
                        for (BlackListData blackListData : blackListDataList) {
                            Response<BlackListData> response = api.addBlackList(blackListData.getId()).execute();
                            if (blackListData.getSyncState() == BlackListSyncState.NEW) {
                                if (response.isSuccessful()) {
                                    blackListData.setSyncState(BlackListSyncState.SYNCHRONIZED);
                                    blackListData.update();
                                } else {
                                    return false;
                                }
                            }
                        }
                        return true;
                    }
                });

    }

    private Observable<Boolean> getDeleteBlackListObserver() {
        return Observable.just(getRestApi())
                .map(new Function<RestApi, Boolean>() {
                    @Override
                    public Boolean apply(RestApi api) throws Exception {
                        List<BlackListData> blackListDataList = DBHelperFactory.getHelper().getBlackListDao().getBlackListForDelete();
                        for (BlackListData blackListData : blackListDataList) {
                            if (blackListData.getSyncState() == BlackListSyncState.DELETE) {
                                Response<Void> response = api.deleteBlackList(blackListData.getId()).execute();
                                if (response.isSuccessful()) {
                                    blackListData.setSyncState(BlackListSyncState.DELETED);
                                    blackListData.setSyncTimeStamp(System.currentTimeMillis());
                                    blackListData.update();
                                } else {
                                    return false;
                                }
                            }
                        }
                        return true;
                    }
                });

    }

    private Observable<Boolean> getBlackListOnServerObserver() {
        return Observable.just(getRestApi())
                .map(new Function<RestApi, Boolean>() {
                    @Override
                    public Boolean apply(RestApi api) throws Exception {
                        Response<ArrayList<BlackListData>> response = getRestApi().getBlackList().execute();
                        if (response.isSuccessful() && response.body() != null) {
                            return DBHelperFactory.getHelper().getBlackListDao().syncBlackList(response.body());
                        }
                        return false;
                    }
                });

    }

    private Observable<Boolean> getFeedByIdOnServerObserver() {
        return Observable.just(getRestApi())
                .map(new Function<RestApi, Boolean>() {
                    @Override
                    public Boolean apply(RestApi api) throws Exception {

                        List<Integer> feedIds = DBHelperFactory.getHelper().getBlackListDao().getListNotInDBFeed();
                        if (feedIds.size() > 0) {
                            for (int id : feedIds) {
                                Response<FeedData> response = getRestApi().getFeedbyId(id).execute();
                                if (response.isSuccessful() && response.body() != null) {
                                    FeedData data = response.body();
                                    data.setImg("feeds/" + id + ".jpg");
                                    DBHelperFactory.getHelper().getFeedDataDao().createOrUpdate(data);
                                    return true;
                                }
                            }
                        }
                        return false;
                    }
                });

    }
}
