package anews.com.model.profile;

import android.support.v4.app.Fragment;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.Arrays;

import anews.com.network.SimpleModel;

public class FaceBookAuthInfo extends SimpleModel<Boolean, Void> {
    private Fragment mFragment;

    public void init(Fragment fragment, CallbackManager callbackManager) {
        mFragment = fragment;
        LoginManager.getInstance().registerCallback(callbackManager, mFacebookCallback);
    }

    private FacebookCallback<LoginResult> mFacebookCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            if (loginResult.getAccessToken() != null) {
                setData(true);
            } else {
                setData(false);
            }
        }

        @Override
        public void onCancel() {
            setData(false);
        }

        @Override
        public void onError(FacebookException error) {
            setData(false);
        }
    };

    public void auth() {
        startNewRequest();
        LoginManager.getInstance().logInWithReadPermissions(mFragment, Arrays.asList("public_profile", "email"));
    }

}
