package anews.com.model.announce.dto;

public enum AnnounceHelpVHType {
    SHOW_CATALOG,
    SHOW_CATALOG_FOR_ON_BOARDING,
    SHOW_FEEDBACK,
    SHOW_EVENTS,
    SHOW_SETTINGS_ANNOUNCE_STYLE,
    OPEN_GOOGLE_PLAY,
    RATE_REMIND_LATER,
    CLOSE_ON_BOARDING
}
