package anews.com.model.announce.dto;

public class AnnounceHelpItem {

    private AnnounceHelpVHType type;

    public AnnounceHelpItem(AnnounceHelpVHType type) {
        this.type = type;
    }

    public AnnounceHelpVHType getType() {
        return type;
    }
}
