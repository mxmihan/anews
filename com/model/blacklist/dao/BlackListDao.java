package anews.com.model.blacklist.dao;

import android.database.Cursor;

import com.j256.ormlite.android.AndroidDatabaseResults;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.CloseableIterator;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;

import anews.com.model.DBHelperFactory;
import anews.com.model.news.dto.FeedData;
import anews.com.model.blacklist.dto.BlackListData;
import anews.com.model.blacklist.dto.BlackListSyncState;

public class BlackListDao extends BaseDaoImpl<BlackListData, Integer> implements Dao<BlackListData, Integer> {
    private static final long SERVER_CACHE_TIME = 5 * 60 * 1000;

    public BlackListDao(ConnectionSource connectionSource, Class<BlackListData> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public PreparedQuery<BlackListData> getBlackListPreparedQuery() {
        try {
            QueryBuilder<BlackListData, Integer> queryBuilderTopBlackList = DBHelperFactory.getHelper().getBlackListDao().queryBuilder();
            Where<BlackListData, Integer> where = queryBuilder().where();
            where.eq(BlackListData.DB_COLUMN_SYNC_STATE, BlackListSyncState.NEW);
            where.eq(BlackListData.DB_COLUMN_SYNC_STATE, BlackListSyncState.SYNCHRONIZED);
            where.or(2);
            queryBuilderTopBlackList.setWhere(where);
            return queryBuilderTopBlackList.orderBy(BlackListData.DB_COLUMN_FEED_ID, true).prepare();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Cursor getBlackListCursor(PreparedQuery<BlackListData> preparedQuery) {
        try {
            CloseableIterator<BlackListData> iterator = iterator(preparedQuery);
            AndroidDatabaseResults results = (AndroidDatabaseResults) iterator.getRawResults();
            results.getRawCursor();
            return results.getRawCursor();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } finally {
            iterator().closeQuietly();
        }
        return null;
    }

    public void addNewBlackList(final int feedId, final FeedData feedData) {
        try {
            if (queryForEq(BlackListData.DB_COLUMN_FEED_ID, feedId) != null) {
                callBatchTasks(new Callable<Boolean>() {
                    @Override
                    public Boolean call() throws Exception {
                        List<BlackListData> blackListDataList = queryBuilder().orderBy(BlackListData.DB_COLUMN_FEED_ID, true)
                                .where().notIn(BlackListData.DB_COLUMN_FEED_ID, feedId).query();
                        BlackListData blackListData = getBlackListData(feedId);
                        if (blackListData != null) {
                            blackListData.setSyncState(BlackListSyncState.NEW);
                            blackListData.update();
                        } else {
                            create(new BlackListData(feedId, BlackListSyncState.NEW, feedData));
                        }
                        for (int i = 0; i < blackListDataList.size(); i++) {
                            BlackListData data = blackListDataList.get(i);
                            if (data.getFeedData() == null) {
                                data.setFeedData(FeedData.createEmptyPost(data.getId()));
                            }

                            data.update();
                        }
                        return null;
                    }
                });

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean isInBlackList(int fidId) {
        try {
            List<BlackListData> blackListDataList = queryForEq(BlackListData.DB_COLUMN_FEED_ID, fidId);
            if (blackListDataList.size() > 0) {
                BlackListData blackListData = blackListDataList.get(0);
                if (blackListData != null &&
                        (blackListData.getSyncState() == BlackListSyncState.NEW || blackListData.getSyncState() == BlackListSyncState.SYNCHRONIZED)) {
                    return true;
                }
            } else {
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    private BlackListData getBlackListData(int fidId) {
        try {
            List<BlackListData> blackListData = queryForEq(BlackListData.DB_COLUMN_FEED_ID, fidId);
            if (blackListData.size() > 0) {
                return blackListData.get(0);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }

    public List<BlackListData> getNewBlackListForAdd() {
        try {
            return queryForEq(BlackListData.DB_COLUMN_SYNC_STATE, BlackListSyncState.NEW);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public void delleteFromBlackList(int findId, boolean isNeedSync) {
        try {
            List<BlackListData> blackListData = queryForEq(BlackListData.DB_COLUMN_FEED_ID, findId);
            if (blackListData.size() > 0) {
                BlackListData bp = blackListData.get(0);
                if (bp != null) {
                    if (isNeedSync) {
                        bp.setFeedData(FeedData.createEmptyPost(findId));
                        bp.setSyncState(BlackListSyncState.DELETE);
                        bp.update();
                    } else {
                        bp.delete();
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void deleteMarkedBlackList() {
        try {
            DeleteBuilder<BlackListData, Integer> db = deleteBuilder();
            db.where().in(BlackListData.DB_COLUMN_SYNC_STATE, BlackListSyncState.DELETED)
                    .and().lt(BlackListData.DB_COLUMN_SYNC_TIME_STAMP, System.currentTimeMillis() - SERVER_CACHE_TIME);
            db.delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean syncBlackList(final ArrayList<BlackListData> blackListData) {
        try {
            callBatchTasks(new Callable<Boolean>() {
                @Override
                public Boolean call() throws Exception {
                    List<Integer> feedIdList = new ArrayList<>();
                    for (BlackListData ids : blackListData) {
                        feedIdList.add(ids.getId());
                    }
                    QueryBuilder<BlackListData, Integer> queryBuilder = DBHelperFactory.getHelper().getBlackListDao().queryBuilder();
                    List<BlackListData> mBlackListData = queryBuilder.orderBy(BlackListData.DB_COLUMN_FEED_ID, true)
                            .where().notIn(BlackListData.DB_COLUMN_FEED_ID, feedIdList).query();
                    Iterator<BlackListData> it = mBlackListData.iterator();
                    while (it.hasNext()) {
                        BlackListData bp = it.next();
                        if (bp.getSyncState() == BlackListSyncState.SYNCHRONIZED
                                && System.currentTimeMillis() - bp.getSyncTimeStamp() < SERVER_CACHE_TIME) {
                            bp.delete();
                            it.remove();
                        }
                    }

                    for (int i = 0; i < blackListData.size(); i++) {
                        BlackListData ids = blackListData.get(i);
                        List<BlackListData> blackListData = queryForEq(BlackListData.DB_COLUMN_FEED_ID, ids.getId());
                        if (blackListData.size() > 0) {
                            if (blackListData.get(0).getSyncState() != BlackListSyncState.DELETE && blackListData.get(0).getSyncState() != BlackListSyncState.DELETED) {
                                update(new BlackListData(ids.getId(), BlackListSyncState.SYNCHRONIZED, FeedData.createEmptyPost(ids.getId())));
                            }
                        } else {
                            createOrUpdate(new BlackListData(ids.getId(), BlackListSyncState.SYNCHRONIZED, FeedData.createEmptyPost(ids.getId())));
                        }
                    }
                    return true;
                }
            });

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public List<BlackListData> getBlackListForDelete() {
        try {
            return queryForEq(BlackListData.DB_COLUMN_SYNC_STATE, BlackListSyncState.DELETE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public List<Integer> getBlackListId() {
        try {
            return queryRaw("SELECT id FROM black_list WHERE (sync_state = 'NEW' OR sync_state = 'SYNCHRONIZED' ) ",
                    new RawRowMapper<Integer>() {
                        public Integer mapRow(String[] columnNames, String[] resultColumns) {
                            return Integer.parseInt(resultColumns[0]);
                        }
                    }).getResults();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }


    public List<Integer> getListNotInDBFeed() {
        try {
            return queryRaw("SELECT id FROM black_list WHERE id NOT IN (SELECT id FROM feed_data ) ",
                    new RawRowMapper<Integer>() {
                        public Integer mapRow(String[] columnNames, String[] resultColumns) {
                            return Integer.parseInt(resultColumns[0]);
                        }
                    }).getResults();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}






