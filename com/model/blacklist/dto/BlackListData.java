package anews.com.model.blacklist.dto;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.table.DatabaseTable;

import anews.com.model.news.dto.FeedData;

@DatabaseTable(tableName = "black_list")
public class BlackListData extends BaseDaoEnabled {
    public static final String DB_COLUMN_FID_BASE_ID = "base_id";
    public static final String DB_COLUMN_FEED_ID = "id";
    public static final String DB_COLUMN_SYNC_STATE = "sync_state";
    public static final String DB_COLUMN_SYNC_TIME_STAMP = "sync_time_stamp";
    public static final String DB_COLUMN_FEED_DATA_ID = "foreign_feed_id";


    @DatabaseField(columnName = DB_COLUMN_FID_BASE_ID, generatedId = true)
    private int baseId;

    @DatabaseField(columnName = DB_COLUMN_FEED_ID)
    private int id;

    @DatabaseField(defaultValue = "NEW", columnName = DB_COLUMN_SYNC_STATE)
    private BlackListSyncState syncState;

    @DatabaseField(columnName = DB_COLUMN_SYNC_TIME_STAMP)
    private long syncTimeStamp;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, foreignColumnName = "id",
            columnName = DB_COLUMN_FEED_DATA_ID, uniqueCombo = true)
    private FeedData feedData;

    public BlackListData() {
    }

    public BlackListData(int id, BlackListSyncState syncState) {

        this.id = id;
        this.syncState = syncState;
    }


    public BlackListData(int id, BlackListSyncState syncState, FeedData feedData) {
        this.id = id;
        this.feedData = feedData;
        this.syncState = syncState;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BlackListSyncState getSyncState() {
        return syncState;
    }

    public void setSyncState(BlackListSyncState syncState) {
        this.syncState = syncState;
    }

    public long getSyncTimeStamp() {
        return syncTimeStamp;
    }

    public void setSyncTimeStamp(long syncTimeStamp) {
        this.syncTimeStamp = syncTimeStamp;
    }

    public void setFeedData(FeedData feedData) {
        this.feedData = feedData;
    }

    public FeedData getFeedData() {
        return feedData;
    }
}
