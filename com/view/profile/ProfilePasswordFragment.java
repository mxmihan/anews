package anews.com.views.profile;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import anews.com.App;
import anews.com.R;
import anews.com.model.profile.AuthInfo;
import anews.com.model.profile.ChangeUserPasswordInfo;
import anews.com.model.profile.dto.UserProfileData;
import anews.com.network.ModelBase;
import anews.com.network.ModelData;
import anews.com.network.ModelError;
import anews.com.preferences.ProfilePreferences;
import anews.com.utils.AppFragment;
import anews.com.utils.AppUtils;
import anews.com.utils.ui.CircularProgressView;
import anews.com.views.auth.ResetPasswordFragment;


public class ProfilePasswordFragment extends AppFragment implements View.OnClickListener {
    public static final String TAG = ProfilePasswordFragment.class.getSimpleName();
    private EditText mEditTextOldPassword;
    private EditText mEditTextNewPassword;
    private EditText mEditTextConfirmPassword;
    private CircularProgressView mProgressView;
    private AuthInfo mAuthInfo;
    private Button mButtonSave;
    private View mViewOldPassword;
    private View mProgressBar;
    private View mViewNewPassword;
    private View mViewConfirmPassword;
    private ChangeUserPasswordInfo mChangeUserPasswordInfo;

    public static ProfilePasswordFragment newInstance() {
        ProfilePasswordFragment fragment = new ProfilePasswordFragment();
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile_edit_password, container, false);
        if (mChangeUserPasswordInfo == null) {
            mChangeUserPasswordInfo = getModel().getChangeUserPasswordInfo();
        }
        if (mAuthInfo == null) {
            mAuthInfo = getModel().getAuthInfo();
        }
        mButtonSave = view.findViewById(R.id.button_save);
        mButtonSave.setOnClickListener(this);
        view.findViewById(R.id.button_forgot_password).setOnClickListener(this);
        mProgressBar = view.findViewById(R.id.progress_bar_horizontal);
        mEditTextOldPassword = view.findViewById(R.id.edit_text_old_password);
        mEditTextNewPassword = view.findViewById(R.id.edit_text_new_password);
        mEditTextConfirmPassword = view.findViewById(R.id.edit_text_confirm_password);
        setActionBarTitle(R.string.str_fragment_title_edit_password);
        mProgressView = view.findViewById(R.id.progress_view);
        mProgressView.setVisibility(View.GONE);
        mViewOldPassword = view.findViewById(R.id.text_input_layout_edit_old_password);
        mViewNewPassword = view.findViewById(R.id.text_input_layout_new_password);
        mViewConfirmPassword = view.findViewById(R.id.text_input_layout_confirm_password);

        return view;
    }

    @Override
    public void onResume() {
        registerModelListener(mChangeUserPasswordInfo, mChangePasswordListener);
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mChangeUserPasswordInfo.removeListener(mChangePasswordListener, false);
    }

    @Override
    public void updateView() {
        mButtonSave.setEnabled(!mChangeUserPasswordInfo.isUpdating());
        mEditTextOldPassword.setEnabled(!mChangeUserPasswordInfo.isUpdating());
        mEditTextNewPassword.setEnabled(!mChangeUserPasswordInfo.isUpdating());
        mEditTextConfirmPassword.setEnabled(!mChangeUserPasswordInfo.isUpdating());
        mProgressBar.setVisibility(!mChangeUserPasswordInfo.isUpdating() ? View.GONE : View.VISIBLE);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_save:
                saveUserPassword();
                break;
            case R.id.button_forgot_password:
                ((ProfileActivity) getActivity()).showFragmentByTag(ResetPasswordFragment.TAG);
                break;
        }
    }

    public void saveUserPassword() {
        String eMail = ProfilePreferences.getInstance().getEmail();
        String oldPassword = mEditTextOldPassword.getText().toString();
        String password = mEditTextNewPassword.getText().toString();
        String confirmPassword = mEditTextConfirmPassword.getText().toString();
        boolean isEmptyoldPassword = TextUtils.isEmpty(oldPassword);
        boolean isEmptyPassword = TextUtils.isEmpty(password);
        boolean isShortPassword = !TextUtils.isEmpty(password) && password.length() <= 5;
        boolean isNotEquals = !password.equals(confirmPassword);
        if (isEmptyoldPassword) {
            shakeView(mViewOldPassword);
            mEditTextOldPassword.requestFocus();
            getApp().showToastInTop(getString(R.string.error_field_is_empty), true);
        } else {
            if (isEmptyPassword || isShortPassword || isNotEquals) {
                if (isEmptyPassword || isShortPassword) {
                    shakeView(mViewNewPassword);
                    mEditTextNewPassword.requestFocus();
                    if (isShortPassword) {
                        getApp().showToastInTop(getString(R.string.password_is_short), true);
                    } else {
                        getApp().showToastInTop(getString(R.string.error_field_is_empty), true);
                    }
                } else {
                    shakeView(mViewConfirmPassword);
                    mEditTextConfirmPassword.requestFocus();
                    getApp().showToastInTop(getString(R.string.error_field_is_not_equals), true);
                }

            } else {
                mChangeUserPasswordInfo.changePassword(eMail, oldPassword, password);
                AppUtils.hideSoftKeyboard(getActivity());
            }
        }
    }

    private ModelBase.Listener mChangePasswordListener = new ModelBase.Listener<UserProfileData, Void>() {
        @Override
        public void onStateChanged(ModelData<UserProfileData, Void> data) {

            updateView();
            if (!mChangeUserPasswordInfo.isUpdating()) {
                if (data != null && data.getData() != null) {
                    App.getInstance().showToast(R.string.str_change_name_successful, true);
                    mEditTextOldPassword.setText("");
                    mEditTextNewPassword.setText("");
                    mEditTextConfirmPassword.setText("");
                    ((ProfileActivity) getActivity()).showFragmentByTag(ProfileFragment.TAG);

                }
            }
        }

        @Override
        public void onError(@NonNull ModelError error) {
            updateView();
            switch (error) {
                case InvalidOldPassword:
                    shakeView(mViewOldPassword);
                    mEditTextOldPassword.requestFocus();
                    getApp().showToast(getString(R.string.enter_incorrect_old_password), false);
                    break;
                case BadNetworkConnection:
                    getApp().showToast(getString(R.string.str_connection_error), false);
                default:
                    getApp().showToast(getString(R.string.error_unknown), false);
                    break;
            }
        }
    };
}