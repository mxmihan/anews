package anews.com.model.profile.dto;

import java.util.List;

public class UserSubscriptionsData {

    private List<Integer> sources;
    private List<Integer> feeds;
    private List<Integer> streams;

    public List<Integer> getSources() {
        return sources;
    }

    public void setSources(List<Integer> sources) {
        this.sources = sources;
    }

    public List<Integer> getFeeds() {
        return feeds;
    }

    public void setFeeds(List<Integer> feeds) {
        this.feeds = feeds;
    }

    public List<Integer> getStreams() {
        return streams;
    }

    public void setStreams(List<Integer> streams) {
        this.streams = streams;
    }

    public boolean isEmpty() {
        return (sources == null || sources.size() == 0) && (feeds == null || feeds.size() == 0) && (streams == null || streams.size() == 0);
    }
}
