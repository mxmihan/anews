package anews.com.views.settings.adapters.horizontal;

import android.view.View;
import android.widget.LinearLayout;

import anews.com.R;
import anews.com.interfaces.OnSettingsSelectListener;
import anews.com.model.preferences.dto.AnnounceStyleType;
import anews.com.utils.StaticUIHelper;

public class TwoCardLargeImageVH extends AbsAnnounceStyleHorizontalVH implements View.OnClickListener {

    private LinearLayout mDescriptionContainer;
    private AnnounceStyleType mAnnounceStyleType;
    private OnSettingsSelectListener mOnSettingsSelectListener;

    public TwoCardLargeImageVH(View itemView, OnSettingsSelectListener listener) {
        super(itemView);
        mOnSettingsSelectListener = listener;
        itemView.setLayoutParams(StaticUIHelper.getAnnounceRelativeParams());
        mDescriptionContainer = itemView.findViewById(R.id.container_description_text);
        mDescriptionContainer.setVisibility(View.GONE);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        mOnSettingsSelectListener.onSettingsSelectItem(mAnnounceStyleType);
    }

    @Override
    public void setData(AnnounceStyleType data) {
        mAnnounceStyleType = data;
    }
}