package anews.com.views.profile.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import anews.com.R;
import anews.com.interfaces.OnProfileAdapterClickListener;

public class ProfileButtonVievHolder extends AbsProfileHolder implements View.OnClickListener {
    private ImageView mIcon;
    private TextView mText;
    public OnProfileAdapterClickListener mListener;
    public ProfileAdapterItem mProfileAdapterItem;

    ProfileButtonVievHolder(View itemView, OnProfileAdapterClickListener listener) {
        super(itemView);
        mIcon = itemView.findViewById(R.id.image_view_icon);
        mText = itemView.findViewById(R.id.button_text);
        mListener = listener;
        itemView.setOnClickListener(this);
    }

    @Override
    void populate(ProfileAdapterItem item) {
        mProfileAdapterItem = item;
        if (item.getTitleResId() > 0) {
            mText.setText(item.getTitleResId());
        }

        if (item.getDrawableResID() > 0) {
            mIcon.setImageResource(item.getDrawableResID());
        }
    }

    @Override
    public void onClick(View v) {
        mListener.OnProfileAdapterClickItem(mProfileAdapterItem);
    }
}