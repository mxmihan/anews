package anews.com.model.preferences.dto;

import anews.com.R;

public enum AnnounceStyleType {
    TWO_CARD_SMALL_IMAGE(R.string.str_two_cards_small_image, R.string.str_two_cards_small_image_description),
    TWO_CARD_LARGE_IMAGE(R.string.str_two_cards_large_image_title, R.string.str_two_cards_large_image_description),
    ONE_CARD_LARGE_IMAGE(R.string.str_one_cards_large_image, R.string.str_one_cards_large_image_description);

    private int title;
    private int description;

    AnnounceStyleType(int title, int description) {
        this.title = title;
        this.description = description;
    }

    public int getTitle() {
        return title;
    }

    public int getDescription() {
        return description;
    }

    public static AnnounceStyleType getStyle(String s) {
        try {
            return valueOf(s);
        } catch (Exception ex) {
            return TWO_CARD_LARGE_IMAGE;
        }
    }
}