package anews.com.model.events;

import com.j256.ormlite.stmt.PreparedQuery;

import java.util.ArrayList;

import anews.com.model.DBHelperFactory;
import anews.com.model.events.dto.EventPushData;
import anews.com.model.news.dto.PostData;
import anews.com.network.SimpleModel;
import anews.com.views.events.adapters.EventsCursorAdapter;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class EventsInfo extends SimpleModel<ArrayList<PostData>, Void> {

    public void getEventPostsFromCache() {
        Observable.just(new Object())
                .subscribeOn(Schedulers.newThread())
                .map(new Function<Object, ArrayList<PostData>>() {
                    @Override
                    public ArrayList<PostData> apply(Object obj) throws Exception {
                        return DBHelperFactory.getHelper().getPostDataDao().getCachedEventPosts();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<PostData>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ArrayList<PostData> announceVHItems) {
                        setData(announceVHItems);
                    }


                    @Override
                    public void onError(Throwable e) {
                        setError(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public EventsCursorAdapter getPreparedEventsCursorAdapter() {
        PreparedQuery<EventPushData> preparedQuery = DBHelperFactory.getHelper().getEventPushDataDao().getEventsQuery();
        return new EventsCursorAdapter(DBHelperFactory.getHelper().getEventPushDataDao().getEventsCursor(preparedQuery), preparedQuery);
    }

    public void updateEventsAdapter(EventsCursorAdapter adapter) {
        adapter.getCursor().close();
        PreparedQuery<EventPushData> preparedQuery = DBHelperFactory.getHelper().getEventPushDataDao().getEventsQuery();
        adapter.changeCursor(DBHelperFactory.getHelper().getEventPushDataDao().getEventsCursor(preparedQuery), preparedQuery);
    }


    public void removeEvent(EventPushData data) {
        DBHelperFactory.getHelper().getEventPushDataDao().removeEvent(data);
    }
}


