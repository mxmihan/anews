package anews.com.views.announce.adapters.horizontal;

import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.lang.ref.WeakReference;

import anews.com.R;
import anews.com.interfaces.OnAnnouncesHelpActionListener;
import anews.com.model.announce.dto.AnnounceHelpVHType;
import anews.com.model.announce.dto.OnBoardingType;
import anews.com.utils.FontsUtils;
import anews.com.utils.StaticUIHelper;

public class AnnounceHorizontalOnboardingAppVH extends RecyclerView.ViewHolder implements View.OnClickListener, PopupMenu.OnMenuItemClickListener {
    private WeakReference<OnAnnouncesHelpActionListener> mOnAnnouncesHelpActionListener;
    private TextView mHeaderTitle;
    private TextView mDescriptionTitle;
    private ImageView mImageView;
    private ImageView mImageMoreVert;
    private TextView mButtonFeedBack;
    private TextView mButtonClose;
    private View mGradient;
    private OnBoardingType mOnBoardingType;

    public AnnounceHorizontalOnboardingAppVH(View itemView, OnAnnouncesHelpActionListener actionListener) {
        super(itemView);
        mOnAnnouncesHelpActionListener = new WeakReference<>(actionListener);
        itemView.setLayoutParams(StaticUIHelper.getAnnounceRelativeParamsForOneCards());

        mHeaderTitle = itemView.findViewById(R.id.text_view_header_title);
        mDescriptionTitle = itemView.findViewById(R.id.text_view_description_title);
        mImageView = itemView.findViewById(R.id.image_view);
        mImageMoreVert = itemView.findViewById(R.id.image_view_more_vert);
        mButtonFeedBack = itemView.findViewById(R.id.text_view_feed_back);
        mGradient = itemView.findViewById(R.id.view_gradient);
        mButtonClose = itemView.findViewById(R.id.text_view_close);
        mButtonFeedBack.setTypeface(FontsUtils.getTypeFace(itemView.getContext(), FontsUtils.MONTSERRAT_MEDIUM));
        mButtonClose.setTypeface(FontsUtils.getTypeFace(itemView.getContext(), FontsUtils.MONTSERRAT_MEDIUM));

        mHeaderTitle.setTypeface(FontsUtils.getTypeFace(itemView.getContext(), FontsUtils.MONTSERRAT_SEMI_BOLD));
        mDescriptionTitle.setTypeface(FontsUtils.getTypeFace(itemView.getContext(), FontsUtils.MONTSERRAT_REGULAR));
        mButtonFeedBack.setTextSize(TypedValue.COMPLEX_UNIT_PX, StaticUIHelper.getTextSizePx() +
                itemView.getContext().getResources().getDimension(R.dimen.announces_text_title_size));
        mButtonClose.setTextSize(TypedValue.COMPLEX_UNIT_PX, StaticUIHelper.getTextSizePx() +
                itemView.getContext().getResources().getDimension(R.dimen.announces_text_title_size));

        mHeaderTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, StaticUIHelper.getTextSizePx() +
                itemView.getContext().getResources().getDimension(R.dimen.onboarding_text_header_title_size));
        mDescriptionTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, StaticUIHelper.getTextSizePx() +
                itemView.getContext().getResources().getDimension(R.dimen.announces_text_title_size));
        if (getAdapterPosition() < 3) {
            mGradient.setOnClickListener(this);
        }
        mImageMoreVert.setOnClickListener(this);
        mButtonFeedBack.setOnClickListener(this);
        mButtonClose.setOnClickListener(this);

    }

    public void setOnBoardingData(OnBoardingType data) {

        mHeaderTitle.setText(data.getHeaderTitleResId());
        mDescriptionTitle.setText(data.getDescriptionResId());
        mImageView.setImageResource(data.getDrawableResId());
        mOnBoardingType = data;
        if (mOnBoardingType == OnBoardingType.FEED_BACK_CARD) {
            mImageMoreVert.setVisibility(View.GONE);
            mButtonFeedBack.setVisibility(View.VISIBLE);
            mButtonClose.setVisibility(View.VISIBLE);
        }
    }

    private void showPopupMenu(View view) {
        PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.main_onboarding, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(this);
        popupMenu.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_view_more_vert:
                showPopupMenu(v);
                break;
            case R.id.text_view_feed_back:
                mOnAnnouncesHelpActionListener.get().onAnnounceHelpAction(AnnounceHelpVHType.SHOW_FEEDBACK, getAdapterPosition());
                break;
            case R.id.text_view_close:
                mOnAnnouncesHelpActionListener.get().onAnnounceHelpAction(AnnounceHelpVHType.CLOSE_ON_BOARDING, getAdapterPosition());
                break;
            case R.id.view_gradient:
                switch (mOnBoardingType) {
                    case ANNOUNCE_STYLE_SETTINGS_CARD:
                        mOnAnnouncesHelpActionListener.get().onAnnounceHelpAction(AnnounceHelpVHType.SHOW_SETTINGS_ANNOUNCE_STYLE, getAdapterPosition());
                        break;
                    case EVENTS_CARD:
                        mOnAnnouncesHelpActionListener.get().onAnnounceHelpAction(AnnounceHelpVHType.SHOW_EVENTS, getAdapterPosition());
                        break;
                    case CATALOG_CARD:
                        mOnAnnouncesHelpActionListener.get().onAnnounceHelpAction(AnnounceHelpVHType.SHOW_CATALOG_FOR_ON_BOARDING, getAdapterPosition());
                        break;
                }
                break;
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        mOnAnnouncesHelpActionListener.get().onAnnounceHelpAction(AnnounceHelpVHType.CLOSE_ON_BOARDING, getAdapterPosition());
        return false;
    }
}
