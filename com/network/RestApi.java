package anews.com.network;

import java.util.ArrayList;
import java.util.Map;

import anews.com.model.bookmarks.dto.BookmarksListIds;
import anews.com.model.categories.dto.CategoriesListData;
import anews.com.model.categories.dto.DefaultCategoriesData;
import anews.com.model.comments.dto.CommentData;
import anews.com.model.comments.dto.CommentsData;
import anews.com.model.comments.dto.CommentsMetaData;
import anews.com.model.news.dto.FeedData;
import anews.com.model.news.dto.PostData;
import anews.com.model.profile.dto.ResetPasswordResponse;
import anews.com.model.profile.dto.UserProfileData;
import anews.com.model.profile.dto.UserSubscribesData;
import anews.com.model.regions.dto.RegionData;
import anews.com.model.regions.dto.UserRegionData;
import anews.com.model.search.dto.SearchData;
import anews.com.model.search.dto.SearchSourcesData;
import anews.com.model.source.dto.CategorySourcesData;
import anews.com.model.blacklist.dto.BlackListData;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface RestApi {

    @POST(ApiUrls.API_LOGIN)
    @FormUrlEncoded
    Call<UserProfileData> authByPassword(@FieldMap Map<String, String> params);

    @GET(ApiUrls.API_LOGIN_BY_TOKEN)
    Call<UserProfileData> authByToken(@Path("provider") String provider, @QueryMap() Map<String, String> accessToken);

    @GET(ApiUrls.V3_PROFILE)
    Call<UserProfileData> getUserProfile();

    @POST(ApiUrls.V0_PROFILE)
    @FormUrlEncoded
    Call<UserProfileData> changeUserProfile(@FieldMap() Map<String, String> userInfo);

    @POST(ApiUrls.V0_PROFILE)
    @FormUrlEncoded
    Call<UserProfileData> changeUserPassword(@FieldMap() Map<String, String> userInfo);

    @Multipart
    @POST(ApiUrls.V0_PROFILE)
    Call<UserProfileData> loadUserAvatar(@Part MultipartBody.Part file);
 
    @POST(ApiUrls.API_REGISTER)
    @FormUrlEncoded
    Call<UserProfileData> registerNewAccount(@FieldMap() Map<String, String> userInfo);

    @POST(ApiUrls.API_RESET_PASSWORD)
    @FormUrlEncoded
    Call<ResetPasswordResponse> resetPassword(@FieldMap() Map<String, String> userInfo);

    @GET(ApiUrls.API_LOGOUT)
    Call<Void> logout();

    @GET(ApiUrls.V2_REGION_TOP_LIST)
    Call<String> getTopListByRegion(@Path("region") String region);

    @GET(ApiUrls.V2_REGION_TOP)
    Call<ArrayList<PostData>> getNewTopByRegion(@Path("region") String region);

    @GET(ApiUrls.V2_REGIONS)
    Call<ArrayList<RegionData>> getRegions();

    @GET(ApiUrls.V4_REGIONS)
    Call<ArrayList<String>> getV4Regions();

    @POST(ApiUrls.V2_LIKE)
    Call<Void> addLike(@Path("postId") int postId);

    @POST(ApiUrls.V3_PROFILE_BLACKLIST)
    @FormUrlEncoded
    Call<BlackListData> addBlackList(@Field(value = "id") int feedId);

    @GET(ApiUrls.V3_PROFILE_BLACKLIST)
    Call<ArrayList<BlackListData>> getBlackList();

    @DELETE(ApiUrls.V3_PROFILE_BLACKLIST_DELETE)
    Call<Void> deleteBlackList(@Path("fid") int feedId);

    @GET(ApiUrls.V3_FEED_ID)
    Call<FeedData> getFeedbyId(@Path("fid") int feedId);
}
