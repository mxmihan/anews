package anews.com.model.profile;

import anews.com.network.ApiUrls;
import anews.com.network.SimpleModel;
import anews.com.utils.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogoutInfo extends SimpleModel<Void, Void> {

    public void logout() {
        startNewRequest();
        getRestApi().logout().enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful() && response.raw() != null) {
                    okhttp3.Response priorResponse = response.raw().priorResponse();
                    if (priorResponse != null && priorResponse.headers() != null && (ApiUrls.API_DOMAIN + ApiUrls.API_LOGIN).equals(priorResponse.header("Location"))) {
                        AppUtils.logD("logout", "success");
                    }
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }
}
