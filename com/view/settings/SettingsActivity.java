package anews.com.views.settings;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import anews.com.R;
import anews.com.analytic.Analytics;
import anews.com.utils.AppFragmentActivity;

public class SettingsActivity extends AppFragmentActivity {
    public static final String SHOW_ONLY_STYLE = "show_only_style";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (getIntent() != null && getIntent().hasExtra(SHOW_ONLY_STYLE)) {
            mSelectedTag = SettingAnnounceStyleFragment.TAG;
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Analytics.openPage(Analytics.OPEN_SCREEN_SETTINGS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected int getFragmentContainerId() {
        return R.id.fragment_container;
    }

    @Override
    protected String getDefaultFragmentTag() {
        return SettingsFragment.TAG;
    }

    @Override
    protected Fragment getContentFragment(String tag) {
        if (SettingsFragment.TAG.equals(tag)) {
            return SettingsFragment.newInstance();
        } else if (SettingAnnounceStyleFragment.TAG.equals(tag)) {
            return SettingAnnounceStyleFragment.newInstance();
        } else if (SettingBlackListFragment.TAG.equals(tag)) {
            return SettingBlackListFragment.newInstance();
        } else {
            return SettingsFragment.newInstance();
        }
    }

    @Override
    public void onBackPressed() {
        if (getIntent() != null && getIntent().hasExtra(SHOW_ONLY_STYLE)) {
                finish();
        } else if (getSelectedTag().equals(SettingAnnounceStyleFragment.TAG)) {
            showFragmentByTag(SettingsFragment.TAG);
        } else {
            super.onBackPressed();
        }
    }

}