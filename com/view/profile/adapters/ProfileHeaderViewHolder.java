package anews.com.views.profile.adapters;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import anews.com.R;
import anews.com.interfaces.OnProfileAdapterClickListener;
import anews.com.preferences.ProfilePreferences;
import anews.com.utils.AppUtils;
import anews.com.utils.glide.GlideApp;

public class ProfileHeaderViewHolder extends AbsProfileHolder implements View.OnClickListener {
    private OnProfileAdapterClickListener mListener;
    private ImageView mImageViewIcon;
    private TextView mTextViewName;
    private TextView mTextViewEmail;
    private ProfileAdapterItem mProfileAdapterItem;

    ProfileHeaderViewHolder(View itemView, OnProfileAdapterClickListener listener) {
        super(itemView);
        itemView.setOnClickListener(this);
        mListener = listener;
        mImageViewIcon = itemView.findViewById(R.id.image_view_account_icon);
        mTextViewName = itemView.findViewById(R.id.text_view_name);
        mTextViewEmail = itemView.findViewById(R.id.text_view_email);
    }

    @Override
    void populate(ProfileAdapterItem item) {
        mProfileAdapterItem = item;
        String avatar = ProfilePreferences.getInstance().getAvatar();
        String firstName = ProfilePreferences.getInstance().getFirstName();
        String lastName = ProfilePreferences.getInstance().getLastName();
        String email = ProfilePreferences.getInstance().getEmail();
        mTextViewName.setText(firstName + " " + lastName);
        if (AppUtils.isStringEmail(email)) {
            mTextViewEmail.setText(email);
        } else {
            mTextViewEmail.setText("");
        }
        if (!TextUtils.isEmpty(avatar)) {
            GlideApp.with(mImageViewIcon.getContext())
                    .load(avatar)
                    .apply(RequestOptions.circleCropTransform())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .addListener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(mImageViewIcon);

        } else {
            GlideApp.with(mImageViewIcon.getContext())
                    .load(R.drawable.ic_avatar)
                    .apply(RequestOptions.circleCropTransform())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(mImageViewIcon);
        }
    }

    @Override
    public void onClick(View v) {
        mListener.OnProfileAdapterClickItem(mProfileAdapterItem);
    }
}
