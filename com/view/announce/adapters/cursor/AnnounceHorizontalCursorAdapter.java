package anews.com.views.announce.adapters.cursor;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.j256.ormlite.stmt.PreparedQuery;

import java.lang.ref.WeakReference;

import anews.com.R;
import anews.com.interfaces.AnnouncePostListener;
import anews.com.interfaces.OnCategorySourceClickListener;
import anews.com.interfaces.OnPositionClickListener;
import anews.com.interfaces.OnPostIdClickListener;
import anews.com.model.announce.dto.AnnounceVHItem;
import anews.com.model.announce.dto.AnnounceVHType;
import anews.com.model.news.dto.PostData;
import anews.com.views.announce.adapters.horizontal.AbsAnnounceHorizontalVH;
import anews.com.views.announce.adapters.horizontal.AnnounceEndVH;
import anews.com.views.announce.adapters.horizontal.AnnounceHorizontalPlaceHolderVH;
import anews.com.views.announce.adapters.horizontal.AnnounceHorizontalPostVH;
import anews.com.views.announce.adapters.horizontal.AnnounceHorizontalTopPlaceHolderVH;
import anews.com.views.announce.adapters.horizontal.AnnounceHorizontalTopVH;
import anews.com.views.announce.adapters.horizontal.AnnounceProgressVH;

public class AnnounceHorizontalCursorAdapter extends AbsAnnounceOrmliteCursorAdapter<PostData, AbsAnnounceHorizontalVH> implements OnPositionClickListener, OnPostIdClickListener {

    private AnnounceVHItem mAnnounceVHItem;
    private WeakReference<AnnouncePostListener> mOnPostClickedListener;
    private WeakReference<OnCategorySourceClickListener> mOnSourceListener;
    private int mPlaceholderCountItem = 2;
    private static final int ITEM_PLACE_HOLDER = 2;

    public AnnounceHorizontalCursorAdapter(AnnounceVHItem data, PreparedQuery<PostData> preparedQuery, Cursor cursor) {
        super(cursor, preparedQuery);
        mAnnounceVHItem = data;
    }

    public void setSourceListener(OnCategorySourceClickListener sourceListener) {
        mOnSourceListener = new WeakReference<>(sourceListener);

    }

    public void setAnnouncePostListener(AnnouncePostListener listener) {
        mOnPostClickedListener = new WeakReference<>(listener);

    }

    @NonNull
    @Override
    public AbsAnnounceHorizontalVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        switch (AnnounceVHType.values()[viewType]) {
            case TOP:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_announce_horizontal_top_item, parent, false);
                return new AnnounceHorizontalTopVH(v, this);
            case TOP_PLACE_HOLDER:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_announce_horizontal_top_place_holder_item, parent, false);
                return new AnnounceHorizontalTopPlaceHolderVH(v);
            case PROGRESS:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_announce_progress_item, parent, false);
                return new AnnounceProgressVH(v);
            case END:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_announce_end_item, parent, false);
                return new AnnounceEndVH(v);
            case PLACE_HOLDER:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_announce_horizontal_place_holder_item, parent, false);
                return new AnnounceHorizontalPlaceHolderVH(v);
            case ITEM: // ITEM
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_announce_horizontal_post_item, parent, false);
                return new AnnounceHorizontalPostVH(v);
            default:
                return null;
        }
    }


    @Override
    public void onBindViewHolder(AbsAnnounceHorizontalVH holder) {
        if (holder instanceof AnnounceHorizontalPlaceHolderVH) {
            ((AnnounceHorizontalPlaceHolderVH) holder).setSourceInfo(mAnnounceVHItem.getCategorySourceData());
        }
    }

    @Override
    public int getItemViewType(int position) {

        if (getCursor() == null || getCursor().getCount() == 0) {
            if (mAnnounceVHItem.isTop()) {
                return AnnounceVHType.TOP_PLACE_HOLDER.ordinal();
            } else {
                return AnnounceVHType.PLACE_HOLDER.ordinal();
            }
        } else {
            if (position > getCursor().getCount() - 1) {
                if (mAnnounceVHItem.isNoMorePosts()) {
                    return AnnounceVHType.END.ordinal();
                } else {
                    return AnnounceVHType.PROGRESS.ordinal();
                }
            } else {
                return mAnnounceVHItem.getAnnounceVHType().ordinal();
            }
        }

    }

    @Override
    public void onPositionClicked(int position, View view) {
        switch (view.getId()) {
            case R.id.view_source_container:
                if (mOnSourceListener.get() != null) {
                    mOnSourceListener.get().onCategoryClicked(mAnnounceVHItem.getCategorySourceData());
                }
                break;
            default:
                if (mOnPostClickedListener.get() != null && position >= 0) {
                    mOnPostClickedListener.get().postClicked(mAnnounceVHItem, (PostData) view.getTag());
                }
                break;
        }

    }

    public int getPostsSize() {
        return getCursor() != null ? getCursor().getCount() : 0;
    }

    public boolean isNoMere() {
        return mAnnounceVHItem.isNoMorePosts();
    }

    @Override
    public int getItemCount() {
        return getCursor() == null || getCursor().getCount() == 0
                ? mPlaceholderCountItem : getCursor().getCount() + 1;
    }

    public AnnounceVHItem getItem() {
        return mAnnounceVHItem;
    }

    @Override
    public void onBindViewHolder(final AbsAnnounceHorizontalVH holder, PostData postData) {
        String title;
        if (mAnnounceVHItem.isTop()) {
            title = postData.getFeedTitle();
        } else {
            title = mAnnounceVHItem.getCategorySourceData().getTitle();
        }
        holder.setData(postData, title);
        if (holder instanceof AnnounceHorizontalPostVH){
            ((AnnounceHorizontalPostVH) holder).addListener(this);
        }

    }


    @Override
    public void onPositionClick(int position) {
//        mListener.onPostClicked(null, getTypedItem(position - (isNeedShowHelp() ? 1 : 0)).getPostId());

    }

}