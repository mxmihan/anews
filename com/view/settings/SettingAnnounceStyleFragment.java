package anews.com.views.settings;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import anews.com.R;
import anews.com.interfaces.OnSettingsSelectListener;
import anews.com.model.preferences.dto.AnnounceStyleType;
import anews.com.preferences.GlobalPreferences;
import anews.com.preferences.ProfilePreferences;
import anews.com.utils.AppFragment;
import anews.com.views.settings.adapters.vertical.AnnounceStyleVerticalAdapter;

public class SettingAnnounceStyleFragment extends AppFragment implements OnSettingsSelectListener {
    public static final String TAG = SettingAnnounceStyleFragment.class.getSimpleName();
    private RecyclerView mRecyclerViewVertical;
    private AnnounceStyleVerticalAdapter mAdapter;
    public static SettingAnnounceStyleFragment newInstance() {
        Bundle args = new Bundle();
        SettingAnnounceStyleFragment fragment = new SettingAnnounceStyleFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting_style_announce, container, false);
        mRecyclerViewVertical = view.findViewById(R.id.recycler_view);
        LinearLayoutManager manager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerViewVertical.setLayoutManager(manager);
        mAdapter = new AnnounceStyleVerticalAdapter(this);
        mRecyclerViewVertical.setAdapter(mAdapter);
        return view;
    }

    @Override
    public void onSettingsSelectItem(AnnounceStyleType announceStyleType) {
        ProfilePreferences.getInstance().setStyleAnnounce(announceStyleType);
        GlobalPreferences.getInstance().setUpdate(true);
        ((SettingsActivity) getActivity()).showFragmentByTag(SettingsFragment.TAG);
    }

    @Override
    public void onResume() {
        super.onResume();
        setActionBarTitle(R.string.str_fragment_title_announce_settings);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mAdapter.notifyDataSetChanged();
    }
}