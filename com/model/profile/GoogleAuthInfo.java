package anews.com.model.profile;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.IOException;

import anews.com.App;
import anews.com.R;
import anews.com.network.SimpleModel;

public class GoogleAuthInfo extends SimpleModel<String, Void> {
    private GoogleApiClient mGoogleApiClient;
    public static final int REQUEST_PICK_GOOGLE_ACCOUNT = 1003;
    private static final String SCOPE = "oauth2:https://www.googleapis.com/auth/userinfo.profile "
            + Scopes.PLUS_LOGIN + " " + Scopes.PLUS_ME + " " + "https://www.googleapis.com/auth/userinfo.email";

    public void auth(Fragment fragment) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            Intent intent = AccountManager.newChooseAccountIntent(null, null,
                    new String[]{"com.google"}, null, null, null, null);

            fragment.startActivityForResult(intent, REQUEST_PICK_GOOGLE_ACCOUNT);
        } else {
            String[] accounts = getAccountNames(fragment.getActivity());
            if (accounts.length > 1) {
                Intent googlePicker = AccountPicker.newChooseAccountIntent(null, null,
                        new String[]{"com.google"}, true, null, null, null, null);
                fragment.startActivityForResult(googlePicker, REQUEST_PICK_GOOGLE_ACCOUNT);
            } else if (accounts.length == 1) {
                auth(accounts[0]);
            } else {
                Toast.makeText(fragment.getActivity(), fragment.getString(R.string.err_no_accounts_in_system), Toast.LENGTH_LONG).show();
            }
        }
    }

    private String[] getAccountNames(Context ctx) {
        AccountManager mAccountManager = AccountManager.get(ctx);
        Account[] accounts = mAccountManager.getAccountsByType("com.google");
        String[] names = new String[accounts.length];
        for (int i = 0; i < names.length; i++) {
            names[i] = accounts[i].name;
        }
        return names;
    }

    public void onResult(int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            auth(data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME));
        }
    }

    public void auth(String email) {
        new OAuthGoogle(email).execute();
    }


    private class OAuthGoogle extends AsyncTask<Void, Void, String> {
        private String mEmail;

        OAuthGoogle(String email) {
            this.mEmail = email;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {

                //TODO need create dev google api
//                if (BuildConfig.DEBUG) {
//                    return "ya29.Glt-BHl9LbR5QmgXvUqubrPu3HBuQOT6yDywOPLZszs1utzp2T9iRYa1rj9pwr8atiJa_jAVbwpITZASe5Ye-NHeTAH6doVRYgESff70F-mqwW2Hqmra3VLbal_E";
//                } else {
                return GoogleAuthUtil.getToken(App.getInstance(), mEmail, SCOPE);
//                }
            } catch (IOException | GoogleAuthException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String token) {
            setData(token);
        }
    }
}
