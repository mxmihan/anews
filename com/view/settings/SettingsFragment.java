package anews.com.views.settings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.text.TextUtils;

import anews.com.R;
import anews.com.analytic.Analytics;
import anews.com.preferences.GlobalPreferences;
import anews.com.preferences.ProfilePreferences;
import anews.com.utils.StaticUIHelper;
import anews.com.views.purchases.PurchasesActivity;

public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener, Preference.OnPreferenceClickListener {
    public static final String TAG = SettingsFragment.class.getSimpleName();
    private static final String KEY_AD_FREE = "key_ad_free";
    private static final String KEY_SETTINGS_ANNOUNCE_FRAGMENT = "setting_announce_fragment";
    private static final String KEY_BLACK_LIST_FRAGMENT = "black_list_fragment";
    private Preference mKeyAdFree;
    private Preference mStyleAnnounceSelect;
    private Preference mBlackListFragment;

    private Preference mPreference;
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        PreferenceManager prefManager = getPreferenceManager();
        prefManager.setSharedPreferencesName(ProfilePreferences.PROFILE_PREFERENCES_FILE);
        addPreferencesFromResource(R.xml.preferences);
        SharedPreferences sharedPreferences = ProfilePreferences.getInstance().getPreferences();
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        mKeyAdFree = prefManager.findPreference(KEY_AD_FREE);
        mBlackListFragment = prefManager.findPreference(KEY_BLACK_LIST_FRAGMENT);
        mStyleAnnounceSelect = findPreference(KEY_SETTINGS_ANNOUNCE_FRAGMENT);
        if (mStyleAnnounceSelect != null) {
            mStyleAnnounceSelect.setOnPreferenceClickListener(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.str_fragment_title_settings);
        mStyleAnnounceSelect.setSummary(ProfilePreferences.getInstance().getStyleAnnounce().getTitle());
    }

    public static Fragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        switch (preference.getKey()) {
            case KEY_AD_FREE:
                startActivity(new Intent(getActivity(), PurchasesActivity.class));
                break;
            case KEY_BLACK_LIST_FRAGMENT:
                ((SettingsActivity) getActivity()).showFragmentByTag(SettingBlackListFragment.TAG);
                break;
            case KEY_SETTINGS_ANNOUNCE_FRAGMENT:
                ((SettingsActivity) getActivity()).showFragmentByTag(SettingAnnounceStyleFragment.TAG);
        }
        return super.onPreferenceTreeClick(preference);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        sendAnalytics(key);
    }


    public void sendAnalytics(final String key) {
        String value = null;
        switch (key) {
            case ProfilePreferences.EXTERNAL_BROWSER:
                value = ProfilePreferences.getInstance().isNeedShowInExternalBrowser() ? "on" : "off";
                break;
            case ProfilePreferences.IS_NEED_SHOW_TOP:
                GlobalPreferences.getInstance().setIsAnnouncesItemsChanges(true);
                value = ProfilePreferences.getInstance().isNeedShowTop() ? "on" : "off";
                break;
            case ProfilePreferences.REGION:
                value = ProfilePreferences.getInstance().getRegion();
                break;
            case ProfilePreferences.USER_IS_NEED_SHOW_COMMENTS:
                value = ProfilePreferences.getInstance().isCommentsVisible() ? "on" : "off";
                break;
            case ProfilePreferences.IS_NEED_MARK_READ_NEWS:
                GlobalPreferences.getInstance().setIsAnnouncesItemsChanges(true);
                StaticUIHelper.removeMarkPosts();
                value = ProfilePreferences.getInstance().isNeedMarkReadNews() ? "on" : "off";
                break;
            case ProfilePreferences.FONT_SIZE:
                GlobalPreferences.getInstance().setUpdate(true);
                StaticUIHelper.cleanTitleSize();
                value = ProfilePreferences.getInstance().getFontsSize().name();
                break;
            case ProfilePreferences.SHOW_IMAGES:
                GlobalPreferences.getInstance().setUpdate(true);
                StaticUIHelper.removeShowImages();
                value = ProfilePreferences.getInstance().isNeedShowImages() ? "on" : "off";
                break;
            case ProfilePreferences.STYLE_ANNOUNCE:
                value = ProfilePreferences.getInstance().getStyleAnnounce().name();
                break;

        }
        if (!TextUtils.isEmpty(value)) {
            Analytics.trackEvent(getContext(), Analytics.CATEGORY_SETTINGS, Analytics.CATEGORY_SETTINGS, key, value, Analytics.buildEmptyParams(getActivity()));
        }
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        //    ((SettingsActivity) getActivity()).showFragmentByTag(SettingAnnounceStyleFragment.TAG);
        return true;
    }
}