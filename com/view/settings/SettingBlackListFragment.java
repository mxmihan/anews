package anews.com.views.settings;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import anews.com.R;
import anews.com.model.blacklist.dto.BlackListData;
import anews.com.model.blacklist.BlackListInfo;
import anews.com.network.ModelBase;
import anews.com.network.ModelData;
import anews.com.network.ModelError;
import anews.com.preferences.GlobalPreferences;
import anews.com.utils.AppStateFragment;
import anews.com.utils.AppUtils;
import anews.com.utils.FontsUtils;
import anews.com.utils.StaticUIHelper;
import anews.com.utils.adapters.AbsSwipeableVH;
import anews.com.utils.adapters.SwipeableItemTouchCallback;
import anews.com.utils.ui.ExTextView;
import anews.com.views.settings.adapters.vertical.BlackListCursorAdapter;

public class SettingBlackListFragment extends AppStateFragment implements SwipeRefreshLayout.OnRefreshListener, SwipeableItemTouchCallback.RecyclerItemTouchHelperListener {
    public static final String TAG = SettingBlackListFragment.class.getSimpleName();
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private BlackListInfo mBlackListInfo = getModel().getBlackListInfo();
    private View mEmptyView;
    private BlackListCursorAdapter mAdapter;
    private ExTextView titleEmpty;
    private ExTextView descriptionEmpty;
    public static SettingBlackListFragment newInstance() {
        Bundle args = new Bundle();
        SettingBlackListFragment fragment = new SettingBlackListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting_black_list, container, false);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.black);
        titleEmpty = view.findViewById(R.id.text_view_empty);
        descriptionEmpty = view.findViewById(R.id.text_view_description_title);
        mEmptyView = view.findViewById(R.id.empty_view);
        titleEmpty.setTypeface(FontsUtils.getTypeFace(view.getContext(), FontsUtils.MONTSERRAT_REGULAR));
        titleEmpty.setTextSize(TypedValue.COMPLEX_UNIT_PX, StaticUIHelper.getTextSizePx() + view.getContext().getResources().getDimension(R.dimen.empty_title_text_size));
        descriptionEmpty.setTypeface(FontsUtils.getTypeFace(view.getContext(), FontsUtils.MONTSERRAT_REGULAR));
        descriptionEmpty.setTextSize(TypedValue.COMPLEX_UNIT_PX, StaticUIHelper.getTextSizePx() + view.getContext().getResources().getDimension(R.dimen.empty_description_text_size));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        ItemTouchHelper touchHelper = new ItemTouchHelper(new SwipeableItemTouchCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, this));
        touchHelper.attachToRecyclerView(mRecyclerView);
        mAdapter = mBlackListInfo.getPreparedBlackListCursorAdapter();
        mRecyclerView.setAdapter(mAdapter);
        updateView();
        return view;
    }
    @Override
    public void onPause() {
        super.onPause();
        mBlackListInfo.removeListener(mBlackListListener, false);
    }
    @Override
    public void saveState() {

    }
    @Override
    public void onResume() {
        registerModelListener(mBlackListInfo, mBlackListListener);
        super.onResume();
        setActionBarTitle(R.string.str_fragment_settings_title_blacklist);
        if (AppUtils.isAuthorized()) {
            mBlackListInfo.getBlackList();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mAdapter.notifyDataSetChanged();
    }
    private ModelBase.Listener mBlackListListener = new ModelBase.Listener<Void, Void>() {
        @Override
        public void onStateChanged(ModelData<Void, Void> data) {
            if (!mBlackListInfo.isUpdating()) {
                mBlackListInfo.updateBlackListAdapter(mAdapter);
                mSwipeRefreshLayout.setRefreshing(false);
                updateView();
                GlobalPreferences.getInstance().setIsAnnouncesItemsChanges(true);
            }
        }
        @Override
        public void onError(@NonNull ModelError error) {
            updateView();
            switch (error) {
                case BadNetworkConnection:
                    getApp().showToast(getString(R.string.str_connection_error), false);
                default:
                    getApp().showToast(getString(R.string.error_unknown), false);
                    break;
            }
        }
    };
    @Override
    public void updateView() {
        mEmptyView.setVisibility(mAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
        mRecyclerView.setVisibility(mAdapter.getItemCount() != 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void startSwipe() {
        mSwipeRefreshLayout.setEnabled(false);
    }

    @Override
    public void stopSwipe() {
        mSwipeRefreshLayout.setEnabled(true);
    }

    @Override
    public void onRefresh() {
        mBlackListInfo.getBlackList();
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof AbsSwipeableVH) {
            BlackListData blackListData = (BlackListData) ((AbsSwipeableVH) viewHolder).getData();
            mBlackListInfo.deleteBlackList(blackListData.getId());
            mBlackListInfo.updateBlackListAdapter(mAdapter);
            updateView();
        }
    }
}