package anews.com.preferences;

import android.content.Context;

public class NotClearablePreferenses extends AbsPreferences {

    private static final String NOT_ClEAR_PREFERENCES_FILE = "not_clear_preferences";

    private static final String IS_APP_ON_BOARDING = "app_on_boarding";
    private static final String APP_RATE_SHOW_TIME ="app_rate_show_time";

    private volatile static NotClearablePreferenses mInstance;

    private NotClearablePreferenses(Context context) {
        super(context, NOT_ClEAR_PREFERENCES_FILE);
    }

    public static void initialize(Context ctx) {
        if (mInstance == null) {
            synchronized (NotClearablePreferenses.class) {
                if (mInstance == null) {
                    mInstance = new NotClearablePreferenses(ctx);
                }
            }
        }
    }

    public static NotClearablePreferenses getInstance() {
        if (mInstance == null) {
            throw new IllegalStateException("Must Initialize NotClearablePreferenses before using getInstance()");
        } else {
            return mInstance;
        }
    }

    public long getAppRateShowTime() {
        return getLongPrefs(APP_RATE_SHOW_TIME);
    }
    public void setAppRateShowTime(long timeInMillis) {
        saveLongPrefs(APP_RATE_SHOW_TIME, timeInMillis);
    }


    public boolean isNeedShowOnboarding() {
        return getBooleanPrefs(IS_APP_ON_BOARDING,true);
    }

    public void setNotNeedShowOnboarding() {
        saveBooleanPrefs(IS_APP_ON_BOARDING, false);
    }

}
