package anews.com.analytic;

import android.app.Activity;
import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import anews.com.model.news.dto.PostData;
import anews.com.preferences.GlobalPreferences;
import anews.com.model.preferences.dto.NewsReadMode;
import anews.com.utils.AppUtils;

public class Analytics {
    public static final String FIRE_BASE_KEY = "XXXXXXXXXXXXX";
    public static final String YANDEX_KEY = "74cf4618-bf4c-4613-9ccc-a54c80c47d8d";
    public static final String FLURRY_KEY = "99TJDK8B84H42GM7X9HJ";
    public static final String GOOGLE_KEY = "UA-52899434-1";
    public static final String APPSFLYER_KEY = "zcHMX5rxTYbmmFFiYVSVVa";
    //Flurry
    public static final String POST_READ = "Post_read";
    public static final String NEWS_CLICK = "Click news";
    public static final String NEWS_ADDED_TO_FAVORITES = "News added to favorites";
    public static final String FEED_ADDED_TO_BLACKLIST = "Feed added to top blacklist";
    public static final String REMOVE_FROM_FAVORITES = "New remove from favorites";
    public static final String REMOVE_FROM_BLACKLIST = "Feed remove from top blacklist";


    /////
    public static final long ANALYTIC_TIMEOUT = 3000;
    public static final String CATEGORY_LAUNCH = "Launch";

    public static final String CATEGORY_FEEDS_AND_CATEGORY = "Feeds and category activity";
    public static final String CATEGORY_ACTIVITY = "Activity";
    public static final String CATEGORY_SETTINGS = "Settings";
    public static final String CATEGORY_POST = "POST";
    public static final String CATEGORY_NEWS_PREVIEW = "News preview";

    public static final String ACTION_LOGOUT = "Logout";
    public static final String ACTION_AUTHORIZATION = "Authorization";

    public static final String ACTION_AUTH_VK = "vk";
    public static final String ACTION_AUTH_FACEBOOK = "facebook";
    public static final String ACTION_AUTH_EMAIL = "e-mail";
    public static final String ACTION_AUTH_GOOGLE = "google +";
    public static final String ACTION_AUTH_TWITTER = "twitter";
    public static final String ACTION_AUTH_CREATE_NEW_ACCOUNT = "create new account";
    public static final String ACTION_AUTH_RESET_PASSWORD = "reset password";

    public static final String ACTION_OPEN_APP = "Open application";
    public static final String ACTION_CLICK = "Click";
    public static final String ACTION_SHARING = "shared news";
    public static final String ACTION_ADD_TO_FAVORITE = "add to favorites";
    public static final String ACTION_ADD_TO_BLACKLIST = "add to favorites";
    public static final String ACTION_REMOVE_FROM_FAVORITE = "remove from favorites";
    public static final String ACTION_REMOVE_FROM_BLACKLIST = "remove from top blacklist";
    public static final String ACTION_LIKE = "like";
    public static final String ACTION_UNLIKE = "unlike";
    public static final String ACTION_OPEN_PUSH = "push";
    public static final String ACTION_POST_READ = "Read post";
    public static final String ACTION_REMOVE_EVENTS = "Remove from events";
    public static final String ACTION_CLEAN_EVENTS = "Clear events";
    public static final String ACTION_ADD_COMMENTS = "add comments";
    public static final String ACTION_CATEGORY_CONTENTS = "Сategory contents";
    public static final String ACTION_FEED_VIEW = "Feed view";
    public static final String ACTION_SORTING = "sorting";
    public static final String ACTION_CHANGE_REGION = "change region";
    public static final String ACTION_SUBSCRIBE = "Subscribe";
    public static final String ACTION_UNSUBSCRIBE = "Unsubscribe";
    public static final String ACTION_OPEN_IN_BROWSER = "Open in browser";
    public static final String ACTION_GO_TO_SOURSE = "Go to sourse";
    public static final String ACTION_RATE_APP = "Rate app";
    public static final String LABEL_HIDE = "hide";
    public static final String LABEL_SHOW = "show";
    public static final String LABEL_CATEGORY = "category";
    public static final String LABEL_FEED = "http://www.anews.com/api/v3/feeds/";
    public static final String LABEL_LINK = "anews.com/p/";

    public static final String LABEL_LATER = "later";
    public static final String LABEL_GOOGLE_PLAY = "google play";

    public static final String OPEN_SCREEN_LOGIN = "Login screen";
    public static final String OPEN_SCREEN_CREATE_ACCOUNT = "Create account";
    public static final String OPEN_SCREEN_PAGE_FORGOT_PASSWORD = "  Forgot password";
    public static final String OPEN_SCREEN_NEWS = "News";
    public static final String OPEN_SCREEN_SINGLE_FEED = "Single feed";
    public static final String OPEN_SCREEN_POST = "Post";
    public static final String OPEN_SCREEN_COMMENTS = "Comments";
    public static final String OPEN_SCREEN_SUBSCRUPTIONS = "Subscriptions";
    public static final String OPEN_SCREEN_CATALOG = "Catalog";
    public static final String OPEN_SCREEN_FEEDS = "Feeds";
    public static final String OPEN_SCREEN_NEWS_SEARCH = "News search";
    public static final String OPEN_SCREEN_FEED_SEARCH = "Feeed search";
    public static final String OPEN_SCREEN_EVETNS = "Events";
    public static final String OPEN_SCREEN_FAVORITES = "Favorites"; //BOOKMARKS
    public static final String OPEN_SCREEN_SETTINGS = "Settings";

    public static final String OPEN_SCREEN_FEEDBACK = "Feedback";
    public static final String OPEN_SCREEN_HELP = "Help";
    public static final String OPEN_SCREEN_SPLASH = "Splash";
    public static final String OPEN_SCREEN_FULL_IMAGE_NEWS = "Full image news";

    private static void addDeviceTypeParam(Context context, Map<String, String> param) {
        if (context != null) {
            param.put("device_type", AppUtils.isTabletLayout(context) ? "tablet" : "phone");

            String mode;
//            if (GlobalPreferences.getInstance().isNormalReadMode()) {

                mode = String.valueOf(NewsReadMode.NORMAL);
//            } else {
//                mode = String.valueOf(NewsReadMode.READABILITY);
//            }
            param.put("mode", mode);
        } else {
            param.put("device_type", "unknown");
        }
    }

    
    public static Map<String, String> buildEmptyParams(Context context) {
        Map<String, String> param = new HashMap<String, String>();
        addDeviceTypeParam(context, param);
        return param;
    }



    public static Map<String, String> buildParamsForPostSelected(Context context, PostData postData) {
        Map<String, String> param = new HashMap<String, String>();
        param.put("source_title", String.valueOf(postData.getFeedTitle()));
        param.put("post_title", postData.getTitle());
        addDeviceTypeParam(context, param);
        return param;
    }

    public static void start(Activity context) {
        AnalyticsTracker.getInstance().startSession(context);
    }

    public static void stop(Activity context) {
        AnalyticsTracker.getInstance().endSession(context);
    }

    public static void openPage(String page) {
        try {
            AnalyticsTracker.getInstance().trackPageView(page);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void trackEvent(Context context, String eventFlurry, String category, String event) {
        try {
            Map<String, String> param = new HashMap<String, String>();
            addDeviceTypeParam(context, param);
            if (eventFlurry != null)
                AnalyticsTracker.getInstance().trackEvent(eventFlurry, param);

            HashMap<Integer, String> googleParams = initGoogleParams(param);
            AnalyticsTracker.getInstance().trackEvent(category, event, null, googleParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void trackEvent(Context context, String eventFlurry, String category, String event, String label, Map<String, String> param) {
        try {
            addDeviceTypeParam(context, param);
            if (eventFlurry != null) {
                AnalyticsTracker.getInstance().trackEvent(eventFlurry, param);
            }

            HashMap<Integer, String> googleParams = initGoogleParams(param);
            AnalyticsTracker.getInstance().trackEvent(category, event, label, googleParams);
        } catch (Exception e) {

        }
    }

    public static void trackEvent(Context context, String eventFlurry, String category, String event, String label) {
        try {
            Map<String, String> param = new HashMap<String, String>();
            addDeviceTypeParam(context, param);
            if (eventFlurry != null) {
                AnalyticsTracker.getInstance().trackEvent(eventFlurry, param);
            }

            HashMap<Integer, String> googleParams = initGoogleParams(param);
            AnalyticsTracker.getInstance().trackEvent(category, event, label, googleParams);
        } catch (Exception e) {
        }
    }

    public static Map<String, String> buildParamsForLogin(Context context, String type) {
        Map<String, String> param = new HashMap<String, String>();
        try {
            param.put("network_type", type);
            addDeviceTypeParam(context, param);
        } catch (Exception e) {
        }
        return param;
    }

    private static HashMap<Integer, String> initGoogleParams(Map<String, String> param) {
        HashMap<Integer, String> googleParams = new HashMap<>();
        if (param.containsKey("uniqueid")) {
            googleParams.put(1, param.get("uniqueid"));
        }
        if (param.containsKey("device_type")) {
            googleParams.put(2, param.get("device_type"));
        }
        if (param.containsKey("key")) {
            googleParams.put(3, param.get("key"));
        }
        if (param.containsKey("link")) {
            googleParams.put(4, param.get("link"));
        }
        if (param.containsKey("source_title")) {
            googleParams.put(6, param.get("source_title"));
        }
        if (param.containsKey("post_title")) {
            googleParams.put(9, param.get("post_title"));
        }
        if (param.containsKey("user_type")) {
            googleParams.put(10, param.get("user_type"));
        }
        return googleParams;
    }

}
