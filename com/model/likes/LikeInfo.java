package anews.com.model.likes;


import android.support.annotation.NonNull;

import anews.com.App;
import anews.com.analytic.Analytics;
import anews.com.model.DBHelperFactory;
import anews.com.network.SimpleModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LikeInfo extends SimpleModel<Void, Void> {

    public void addLikeNews(final int postId) {
        startNewRequest();
        getRestApi().addLike(postId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                if (response.isSuccessful()) {
                    DBHelperFactory.getHelper().getLikesDataDao().setLike(postId, true);
                    setData(null);
                    Analytics.trackEvent(App.getInstance(), Analytics.ACTION_LIKE, Analytics.CATEGORY_ACTIVITY,
                            Analytics.ACTION_LIKE, Analytics.LABEL_LINK + postId);
                } else {
                    setError(response);

                }
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                setError(t);
            }
        });
    }

    public void unLikeNews(final int postId) {
        startNewRequest();
        getRestApi().addLike(postId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                if (response.isSuccessful()) {
                    DBHelperFactory.getHelper().getLikesDataDao().setLike(postId, false);
                    setData(null);
                    Analytics.trackEvent(App.getInstance(), Analytics.ACTION_UNLIKE, Analytics.CATEGORY_ACTIVITY,
                            Analytics.ACTION_UNLIKE, Analytics.LABEL_LINK + postId);
                } else {
                    setError(response);

                }
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                setError(t);
            }
        });
    }

}