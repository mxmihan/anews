package anews.com.notifications;

import android.content.Intent;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import java.io.Serializable;

import anews.com.App;
import anews.com.analytic.Analytics;
import anews.com.model.events.dto.EventPushData;
import anews.com.views.main.MainActivity;

/**
 * TODO
 * Open SplashActivity with a flag, that it launches the push content right after initialization
 * As a first item, with possible scroll to right, which will scroll through TOP news
 */

public class NotificationOpenedHandler implements OneSignal.NotificationOpenedHandler, Serializable {
    private App application;

    public NotificationOpenedHandler(App application) {
        this.application = application;
    }

    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        try {

            Gson gson = new GsonBuilder().serializeNulls().create();
            Intent intent = new Intent(application, MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
                            | Intent.FLAG_ACTIVITY_SINGLE_TOP
                            | Intent.FLAG_ACTIVITY_NEW_TASK);
            if (result.notification.payload.additionalData != null) {
                Analytics.trackEvent(application, null, Analytics.CATEGORY_ACTIVITY, Analytics.ACTION_OPEN_PUSH);
                EventPushData data = gson.fromJson(result.notification.payload.additionalData.toString(), EventPushData.class);
                intent.putExtra(MainActivity.NOTIFICATION_DATA, data);
                application.startActivity(intent);
            }
        } catch (Exception ignored) {
        }

    }
}
