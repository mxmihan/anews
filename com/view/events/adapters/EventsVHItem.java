package anews.com.views.events.adapters;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;

import anews.com.R;
import anews.com.interfaces.OnPositionClickListener;
import anews.com.model.events.dto.EventPushData;
import anews.com.utils.DateUtil;
import anews.com.utils.adapters.AbsSwipeableVH;
import anews.com.utils.glide.GlideUtils;
import anews.com.utils.ui.ExTextView;

public class EventsVHItem extends AbsSwipeableVH<EventPushData> implements View.OnClickListener {

    private ExTextView mTitle;
    private TextView mSource;
    private TextView mTime;

    private ImageView mImage;
    private WeakReference<OnPositionClickListener> mListener;
    private EventPushData eventPushData;

    @Override
    public void onClick(View v) {
        if (mListener.get() != null) {
            mListener.get().onPositionClick(getAdapterPosition());
        }
    }

    public EventsVHItem(View itemView, OnPositionClickListener listener) {
        super(itemView);
        mListener = new WeakReference<>(listener);
        mSource = itemView.findViewById(R.id.tv_source);
        mTitle = itemView.findViewById(R.id.tv_title);
        mImage = itemView.findViewById(R.id.iv_image);
        mTime = itemView.findViewById(R.id.text_view_time);
        itemView.setOnClickListener(this);
    }

    void bindView(EventPushData data) {
        eventPushData = data;
        mSource.setText(data.getPostData().getFeedTitle());
        mTitle.setText(data.getPostData().getTitle());
        mTime.setText(" \u2022 " + (DateUtil.dateBack(data.getPostData().getTimeStamp() * 1000)));
        if (!TextUtils.isEmpty(data.getPostData().getImg())) {
            GlideUtils.loadImage(itemView.getContext(), mImage, data.getPostData().getImageUrl((int) itemView.getResources().getDimension(R.dimen.events_image_size), 0));
        } else {
            mImage.setImageResource(R.color.announce_card_background);
        }
    }

    @Override
    public boolean isSwipeable() {
        return true;
    }

    @Override
    public EventPushData getData() {
        return eventPushData;
    }


}