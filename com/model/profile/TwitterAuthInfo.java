package anews.com.model.profile;

import android.content.Intent;
import android.view.View;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import anews.com.network.SimpleModel;

public class TwitterAuthInfo extends SimpleModel<Boolean, Void> {
    private TwitterLoginButton mLoginButton;

    public void init(View twitterButton) {
        mLoginButton = (TwitterLoginButton) twitterButton;
        mLoginButton.setCallback(mTwitterCallBack);




    }

    public void onResult(int requestCode, int resultCode, Intent data) {
        mLoginButton.onActivityResult(requestCode, resultCode, data);
    }

    private Callback<TwitterSession> mTwitterCallBack = new Callback<TwitterSession>() {
        @Override
        public void success(Result<TwitterSession> result) {
            setData(true);
        }

        @Override
        public void failure(TwitterException exception) {
            setData(false);
        }
    };

    public void auth() {

    }
}
