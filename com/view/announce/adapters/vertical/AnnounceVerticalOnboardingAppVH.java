package anews.com.views.announce.adapters.vertical;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;

import com.github.rubensousa.gravitysnaphelper.GravityPagerSnapHelper;

import anews.com.R;
import anews.com.interfaces.OnAnnouncesHelpActionListener;
import anews.com.model.announce.dto.AnnounceHelpVHType;
import anews.com.model.announce.dto.AnnounceVHItem;
import anews.com.utils.ui.HorizontalLinearLayoutManager;
import anews.com.views.announce.adapters.horizontal.AnnounceHorizontalOnboardingAppAdapter;

public class AnnounceVerticalOnboardingAppVH extends AbsAnnounceVerticalVH implements OnAnnouncesHelpActionListener {
    private OnAnnouncesHelpActionListener mOnAnnouncesHelpActionListener;
    private AnnounceHorizontalOnboardingAppAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private AnnounceVHItem mAnnounceVHItem;
    private int mAdapterPosition;

    public AnnounceVerticalOnboardingAppVH(View itemView, OnAnnouncesHelpActionListener actionListener) {
        super(itemView);
        mOnAnnouncesHelpActionListener = actionListener;
        mRecyclerView = itemView.findViewById(R.id.recycler_view_onbording_app);
        mRecyclerView.setLayoutManager(new HorizontalLinearLayoutManager(itemView.getContext(), LinearLayoutManager.HORIZONTAL, false));
        GravityPagerSnapHelper snapHelper = new GravityPagerSnapHelper(Gravity.START);
        snapHelper.attachToRecyclerView(mRecyclerView);
        mAdapter = new AnnounceHorizontalOnboardingAppAdapter(this);
        mRecyclerView.addOnScrollListener(mRecyclerScrollListener);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
    }

    @Override
    public void setData(AnnounceVHItem data) {
        mAnnounceVHItem = data;
        mAdapterPosition = mAnnounceVHItem.getAdapterPosition();
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public int getItemPosition() {
        return mAdapterPosition;
    }

    @Override
    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    @Override
    public void setScrolledPosition(int position) {
        LinearLayoutManager layoutManager = ((LinearLayoutManager) mRecyclerView.getLayoutManager());
        layoutManager.scrollToPosition(position);
    }

    @Override
    public void onAnnounceHelpAction(AnnounceHelpVHType action, int position) {
        mOnAnnouncesHelpActionListener.onAnnounceHelpAction(action, getAdapterPosition());
    }

    private RecyclerView.OnScrollListener mRecyclerScrollListener = new RecyclerView.OnScrollListener() {

        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                LinearLayoutManager layoutManager = ((LinearLayoutManager) mRecyclerView.getLayoutManager());
                mAnnounceVHItem.setHorizontalScrollPosition(layoutManager.findFirstCompletelyVisibleItemPosition());
            }
        }
    };
}