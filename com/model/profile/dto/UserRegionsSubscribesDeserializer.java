package anews.com.model.profile.dto;


import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class UserRegionsSubscribesDeserializer implements JsonDeserializer<UserRegionsSubscribesData> {

    @Override
    public UserRegionsSubscribesData deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = element.getAsJsonObject();
        LinkedHashMap<String, ArrayList<UserSourceSubscribesData>> linkedHashMap = new LinkedHashMap<>();
        for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
            ArrayList<UserSourceSubscribesData> categoryData = context.deserialize(entry.getValue(), new TypeToken<ArrayList<UserSourceSubscribesData>>() {
            }.getType());
            linkedHashMap.put(entry.getKey(), categoryData);
        }
        return new UserRegionsSubscribesData(linkedHashMap);
    }

}
