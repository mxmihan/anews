package anews.com.views.profile.adapters;

public class ProfileAdapterItem {

    private Type type;
    private int titleResId;
    private int drawableResId;

    public ProfileAdapterItem(Type type) {
        this.type = type;
    }

    public ProfileAdapterItem(Type type, int titleResId, int drawableResId) {
        this.type = type;
        this.titleResId = titleResId;
        this.drawableResId = drawableResId;
    }

    public Type getType() {
        return type;
    }

    public int getTitleResId() {
        return titleResId;
    }

    public int getDrawableResID() {
        return drawableResId;
    }

    public enum Type {
        HEADER_ITEM,
        BUTTON_ITEM,
    }

}
