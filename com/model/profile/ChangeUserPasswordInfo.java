package anews.com.model.profile;

import java.io.IOException;
import java.util.HashMap;

import anews.com.model.profile.dto.UserProfileData;
import anews.com.network.ErrorUtils;
import anews.com.network.ModelError;
import anews.com.network.SimpleModel;
import anews.com.network.exceptions.ModelThrowException;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class ChangeUserPasswordInfo extends SimpleModel<UserProfileData, Void> {

    public void changePassword(final String email, final String oldPassword, final String newPassword) {
        final HashMap<String, String> checkPasswordMap = new HashMap<>();
        final HashMap<String, String> changePasswordMap = new HashMap<>();
        checkPasswordMap.put("username", email);
        checkPasswordMap.put("password", oldPassword);
        changePasswordMap.put("password", newPassword);
        startNewRequest();
        Observable.just(new UserProfileData())
                .subscribeOn(Schedulers.newThread())
                .map(new Function<UserProfileData, UserProfileData>() {
                    @Override
                    public UserProfileData apply(UserProfileData userProfileData) throws IOException {
                        Response<UserProfileData> response = getRestApi().authByPassword(checkPasswordMap).execute();
                        if (response.isSuccessful()) {
                            if (response.body().getCode() != null) {
                                if (response.body().getCode() == 100) {
                                    throw new ModelThrowException(ModelError.InvalidOldPassword);
                                }
                            }
                        } else {
                            setError(ErrorUtils.parseError(response).getModelError());
                        }
                        return userProfileData;
                    }
                })
                .map(new Function<UserProfileData, UserProfileData>() {
                    @Override
                    public UserProfileData apply(UserProfileData userProfileData) throws IOException {
                        Response<UserProfileData> response = getRestApi().changeUserPassword(changePasswordMap).execute();
                        if (!response.isSuccessful()) {
                            setError(ErrorUtils.parseError(response).getModelError());
                        }
                        return userProfileData;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserProfileData>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(UserProfileData userProfileData) {
                        if (userProfileData.getError() != null) {
                        } else {
                            setData(userProfileData);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        setError(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}


