package anews.com.model.profile;


import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import anews.com.model.DBHelperFactory;
import anews.com.model.profile.dto.UserSubscriptionsOrderCategoriesData;
import anews.com.model.profile.dto.UserSubscriptionsOrderData;
import anews.com.network.RestApi;
import anews.com.network.SimpleModel;
import anews.com.preferences.ProfilePreferences;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class UserSubscriptionsOrderInfo extends SimpleModel<Void, Void> {

    public void syncSubscriptionsOrder() {
        Observable.just(getRestApi())
                .subscribeOn(Schedulers.io())
                .map(new Function<RestApi, Object>() {
                    @Override
                    public Object apply(RestApi restApi) {
                        LinkedHashMap<String, ArrayList<UserSubscriptionsOrderCategoriesData>> orders = DBHelperFactory.getHelper().getCategoryDataDao().getUserSubscriptionsForOrder();
                        UserSubscriptionsOrderData orderData = new UserSubscriptionsOrderData();
                        orderData.setLastSort(ProfilePreferences.getInstance().getLastSort());
                        orderData.setRegions(orders);
                        try {
                            String order = new Gson().toJson(orderData);
                            restApi.syncSubscriptionsOrder(order).execute();
                        } catch (Exception e) {
                            Crashlytics.logException(e);
                        }
                        return new Object();
                    }
                })
                .subscribe();


    }
}
