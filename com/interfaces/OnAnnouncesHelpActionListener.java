package anews.com.interfaces;


import anews.com.model.announce.dto.AnnounceHelpVHType;

public interface OnAnnouncesHelpActionListener {
    void onAnnounceHelpAction(AnnounceHelpVHType action, int position);
}
