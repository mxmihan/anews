package anews.com.model.events.dto;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

import anews.com.model.news.dto.PostData;

@DatabaseTable(tableName = "events_posts_ids")
public class EventPushData extends BaseDaoEnabled implements Serializable {
    private static final long serialVersionUID = 2536914838650478229L;
    public static final String DB_COLUMN_ID = "base_id";
    public static final String DB_COLUMN_POST_ID = "post_id";
    public static final String DB_COLUMN_FOREIGN_POST_ID = "foreign_post_id";
    public static final String DB_COLUMN_TYPE = "type";

    @DatabaseField(generatedId = true, columnName = DB_COLUMN_ID)
    private int baseId;

    @SerializedName("p")
    @DatabaseField(columnName = DB_COLUMN_POST_ID)
    private int postId;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, foreignColumnName = "id",
            columnName = DB_COLUMN_FOREIGN_POST_ID, uniqueCombo = true)
    private PostData postData;

    @SerializedName("type")
    @DatabaseField(columnName = DB_COLUMN_TYPE)
    private EventPushType type;

    public EventPushData() {
    }

    public EventPushData(PostData postData, EventPushType type) {
        this.postData = postData;
        this.postId = postData.getId();
        this.type = type;
    }

    public int getPostId() {
        return postId;
    }


    public EventPushType getType() {
        return type;
    }

    public PostData getPostData() {
        return postData;
    }

    public void setPostData(PostData postData) {
        this.postData = postData;
    }

    public void setType(EventPushType type) {
        this.type = type;
    }

    public String getLinkForTack() {
        return "anews.com/p/" + getPostId();
    }
}
