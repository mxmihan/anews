package anews.com.preferences;

import android.content.Context;
import android.content.SharedPreferences;


public abstract class AbsPreferences {
    private static final String TAG = "AbsPreferences";
    private static final Object mLock = new Object();
    private SharedPreferences mSharedPreferences;

    AbsPreferences(Context context, String prefFile) {
        mSharedPreferences = context.getSharedPreferences(prefFile, Context.MODE_PRIVATE);
    }

    static void save(SharedPreferences.Editor editor) {
        synchronized (mLock) {
            editor.apply();
        }
    }

    public SharedPreferences getPreferences() {
        return mSharedPreferences;
    }

    public SharedPreferences.Editor getEditor() {
        return getPreferences().edit();
    }

    public String getStringPrefs(String prefsToken) {
        return getPreferences().getString(prefsToken, "");
    }

    public String getStringPrefs(String prefsToken, String defValue) {
        return getPreferences().getString(prefsToken, defValue);
    }

    public void saveStringPrefs(String prefsToken, String data) {
        save(getEditor().putString(prefsToken, data));
    }

    public void saveLongPrefs(String prefsToken, long data) {
        save(getEditor().putLong(prefsToken, data));
    }

    public float getFloatPrefs(String prefsToken) {
        return getPreferences().getFloat(prefsToken, 0f);
    }

    public Boolean getBooleanPrefs(String prefsToken) {
        return getPreferences().getBoolean(prefsToken, false);
    }

    public Boolean getBooleanPrefs(String prefsToken, boolean defValue) {
        return getPreferences().getBoolean(prefsToken, defValue);
    }

    public void saveBooleanPrefs(String prefsToken, boolean data) {
        save(getEditor().putBoolean(prefsToken, data));
    }

    public void saveIntPrefs(String prefsToken, int data) {
        save(getEditor().putInt(prefsToken, data));
    }

    public int getIntPrefs(String prefsToken) {
        return getPreferences().getInt(prefsToken, -1);
    }

    public long getLongPrefs(String prefsToken) {
        return getPreferences().getLong(prefsToken, -1);
    }

    public void wipeAllSavedData() {
        mSharedPreferences.edit().clear().apply();
    }

}
