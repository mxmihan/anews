package anews.com.views.settings.adapters.vertical;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import anews.com.R;
import anews.com.model.blacklist.dto.BlackListData;
import anews.com.utils.FontsUtils;
import anews.com.utils.StaticUIHelper;
import anews.com.utils.adapters.AbsSwipeableVH;
import anews.com.utils.glide.GlideApp;
import anews.com.utils.ui.ExTextView;

public class BlackListVerticalVH extends AbsSwipeableVH<BlackListData> {

    private ExTextView mFeedTitle;
    private ImageView mFeedIcon;
    private BlackListData data;

    public BlackListVerticalVH(@NonNull View itemView) {
        super(itemView);
        mFeedTitle = itemView.findViewById(R.id.text_view_feed_title);
        mFeedIcon = itemView.findViewById(R.id.image_view_feed_icon);
        mFeedTitle.setTypeface(FontsUtils.getTypeFace(itemView.getContext(), FontsUtils.MONTSERRAT_MEDIUM));
        mFeedTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, StaticUIHelper.getTextSizePx() + itemView.getContext().getResources().getDimension(R.dimen.btn_text_size));
    }

    public void bindView(BlackListData data) {
        this.data = data;
        if (data.getFeedData() != null) {
            if (!TextUtils.isEmpty(data.getFeedData().getTitle())) {
                mFeedTitle.setText(data.getFeedData().getTitle());
            }
            if (!TextUtils.isEmpty(data.getFeedData().getImageUrlOriginal())) {
                GlideApp.with(mFeedIcon.getContext())
                        .load(data.getFeedData().getImageUrlOriginal())
                    .apply(RequestOptions.circleCropTransform())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .addListener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                        .into(mFeedIcon);
            }
        }
    }

    @Override
    public boolean isSwipeable() {
        return true;
    }

    @Override
    public BlackListData getData() {
        return data;
    }
}
