package anews.com.model.likes.dto;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = "likes_data")
public class LikeData extends BaseDaoEnabled implements Serializable {
    public static final String DB_COLUMN_POST_ID = "post_id";
    public static final String DB_COLUMN_LIKES_TIME = "like_time";
    public static final String DB_COLUMN_LIKES_STATE = "like_state";

    @DatabaseField(id = true, columnName = DB_COLUMN_POST_ID)
    private int postId;


    @DatabaseField(columnName = DB_COLUMN_LIKES_TIME)
    private long likeTime;

    @DatabaseField(columnName = DB_COLUMN_LIKES_STATE)
    private LikeState likeState;

    public LikeData() {
    }

    public LikeData(int postId, long likeTime, LikeState likeState) {
        this.postId = postId;
        this.likeTime = likeTime;
        this.likeState = likeState;
    }

    public LikeState getLikeState() {
        return likeState;
    }

    public void setLikeState(LikeState likeState) {
        this.likeState = likeState;
    }


    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public long getLikeTime() {
        return likeTime;
    }

    public void setLikeTime(long likeTime) {
        this.likeTime = likeTime;
    }


}
