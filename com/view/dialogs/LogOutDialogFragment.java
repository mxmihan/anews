package anews.com.views.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import anews.com.R;


public class LogOutDialogFragment extends DialogFragment implements View.OnClickListener {
    public static final String TAG = "LogOutDialogFragment";

    public static LogOutDialogFragment newInstance() {

        return new LogOutDialogFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.DialogFragment);
        return dialog;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_log_out, null, false);
        view.findViewById(R.id.text_view_positive).setOnClickListener(this);
        view.findViewById(R.id.text_view_negative).setOnClickListener(this);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.text_view_positive:
                if (getParentFragment() instanceof LogOutDialogListener) {
                    ((LogOutDialogListener) getParentFragment()).logout();
                }
                if (getActivity() instanceof LogOutDialogListener) {
                    ((LogOutDialogListener) getActivity()).logout();
                }
                dismiss();
                break;
            case R.id.text_view_negative:
                dismiss();
                break;
        }
    }

    public interface LogOutDialogListener {
        void logout();
    }

}
