package anews.com.model.profile.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import anews.com.model.profile.dto.UserProfileData;

public class UserProfileDataDao extends BaseDaoImpl<UserProfileData, Integer> implements Dao<UserProfileData, Integer> {

    public UserProfileDataDao(ConnectionSource connectionSource, Class<UserProfileData> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
