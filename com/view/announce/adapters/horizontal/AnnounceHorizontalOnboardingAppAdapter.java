package anews.com.views.announce.adapters.horizontal;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import anews.com.R;
import anews.com.interfaces.OnAnnouncesHelpActionListener;
import anews.com.model.announce.dto.OnBoardingType;

public class AnnounceHorizontalOnboardingAppAdapter extends RecyclerView.Adapter<AnnounceHorizontalOnboardingAppVH> {

    private OnAnnouncesHelpActionListener mOnAnnouncesHelpActionListener;
    private List<OnBoardingType> listOnBoarding;

    public AnnounceHorizontalOnboardingAppAdapter(OnAnnouncesHelpActionListener actionListener) {
        mOnAnnouncesHelpActionListener = actionListener;
        setDataOnBoarding();
    }

    @NonNull
    @Override
    public AnnounceHorizontalOnboardingAppVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_announce_horizontal_onbording_item, parent, false);
        return new AnnounceHorizontalOnboardingAppVH(v, mOnAnnouncesHelpActionListener);
    }

    @Override
    public void onBindViewHolder(@NonNull AnnounceHorizontalOnboardingAppVH holder, int position) {
        holder.setOnBoardingData(listOnBoarding.get(position));
    }

    @Override
    public int getItemCount() {
        return listOnBoarding.size();
    }

    private void setDataOnBoarding() {
        listOnBoarding = new ArrayList<>();
        for (OnBoardingType onBoardingType : OnBoardingType.values()) {
            listOnBoarding.add(onBoardingType);
        }
    }

}
