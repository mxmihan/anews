package anews.com.views.profile.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class AbsProfileHolder extends RecyclerView.ViewHolder {

    AbsProfileHolder(View itemView) {
        super(itemView);
    }
    abstract void populate(ProfileAdapterItem item);
}
