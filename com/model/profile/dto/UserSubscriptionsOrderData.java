package anews.com.model.profile.dto;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class UserSubscriptionsOrderData {

    @SerializedName("last_sort")
    private long lastSort;

    private LinkedHashMap<String, ArrayList<UserSubscriptionsOrderCategoriesData>> regions;

    public void setLastSort(long lastSort) {
        this.lastSort = lastSort;
    }

    public void setRegions(LinkedHashMap<String, ArrayList<UserSubscriptionsOrderCategoriesData>> regions) {
        this.regions = regions;
    }
}
