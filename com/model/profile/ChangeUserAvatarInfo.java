package anews.com.model.profile;

import java.io.File;
import java.io.IOException;

import anews.com.model.profile.dto.UserProfileData;
import anews.com.network.SimpleModel;
import anews.com.preferences.ProfilePreferences;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;


public class ChangeUserAvatarInfo extends SimpleModel<UserProfileData, Void> {
    private MultipartBody.Part getMultipartBody(File file) {
        RequestBody requestBody = getRequestBodyForImage(file);
        return MultipartBody.Part.createFormData("avatar_image", file.getName(), requestBody);
    }

    private RequestBody getRequestBodyForImage(File file) {
        return RequestBody.create(MediaType.parse("image/*"), file);
    }

    public void changeUserAvatar(File file) {
        startNewRequest();
        Observable.just(file)
                .subscribeOn(Schedulers.newThread())
                .map(new Function<File, UserProfileData>() {
                    @Override
                    public UserProfileData apply(File file) {
                        try {
                            Response<UserProfileData> response = getRestApi().loadUserAvatar(getMultipartBody(file)).execute();
                            if (response.isSuccessful()) {
                                return response.body();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        throw new RuntimeException();
                    }
                })

                .map(new Function<UserProfileData, UserProfileData>() {
                    @Override
                    public UserProfileData apply(UserProfileData userProfileData) {
                        try {
                            Response<UserProfileData> response = getRestApi().getUserProfile().execute();
                            if (response.isSuccessful()) {
                                return response.body();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        throw new RuntimeException();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserProfileData>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                    @Override
                    public void onNext(UserProfileData userProfileData) {
                        ProfilePreferences.getInstance().saveUserProfileData(userProfileData);
                        setData(userProfileData);
                    }

                    @Override
                    public void onError(Throwable e) {
                        setError(e);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


}
