package anews.com.model.profile.dto;

import java.util.Map;

public class UserSubscriptionsOrderCategoriesData {
    private int id;
    private Map<Integer, Integer> streams;
    private Map<Integer, Integer> feeds;
    private int pos = 0;

    public void setId(int id) {
        this.id = id;
    }


    public void setPos(int pos) {
        this.pos = pos;
    }

    public Map<Integer, Integer> getStreams() {
        return streams;
    }

    public Map<Integer, Integer> getFeeds() {
        return feeds;
    }

    public void setStreams(Map<Integer, Integer> streams) {
        this.streams = streams;
    }

    public void setFeeds(Map<Integer, Integer> feeds) {
        this.feeds = feeds;
    }
}
