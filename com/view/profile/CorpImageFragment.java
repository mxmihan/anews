package anews.com.views.profile;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;

import androidx.annotation.RequiresApi;
import anews.com.R;
import anews.com.model.profile.ChangeUserAvatarInfo;
import anews.com.utils.AppFragment;
import anews.com.utils.AppUtils;

public class CorpImageFragment extends AppFragment implements CropImageView.OnCropImageCompleteListener {
    public static final String TAG = CorpImageFragment.class.getSimpleName();
    private CropImageView mCropImageView;
    private ChangeUserAvatarInfo mChangeUserAvatarInfo;
    private boolean isUri;
    private String mImageFilePath;

    public static CorpImageFragment newInstance() {
        Bundle args = new Bundle();
        CorpImageFragment fragment = new CorpImageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_croping, container, false);
        setActionBarTitle(R.string.str_editor_title);
        if (mChangeUserAvatarInfo == null) {
            mChangeUserAvatarInfo = getModel().getChangeUserAvatarInfo();
        }

        mCropImageView = view.findViewById(R.id.cropImageView);
        setHasOptionsMenu(true);
        setCropImageViewOptions();
        isUri = getArguments().getBoolean(ProfileFragment.SAVED_IS_URI);
        mImageFilePath = getArguments().getString(ProfileFragment.SAVED_IMAGE_FILE_PATH);
        if (mImageFilePath != null) {
            try {
                mCropImageView.setImageBitmap(AppUtils.getResolutionBitmap(AppUtils.getBitmap(isUri, mImageFilePath, getContext())));
                mCropImageView.rotateImage(AppUtils.getRotationImage(mImageFilePath));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        mCropImageView.setOnCropImageCompleteListener(this);
        return view;
    }

    private void setCropImageViewOptions() {
        mCropImageView.setFixedAspectRatio(true);
        mCropImageView.setCropShape(CropImageView.CropShape.OVAL);
        mCropImageView.setShowProgressBar(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_crop_button, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.main_action_crop) {
            mCropImageView.getCroppedImageAsync();
            return true;
        } else if (item.getItemId() == R.id.main_action_rotate) {
            mCropImageView.rotateImage(90);
            return true;
        } else if (item.getItemId() == R.id.main_action_flip_horizontally) {
            mCropImageView.flipImageHorizontally();
            return true;
        } else if (item.getItemId() == R.id.main_action_flip_vertically) {
            mCropImageView.flipImageVertically();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCropImageComplete(CropImageView cropImageView, CropImageView.CropResult cropResult) {
        Bundle args = new Bundle();
        args.putBoolean(ProfileFragment.SAVED_IS_UPDATE, true);
        mChangeUserAvatarInfo.changeUserAvatar(new File(
                AppUtils.getRealPathFromUri(AppUtils.getUriImageFromBitmap(cropResult.getBitmap(), getContext()), getContext()
                )
        ));

        ((ProfileActivity) getActivity()).showFragmentByTag(ProfileFragment.TAG, args);
    }
}

