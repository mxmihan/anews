package anews.com.views.settings.adapters.horizontal;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import anews.com.model.preferences.dto.AnnounceStyleType;

public abstract class AbsAnnounceStyleHorizontalVH extends RecyclerView.ViewHolder {

    public AbsAnnounceStyleHorizontalVH(View itemView) {
        super(itemView);
    }
    public abstract void setData(AnnounceStyleType data);

}
