package anews.com.interfaces;


import anews.com.views.profile.adapters.ProfileAdapterItem;

public interface OnProfileAdapterClickListener {
    void OnProfileAdapterClickItem(ProfileAdapterItem item);
}
