package anews.com.model.profile.dto;

public class AuthDataRequest {

    private String username;
    private String password;

    public AuthDataRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
