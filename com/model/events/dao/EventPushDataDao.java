package anews.com.model.events.dao;

import android.database.Cursor;

import com.crashlytics.android.Crashlytics;
import com.j256.ormlite.android.AndroidConnectionSource;
import com.j256.ormlite.android.AndroidDatabaseResults;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.CloseableIterator;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import anews.com.model.DBHelperFactory;
import anews.com.model.events.dto.EventPushData;
import anews.com.model.news.dto.SourcePostsBaseIds;

public class EventPushDataDao extends BaseDaoImpl<EventPushData, Integer> implements Dao<EventPushData, Integer> {

    public EventPushDataDao(ConnectionSource connectionSource, Class<EventPushData> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public void removeEvent(EventPushData data) {
        try {
            data.delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public PreparedQuery<EventPushData> getEventsQuery() {
        try {
            QueryBuilder<EventPushData, Integer> queryBuilderBookmarks = queryBuilder();
            return queryBuilderBookmarks.orderBy(EventPushData.DB_COLUMN_ID, false).prepare();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Cursor getEventsCursor(PreparedQuery<EventPushData> preparedQuery) {
        try {
            CloseableIterator<EventPushData> iterator = iterator(preparedQuery);
            AndroidDatabaseResults results = (AndroidDatabaseResults) iterator.getRawResults();
            return results.getRawCursor();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } finally {
            iterator().closeQuietly();
        }
        return null;
    }
}

