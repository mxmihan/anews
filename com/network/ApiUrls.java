package anews.com.network;



public class ApiUrls {
    public final static String API_DOMAIN = "https://xxxxxxxxxxxxxxxxx/";
  
    public final static String API_LOGIN = "api/login/";

    public final static String API_LOGIN_BY_TOKEN = "api/login/by_token/{provider}/";
    public final static String API_REGISTER = "api/register/";
    public final static String API_RESET_PASSWORD = "api/init_password_reset/";
    public final static String API_LOGOUT = "api/logout/";

    public static final String V2_REGIONS = "https:/xxxxxxxxx/info/regions";


    /*API v0*/
    public final static String V0_PROFILE = "api/v0/user.json";
    /*API v2*/
 
    public static final String V2_LIKE = "api/v2/posts/{postId}/likes/";
    public static final String V2_ISLIKE = "api/v2/likes/check/{ids}/";
    public static final String V2_LIKE_LIST = "api/v2/posts/{postId}/likes/list";

    /*API v3*/

    public static final String V3_COMMENTS_POSTS = "api/v3/posts/{postId}/comments/region/{region}";
    public static final String V3_ADD_COMMENTS_POSTS = "api/v3/posts/{postId}/comments/";
    public static final String V3_CATEGORIES = "api/v3/categories/";
    public static final String V3_CATEGORIES_SOURCES = "api/v3/categories/{sourceId}/sources/";
    public final static String V3_PROFILE = "api/v3/profile/";
    public final static String V3_PROFILE_SUBSCRIPTIONS = "api/v3/profile/subscriptions/";
    public final static String V3_PROFILE_SUBSCRIPTIONS_ORDER = "api/v3/profile/subscriptions/order/";
    public final static String V3_SEARCH_POSTS = "api/v3/search/";
    public final static String V3_PROFILE_BLACKLIST = "api/v3/profile/blacklist/";
    public final static String V3_PROFILE_BLACKLIST_DELETE = "api/v3/profile/blacklist/{fid}/";
    public final static String V3_FEED_ID = "api/v3/feeds/{fid}/";
    /*API v4*/

    public static final String V4_REGIONS = "api/v4/regions/";

}
