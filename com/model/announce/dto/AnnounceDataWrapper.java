package anews.com.model.announce.dto;

public class AnnounceDataWrapper {

    private AnnounceDataType dataType;
    private Object data;

    public AnnounceDataWrapper(AnnounceDataType dataType, Object data) {
        this.dataType = dataType;
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public AnnounceDataType getDataType() {
        return dataType;
    }

}
