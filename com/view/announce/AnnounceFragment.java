package anews.com.views.announce;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.ads.AdError;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdsManager;
import com.uservoice.uservoicesdk.UserVoice;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.nativeads.NativeAdLoader;
import com.yandex.mobile.ads.nativeads.NativeAdLoaderConfiguration;
import com.yandex.mobile.ads.nativeads.NativeAppInstallAd;
import com.yandex.mobile.ads.nativeads.NativeContentAd;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import anews.com.R;
import anews.com.analytic.Analytics;
import anews.com.interfaces.AnnouncePaginationListener;
import anews.com.interfaces.AnnouncePostListener;
import anews.com.interfaces.LoadAdsCallback;
import anews.com.interfaces.OnActiveScrollingPositionListener;
import anews.com.interfaces.OnAdapterClickListener;
import anews.com.interfaces.OnAnnouncesHelpActionListener;
import anews.com.interfaces.OnCategorySourceClickListener;
import anews.com.interfaces.OnViewScreenListener;
import anews.com.model.ads.dto.AdsNetworksData;
import anews.com.model.announce.FullLoadAnnouncesInfo;
import anews.com.model.announce.NewAnnouncesInfo;
import anews.com.model.announce.OldAnnouncesInfo;
import anews.com.model.announce.dto.AnnounceDataType;
import anews.com.model.announce.dto.AnnounceDataWrapper;
import anews.com.model.announce.dto.AnnounceHelpVHType;
import anews.com.model.announce.dto.AnnounceVHItem;
import anews.com.model.announce.dto.NewsAnnounceRunnable;
import anews.com.model.categories.dto.CategorySourceData;
import anews.com.model.news.dto.PostData;
import anews.com.model.blacklist.BlackListInfo;
import anews.com.network.ModelBase;
import anews.com.network.ModelData;
import anews.com.network.ModelError;
import anews.com.preferences.GlobalPreferences;
import anews.com.preferences.NotClearablePreferenses;
import anews.com.preferences.ProfilePreferences;
import anews.com.utils.AppStateFragment;
import anews.com.utils.AppUtils;
import anews.com.utils.ui.CircularProgressView;
import anews.com.utils.ui.ExVerticalLinearLayoutManager;
import anews.com.utils.ui.scroller.ExFastScroller;
import anews.com.views.announce.adapters.vertical.AnnounceVerticalAdapter;
import anews.com.views.main.MainActivity;
import anews.com.views.news.FullNewsActivity;
import anews.com.views.settings.SettingsActivity;
import anews.com.views.source.SingleSourceFragment;
import anews.com.views.source.SingleSourcePostsActivity;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

import static android.app.Activity.RESULT_OK;

public class AnnounceFragment extends AppStateFragment implements SwipeRefreshLayout.OnRefreshListener,
        AnnouncePostListener, AnnouncePaginationListener, OnViewScreenListener<AnnounceVHItem>,
        OnAnnouncesHelpActionListener, OnCategorySourceClickListener, LoadAdsCallback, OnAdapterClickListener {
    public static final long MONTH = 30 * 24 * 60 * 60 * 1000L;//30 дней  / 24 часа/ 1час// 1мин
    public static final long FOUR_DAY = 4 * 24 * 60 * 60 * 1000L;//4 дня  / 24 часа/ 1час// 1мин
    public static final String TAG = AnnounceFragment.class.getSimpleName();
    public static final String BUNDLE_ANNOUNCE_VERTICAL_POSITION = "announce_open_position";
    private OldAnnouncesInfo mOldAnnouncesInfo = getModel().getOldAnnouncesInfo();
    private NewAnnouncesInfo mNewAnnouncesInfo = getModel().getNewAnnouncesInfo();
    private BlackListInfo mBlackListInfo = getModel().getBlackListInfo();
    private NativeAdsManager mFacebookAdsLoader;
    private NativeAdLoader mYandexAdsLoader;
    private FullLoadAnnouncesInfo mFullLoadAnnouncesInfo = getModel().getFullLoadAnnouncesInfo();
    private LinkedHashMap<Integer, NewsAnnounceRunnable> mActiveLoadItems = new LinkedHashMap<>();
    private RecyclerView mRecyclerVertical;
    private AnnounceVerticalAdapter mAnnounceAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private int mTotalScrolledY;
    private CircularProgressView mProgressView;
    private MaterialProgressBar mHorizontalProgressBar;
    private boolean isNeedUpdate = false;
    private int mAnnounceOpenAdapterPosition = -1;
    private int mScrollPosition = -1;
    private boolean isNeedUpdateAlpha;
    private int mTopHeight;

    public static AnnounceFragment newInstance() {
        Bundle args = new Bundle();
        AnnounceFragment fragment = new AnnounceFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_announce, container, false);
        mHorizontalProgressBar = view.findViewById(R.id.progress_bar_horizontal);
        mRecyclerVertical = view.findViewById(R.id.recycler_view_announce_cluster);
        mRecyclerVertical.addOnScrollListener(mRecyclerScrollListener);
        mRecyclerVertical.setHasFixedSize(true);
        ExVerticalLinearLayoutManager linearLayoutManager = new ExVerticalLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false) {
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }
        };
        mRecyclerVertical.setLayoutManager(linearLayoutManager);
        if (mAnnounceAdapter == null) {
            mAnnounceAdapter = new AnnounceVerticalAdapter(this, this, this, this, this, this);
            mAnnounceAdapter.setOnViewScreenListener(this);
            mAnnounceAdapter.setOnActiveScrollingPosition(mOnActiveScrollingPosition);
        }

        mRecyclerVertical.setAdapter(mAnnounceAdapter);
        mRecyclerVertical.setItemViewCacheSize(20);
        mRecyclerVertical.setDrawingCacheEnabled(true);
        mRecyclerVertical.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
//        ExFastScroller fastScroller = view.findViewById(R.id.recycler_fast_scroller);
//        fastScroller.setRecyclerView(mRecyclerVertical);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.black);
        mSwipeRefreshLayout.setProgressViewOffset(true, 0, AppUtils.dpToPx(66));
        mProgressView = view.findViewById(R.id.progress_view);
        isNeedUpdate = false;

        if (getSavedInstanceState() != null) {
            if (getSavedInstanceState().containsKey(BUNDLE_ANNOUNCE_VERTICAL_POSITION)) {
                mAnnounceOpenAdapterPosition = getSavedInstanceState().getInt(BUNDLE_ANNOUNCE_VERTICAL_POSITION);
            }
        }
        mTopHeight = (int) getResources().getDimension(R.dimen.announce_top_height);
        initFacebookAds();
        initYandexAds();
        loadYAAd();
        loadFaceBookAd();
        return view;

    }

    public RecyclerView.OnScrollListener mRecyclerScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            mRecyclerVertical.post(new Runnable() {
                @Override
                public void run() {
                    if (isNeedUpdateAlpha) {
                        if (getActivity() != null && getActivity() instanceof MainActivity) {
                            LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerVertical.getLayoutManager();
                            if (linearLayoutManager != null && linearLayoutManager.findFirstVisibleItemPosition() == 0) {
                                mTotalScrolledY = mRecyclerVertical.computeVerticalScrollOffset();
                            } else {
                                mTotalScrolledY = mTopHeight;
                            }

                            ((MainActivity) getActivity()).updateAlphaToolBarAndShadow(mTotalScrolledY);
                        }

                    }
                }
            });


        }

    };

    private void checkTopView() {
        isNeedUpdateAlpha = ProfilePreferences.getInstance().isNeedShowTop();
        if (isNeedUpdateAlpha) {
            mRecyclerVertical.setPadding(0, AppUtils.dpToPx(0), 0, AppUtils.dpToPx(2));
        } else {
            mRecyclerVertical.setPadding(0, AppUtils.dpToPx(56), 0, AppUtils.dpToPx(2));
            if (getActivity() != null && getActivity() instanceof MainActivity) {
                ((MainActivity) getActivity()).disableAlphaToolbar(true);
            }
        }
    }

    @Override
    public void onResume() {
        registerModelListener(mOldAnnouncesInfo, mOldAnnounceListener);
        registerModelListener(mNewAnnouncesInfo, mNewAnnounceListener);
        registerModelListener(mFullLoadAnnouncesInfo, mFullLoadAnnounceListener);
        registerModelListener(mBlackListInfo, mTopBlackListListener);
        super.onResume();
        setActionBarTitle(R.string.app_name);
        checkTopView();
        mBlackListInfo.getBlackList();
        if (mAnnounceAdapter == null || mAnnounceAdapter.getItemCount() == 0
                || GlobalPreferences.getInstance().isAnnouncesItemsChanges()) {
            mActiveLoadItems = new LinkedHashMap<>();
            mProgressView.setVisibility(View.VISIBLE);
            mSwipeRefreshLayout.setVisibility(View.GONE);
            mFullLoadAnnouncesInfo.getAllAnnouncesFromCache();
        } else {
            if (isNeedUpdate && mActiveLoadItems != null && mActiveLoadItems.size() > 0) {
                for (Map.Entry<Integer, NewsAnnounceRunnable> entry : mActiveLoadItems.entrySet()) {
                    if ((System.currentTimeMillis() - entry.getValue().getItem().getLastTimeUpdate()) > 60 * 1000
                            && entry.getValue().getItem().getAdapterPosition() != mAnnounceOpenAdapterPosition) { // 2 minutes
                        NewsAnnounceRunnable announceRunnable = entry.getValue();
                        entry.setValue(announceRunnable);
                        mHandler.postDelayed(entry.getValue(), 300);
                    }
                }
            }
            if (mFullLoadAnnouncesInfo.isUpdating() && mSwipeRefreshLayout.getVisibility() == View.VISIBLE) {
                mSwipeRefreshLayout.setRefreshing(true);

            }
        }
        if (GlobalPreferences.getInstance().isNeedUpdate()) {
            mAnnounceAdapter.update();
            GlobalPreferences.getInstance().setUpdate(false);
        }
        Analytics.openPage(Analytics.OPEN_SCREEN_NEWS);
    }

    @Override
    public void onPause() {
        super.onPause();
        mOldAnnouncesInfo.removeListener(mOldAnnounceListener, false);
        mNewAnnouncesInfo.removeListener(mNewAnnounceListener, false);
        mFullLoadAnnouncesInfo.removeListener(mFullLoadAnnounceListener, false);
        mBlackListInfo.removeListener(mTopBlackListListener, false);
        mSwipeRefreshLayout.setRefreshing(false);
        mHorizontalProgressBar.setVisibility(View.GONE);
        isNeedUpdate = true;
        mFullLoadAnnouncesInfo.unsubscribeNetworkLoader();
    }

    @Override
    public void saveState() {
        if (mAnnounceOpenAdapterPosition >= 0) {
            Bundle bundle = new Bundle();
            bundle.putInt(BUNDLE_ANNOUNCE_VERTICAL_POSITION, mAnnounceOpenAdapterPosition);
            setSaveState(bundle);
        }
    }

    public void scrollAnnounces() {
        if (mAnnounceAdapter.getItemCount() >= mAnnounceOpenAdapterPosition && mScrollPosition >= 0) {
            AnnounceVHItem announceVHItem = mAnnounceAdapter.getItem(mAnnounceOpenAdapterPosition);
            mAnnounceAdapter.scrollToAnnouncesPosition(announceVHItem, mScrollPosition);
        }
    }

    private ModelBase.Listener mTopBlackListListener = new ModelBase.Listener<AnnounceDataWrapper, Void>() {

        @Override
        public void onStateChanged(ModelData<AnnounceDataWrapper, Void> data) {
            if (!mBlackListInfo.isUpdating()) {
                GlobalPreferences.getInstance().setIsAnnouncesItemsChanges(true);
            }
        }

        @Override
        public void onError(@NonNull ModelError error) {

        }
    };

    private ModelBase.Listener mOldAnnounceListener = new ModelBase.Listener<AnnounceDataWrapper, Void>() {

        @Override
        public void onStateChanged(ModelData<AnnounceDataWrapper, Void> data) {
            if (mOldAnnouncesInfo.isUpdating()) {
                mHorizontalProgressBar.setVisibility(View.VISIBLE);
            } else if (mOldAnnouncesInfo.isNext()) {
                if (data != null && data.getData().getDataType() == AnnounceDataType.ON_NEXT) {
                    mAnnounceAdapter.addOldAnnounces((AnnounceVHItem) data.getData().getData());
                }
            }
        }

        @Override
        public void onError(@NonNull ModelError error) {
            mHorizontalProgressBar.setVisibility(View.GONE);
        }
    };

    private ModelBase.Listener mNewAnnounceListener = new ModelBase.Listener<AnnounceDataWrapper, Void>() {

        @Override
        public void onStateChanged(ModelData<AnnounceDataWrapper, Void> data) {
            if (mNewAnnouncesInfo.isUpdating()) {
                mHorizontalProgressBar.setVisibility(View.VISIBLE);
            } else if (mNewAnnouncesInfo.isNext()) {
                if (data != null && data.getData().getDataType() == AnnounceDataType.ON_NEXT) {
                    mAnnounceAdapter.addNewAnnounces((AnnounceVHItem) data.getData().getData());
                }
            } else if (mNewAnnouncesInfo.isCompleted()) {
                mHorizontalProgressBar.setVisibility(View.GONE);
            }
        }

        @Override
        public void onError(@NonNull ModelError error) {
            mHorizontalProgressBar.setVisibility(View.GONE);
        }
    };


    private ModelBase.Listener mFullLoadAnnounceListener = new ModelBase.Listener<List<AnnounceVHItem>, Void>() {

        @Override
        public void onStateChanged(ModelData<List<AnnounceVHItem>, Void> data) {
            if (mFullLoadAnnouncesInfo.isUpdating()) {
                if (mSwipeRefreshLayout.getVisibility() == View.VISIBLE) {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            } else {
                GlobalPreferences.getInstance().setIsAnnouncesItemsChanges(false);
                mAnnounceAdapter.addAllAnnounces(data.getData());
                if (mSwipeRefreshLayout.getVisibility() == View.GONE) {
                    mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                } else {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
                mProgressView.setVisibility(View.GONE);
            }
        }

        @Override
        public void onError(@NonNull ModelError error) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    };

    @Override
    public void onRefresh() {
        mFullLoadAnnouncesInfo.getAllNewAnnounces();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
//        StaticUIHelper.cleanAnnouncesCache();
        if (mAnnounceAdapter != null) {
            mAnnounceAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void postClicked(AnnounceVHItem announceVHItem, PostData postData) {
        Analytics.trackEvent(getActivity(), Analytics.ACTION_CLICK, Analytics.CATEGORY_NEWS_PREVIEW, Analytics.ACTION_CLICK, postData.getLinkForTack(),
                Analytics.buildParamsForPostSelected(getContext(), postData));
        mAnnounceOpenAdapterPosition = announceVHItem.getAdapterPosition();
        Intent intent = new Intent(getActivity(), FullNewsActivity.class);
        intent.putExtra(FullNewsActivity.EXTRA_CATEGORY_SOURCE_DATA, announceVHItem.getCategorySourceData());
        intent.putExtra(FullNewsActivity.EXTRA_ANNOUNCE_ID, postData.getId());
        getActivity().startActivityFromFragment(this, intent, MainActivity.REQUEST_CODE_OPEN_ANNOUNCE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (RESULT_OK == resultCode) {
            switch (requestCode) {
                case MainActivity.REQUEST_CODE_OPEN_ANNOUNCE:
                    mScrollPosition = data.getExtras().getInt(MainActivity.RESULT_ANNOUNCE_BACK_POSITION);
                    if (mAnnounceAdapter != null && mAnnounceAdapter.getItemCount() > 0) {
                        scrollAnnounces();
                    }
                    break;
            }
        }
    }

    @Override
    public void paginationItem(AnnounceVHItem announceVHItem) {
        mOldAnnouncesInfo.getOldPosts(announceVHItem);
    }

    @Override
    public void onItemAttached(AnnounceVHItem item) {
        if (item == null) {
            return;
        }
        if ((System.currentTimeMillis() - item.getLastTimeUpdate()) > 60 * 1000) { // 2 minutes
            NewsAnnounceRunnable announceRunnable = new NewsAnnounceRunnable(item, mNewAnnouncesInfo);
            mActiveLoadItems.put(item.getAdapterPosition(), announceRunnable);
            mHandler.postDelayed(announceRunnable, 300);
        }
    }

    @Override
    public void onItemDetached(AnnounceVHItem item) {
        if (mActiveLoadItems.containsKey(item.getAdapterPosition())) {
            mHandler.removeCallbacks(mActiveLoadItems.get(item.getAdapterPosition()));
            mActiveLoadItems.remove(item.getAdapterPosition());
        }
    }

    private OnActiveScrollingPositionListener mOnActiveScrollingPosition = new OnActiveScrollingPositionListener() {
        @Override
        public void onActivePosition(int position) {
//            if (mAnnounceOpenAdapterPosition >= 0 && mAnnounceOpenAdapterPosition != position
//                    && mAnnounceOpenAdapterPosition <= mAnnounceAdapter.getItemCount()) {
//                onItemAttached(mAnnounceAdapter.getItem(mAnnounceOpenAdapterPosition));
//            }
            mAnnounceOpenAdapterPosition = position;
        }
    };

    private Handler mHandler = new Handler();

    @Override
    public void onAnnounceHelpAction(AnnounceHelpVHType action, int position) {
        switch (action) {
            case SHOW_CATALOG:
                ((MainActivity) getActivity()).showCatalog();
                break;
            case OPEN_GOOGLE_PLAY:
                rateApp(position);
                break;
            case RATE_REMIND_LATER:
                rateAppRemindLater(position);
                break;
            case CLOSE_ON_BOARDING:
                closeOnBoarding(position);
                break;
            case SHOW_FEEDBACK:
                UserVoice.launchUserVoice(getContext());
                Analytics.openPage(Analytics.OPEN_SCREEN_FEEDBACK);
                break;
            case SHOW_SETTINGS_ANNOUNCE_STYLE:
                Intent intent = new Intent(getActivity(), SettingsActivity.class);
                intent.putExtra(SettingsActivity.SHOW_ONLY_STYLE, SettingsActivity.SHOW_ONLY_STYLE);
                startActivity(intent);
                break;
            case SHOW_EVENTS:
                ((MainActivity) getActivity()).showEventsFragment();
                break;
            case SHOW_CATALOG_FOR_ON_BOARDING:
                ((MainActivity) getActivity()).showCatalogFragment();
                break;
        }
    }


    @Override
    public void onCategoryClicked(CategorySourceData data) {
        if (data != null) {
            Analytics.trackEvent(getContext(), Analytics.ACTION_FEED_VIEW, Analytics.CATEGORY_NEWS_PREVIEW,
                    Analytics.ACTION_FEED_VIEW, data.getTitle());
            Intent intent = new Intent(getActivity(), SingleSourcePostsActivity.class);
            intent.putExtra(SingleSourceFragment.ARGS_CATEGORY_SOURCE_DATA, data);
            getActivity().startActivity(intent);

        }
    }

    public void scrollToTop() {
        mRecyclerVertical.smoothScrollToPosition(0);
    }

    private void initYandexAds() {
        String ydAdsKey = getModel().getAdsInfo().getMainAdsKey(AdsNetworksData.AdsType.YA);
        if (ydAdsKey != null) {
            NativeAdLoaderConfiguration adLoaderConfiguration =
                    new NativeAdLoaderConfiguration.Builder(ydAdsKey, true)
                            .setImageSizes(NativeAdLoaderConfiguration.NATIVE_IMAGE_SIZE_LARGE)
                            .build();
            mYandexAdsLoader = new NativeAdLoader(getActivity(), adLoaderConfiguration);

            mYandexAdsLoader.setOnLoadListener(new NativeAdLoader.OnLoadListener() {
                @Override
                public void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
                    AppUtils.logD("ADS onAdFailedToLoad ", adRequestError.getDescription());
                }

                @Override
                public void onAppInstallAdLoaded(@NonNull NativeAppInstallAd nativeAppInstallAd) {
                    if (mAnnounceAdapter != null) {
                        mAnnounceAdapter.addAd(nativeAppInstallAd);
                    }
                }

                @Override
                public void onContentAdLoaded(@NonNull NativeContentAd nativeContentAd) {
                    if (mAnnounceAdapter != null) {
                        mAnnounceAdapter.addAd(nativeContentAd);
                    }
                }
            });
        }
    }

    private void initFacebookAds() {
        String faceBookKey = getModel().getAdsInfo().getMainAdsKey(AdsNetworksData.AdsType.FB);
        if (faceBookKey != null) {
            mFacebookAdsLoader = new NativeAdsManager(getActivity(), faceBookKey, 5);
            mFacebookAdsLoader.setListener(new NativeAdsManager.Listener() {
                @Override
                public void onAdsLoaded() {
                    if (mAnnounceAdapter != null) {
                        mAnnounceAdapter.addAd(mFacebookAdsLoader);
                    }
                }

                @Override
                public void onAdError(AdError adError) {
                    AppUtils.logE("onAdError", adError.getErrorMessage());
                }
            });
        }
    }

    private void loadYandexAds() {
        mYandexAdsLoader.loadAd(new AdRequest.Builder().build());
    }

    private void loadFacebookAds() {
        mFacebookAdsLoader.loadAds(NativeAd.MediaCacheFlag.ALL);
    }

    @Override
    public void loadFaceBookAd() {
        loadFacebookAds();
    }

    @Override
    public void loadYAAd() {
        loadYandexAds();
    }

    public void clearAd() {
        mActiveLoadItems = new LinkedHashMap<>();
        mProgressView.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setVisibility(View.GONE);
        mFullLoadAnnouncesInfo.getAllAnnouncesFromCache();
    }

    @Override
    public void onAdapterClickItem(View view) {
        switch (view.getId()) {
            case R.id.image_remove_ads:
                if (getActivity() instanceof MainActivity) {
                    ((MainActivity) getActivity()).inAppPurchases();
                }
                break;
        }
    }

    public void rateApp(int position) {
        try {
            Intent rateIntent = rateIntentForUrl("market://details");
            startActivity(rateIntent);
        } catch (ActivityNotFoundException e) {
            Intent rateIntent = rateIntentForUrl("https://play.google.com/store/apps/details");
            startActivity(rateIntent);
        }
        NotClearablePreferenses.getInstance().setAppRateShowTime(-2);
        mAnnounceAdapter.removeItemList(position);
        mAnnounceAdapter.update();
        Analytics.trackEvent(getContext(), Analytics.CATEGORY_SETTINGS,
                Analytics.CATEGORY_NEWS_PREVIEW, Analytics.ACTION_RATE_APP, Analytics.LABEL_GOOGLE_PLAY, Analytics.buildEmptyParams(getActivity()));
    }

    private Intent rateIntentForUrl(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("%s?id=%s", url, getActivity().getPackageName())));
        int flags = Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK;
        if (Build.VERSION.SDK_INT >= 21) {
            flags |= Intent.FLAG_ACTIVITY_NEW_DOCUMENT;
        } else {
            flags |= Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
        }
        intent.addFlags(flags);
        return intent;
    }

    public void rateAppRemindLater(int position) {
        NotClearablePreferenses.getInstance().setAppRateShowTime(System.currentTimeMillis() + MONTH);
        mAnnounceAdapter.removeItemList(position);
        mAnnounceAdapter.update();
        Analytics.trackEvent(getContext(), Analytics.CATEGORY_SETTINGS,
                Analytics.CATEGORY_NEWS_PREVIEW, Analytics.ACTION_RATE_APP, Analytics.LABEL_LATER, Analytics.buildEmptyParams(getActivity()));
    }

    public void closeOnBoarding(int position) {
        NotClearablePreferenses.getInstance().setNotNeedShowOnboarding();
        //  NotClearablePreferenses.getInstance().setAppRateShowTime(System.currentTimeMillis() + FOUR_DAY);
        mAnnounceAdapter.removeItemList(position);
    }

}

