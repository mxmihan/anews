package anews.com.views.settings.adapters.vertical;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import anews.com.R;
import anews.com.interfaces.OnSettingsSelectListener;
import anews.com.model.preferences.dto.AnnounceStyleType;

public class AnnounceStyleVerticalAdapter extends RecyclerView.Adapter<AbsAnnounceStyleVerticalVH> implements OnSettingsSelectListener {
    private ArrayList<AnnounceVHStyleItem> mItemVerticalList;
    private OnSettingsSelectListener mOnSettingsSelectListener;

    public AnnounceStyleVerticalAdapter(OnSettingsSelectListener listeter) {
        mOnSettingsSelectListener = listeter;
        setData();
    }

    @NonNull
    @Override
    public AbsAnnounceStyleVerticalVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_announce_style_vertical_item, parent, false);
        return new AnnounceStyleVerticalVH(view, this);
    }

    @Override
    public void onBindViewHolder(@NonNull AbsAnnounceStyleVerticalVH holder, int position) {
        holder.setData(mItemVerticalList.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return mItemVerticalList.get(position).getAnnounceStyleType().ordinal();
    }

    @Override
    public int getItemCount() {
        return mItemVerticalList.size();
    }

    private void setData() {
        mItemVerticalList = new ArrayList<>();
        for (AnnounceStyleType announceStyleType : AnnounceStyleType.values()) {
            mItemVerticalList.add(new AnnounceVHStyleItem(announceStyleType));
        }
    }

    @Override
    public void onSettingsSelectItem(AnnounceStyleType announceStyleType) {
        notifyDataSetChanged();
        mOnSettingsSelectListener.onSettingsSelectItem(announceStyleType);
    }
}


