package anews.com.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;

import java.util.Hashtable;

public class FontsUtils {
    public final static int MONTSERRAT_MEDIUM = 0;
    public final static int MONTSERRAT_REGULAR = 1;
    public final static int MONTSERRAT_SEMI_BOLD = 3;

    private static final Hashtable<String, Typeface> cache = new Hashtable<>();

    public static Typeface getTypeFace(Context context, int typeFace) {
        synchronized (cache) {
            String assetPath;
            switch (typeFace) {
                case MONTSERRAT_MEDIUM:
                default:
                    assetPath = "fonts/Montserrat-Medium.ttf";
                    break;
                case MONTSERRAT_REGULAR:
                    assetPath = "fonts/Montserrat-Regular.ttf";
                    break;

                case MONTSERRAT_SEMI_BOLD:
                    assetPath = "fonts/Montserrat-SemiBold.ttf";
                    break;

            }
            if (!cache.containsKey(assetPath)) {
                try {
                    cache.put(assetPath, Typeface.createFromAsset(context.getAssets(), assetPath));
                } catch (Exception e) {
                    Log.e("TypeFaces", "Typeface not loaded.");
                    return null;
                }
            }
            return cache.get(assetPath);
        }
    }
}
