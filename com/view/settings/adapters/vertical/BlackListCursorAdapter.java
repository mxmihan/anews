package anews.com.views.settings.adapters.vertical;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.j256.ormlite.stmt.PreparedQuery;

import anews.com.R;
import anews.com.model.blacklist.dto.BlackListData;
import anews.com.utils.adapters.AbsOrmliteCursorAdapter;
import anews.com.utils.adapters.AbsSwipeableVH;

public class BlackListCursorAdapter extends AbsOrmliteCursorAdapter<BlackListData, AbsSwipeableVH> {
    public BlackListCursorAdapter(Cursor cursor, PreparedQuery<BlackListData> pq) {
        super(cursor, pq);

    }
    @Override
    public void onBindViewHolder(final AbsSwipeableVH holder, BlackListData blackListData) {
        if (holder instanceof BlackListVerticalVH) {
            BlackListVerticalVH blackListVerticalVH = (BlackListVerticalVH) holder;
            blackListVerticalVH.bindView(blackListData);
        }
    }
    @NonNull
    @Override
    public AbsSwipeableVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BlackListVerticalVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_black_list_item, parent, false));
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public boolean isNeedShowHelp() {
        return false;
    }

    @Override
    public void onBindViewHolder(AbsSwipeableVH holder) {
    }
}


