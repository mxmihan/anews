package anews.com.views.settings.adapters.horizontal;

import android.view.View;
import android.widget.LinearLayout;

import anews.com.R;
import anews.com.interfaces.OnSettingsSelectListener;
import anews.com.model.preferences.dto.AnnounceStyleType;
import anews.com.utils.StaticUIHelper;

public class OneCardLargeImageVH extends AbsAnnounceStyleHorizontalVH implements View.OnClickListener {

    private LinearLayout mDescriptionContainer;
    private AnnounceStyleType mAnnounceStyleType;
    private OnSettingsSelectListener mOnSettingsSelectListener;

    public OneCardLargeImageVH(View itemView, OnSettingsSelectListener listener) {
        super(itemView);
        mOnSettingsSelectListener = listener;
        mDescriptionContainer = itemView.findViewById(R.id.container_description_text);
        mDescriptionContainer.setVisibility(View.GONE);
        itemView.setOnClickListener(this);
        itemView.setLayoutParams(StaticUIHelper.getAnnounceRelativeParamsForOneCards());
    }

    @Override
    public void onClick(View v) {
        mOnSettingsSelectListener.onSettingsSelectItem(mAnnounceStyleType);
    }

    @Override
    public void setData(AnnounceStyleType data) {
        mAnnounceStyleType = data;
    }
}
