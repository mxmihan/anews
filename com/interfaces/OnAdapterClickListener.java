package anews.com.interfaces;

import android.view.View;

public interface OnAdapterClickListener {

    void onAdapterClickItem(View view);
}
