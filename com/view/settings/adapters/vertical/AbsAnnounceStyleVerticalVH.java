package anews.com.views.settings.adapters.vertical;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class AbsAnnounceStyleVerticalVH extends RecyclerView.ViewHolder {

    public AbsAnnounceStyleVerticalVH(View itemView) {
        super(itemView);
    }

    public abstract void setData(AnnounceVHStyleItem data);

    public abstract int getItemPosition();

}
