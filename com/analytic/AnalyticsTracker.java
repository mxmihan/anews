package anews.com.analytic;

import android.app.Activity;
import android.app.Application;

import com.appsflyer.AppsFlyerLib;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.HitBuilders.EventBuilder;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import anews.com.App;

public class AnalyticsTracker {

    private static AnalyticsTracker tracker;

    private Tracker mTrackerGoogle;


    private AnalyticsTracker() {

    }

    public static AnalyticsTracker getInstance() {
        if (tracker == null) {
            tracker = new AnalyticsTracker();
        }
        return tracker;
    }

    public void dispatchOpenAndStart(Application context, String googleId, boolean isRegistered) {
        GoogleAnalytics.getInstance(context).enableAutoActivityReports(context);
        mTrackerGoogle = GoogleAnalytics.getInstance(context).newTracker(googleId);
        mTrackerGoogle.set("&cd10", isRegistered ? "registered" : "unregistered");
    }

    public void startSession(Activity context) {
        GoogleAnalytics.getInstance(context).reportActivityStart(context);
        FlurryAgent.onStartSession(context);
    }

    public void endSession(Activity context) {
        GoogleAnalytics.getInstance(context).dispatchLocalHits();
        FlurryAgent.onEndSession(context);
        GoogleAnalytics.getInstance(context).reportActivityStop(context);
    }

    public void trackPageView(String pageName) {
        mTrackerGoogle.setPage(pageName);
        mTrackerGoogle.setScreenName(pageName);
        mTrackerGoogle.send(new HitBuilders.ScreenViewBuilder().build());
        FlurryAgent.logEvent(pageName);
        AppsFlyerLib.getInstance().trackEvent(App.getInstance(), pageName, null);
    }

    public void trackEvent(String actionName, Map<String, String> params) {
        if (params == null) {
            FlurryAgent.logEvent(actionName);
        } else {
            FlurryAgent.logEvent(actionName, params);
        }
    }

    public void trackEvent(String category, String actionName, String label, Map<Integer, String> params) {
        EventBuilder setLabel = new HitBuilders.EventBuilder().setCategory(category).setAction(actionName).setLabel(label);
        Map<String, Object> newMap = new HashMap<>();
        if (params != null) {
            for (Map.Entry<Integer, String> entry : params.entrySet()) {
                newMap.put(String.valueOf(entry.getKey()), entry.getValue());
            }

            Set<Integer> keySet = params.keySet();
            for (Integer key : keySet) {
                setLabel.setCustomDimension(key, params.get(key));
            }
        }

        AppsFlyerLib.getInstance().trackEvent(App.getInstance(), actionName, newMap);
        mTrackerGoogle.send(setLabel.build());
    }
}
