package anews.com.views.profile;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import anews.com.App;
import anews.com.R;
import anews.com.model.profile.ChangeUserNameInfo;
import anews.com.model.profile.dto.UserProfileData;
import anews.com.network.ModelBase;
import anews.com.network.ModelData;
import anews.com.network.ModelError;
import anews.com.preferences.ProfilePreferences;
import anews.com.utils.AppFragment;
import anews.com.utils.AppUtils;
import anews.com.utils.ui.CircularProgressView;

public class ProfileNameFragment extends AppFragment implements View.OnClickListener {

    public static final String TAG = ProfileNameFragment.class.getSimpleName();

    private EditText mEditViewFirstName;
    private EditText mEditViewLastName;
    private CircularProgressView mProgressView;
    private View mViewFirstName;
    private ChangeUserNameInfo mChangeUserNameInfo;
    private View mProgressBar;
    private Button mButtonSave;

    public static ProfileNameFragment newInstance() {
        Bundle args = new Bundle();
        ProfileNameFragment fragment = new ProfileNameFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_name, container, false);
        if (mChangeUserNameInfo == null) {
            mChangeUserNameInfo = getModel().getChangeUserNameInfo();
        }
        mProgressBar = view.findViewById(R.id.progress_bar_horizontal);
        setActionBarTitle(R.string.str_fragment_title_edit_name);
        mButtonSave = view.findViewById(R.id.button_save);
        mButtonSave.setOnClickListener(this);
        mEditViewFirstName = view.findViewById(R.id.edit_text_first_name);
        mEditViewLastName = view.findViewById(R.id.edit_text_last_name);
        mProgressView = view.findViewById(R.id.progress_view);
        mProgressView.setVisibility(View.GONE);
        mViewFirstName = view.findViewById(R.id.text_input_layout_firstname);
        mEditViewFirstName.setText(ProfilePreferences.getInstance().getFirstName());
        mEditViewLastName.setText(ProfilePreferences.getInstance().getLastName());

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_save:
                changeUserName();
                break;

        }
    }

    @Override
    public void onResume() {
        registerModelListener(mChangeUserNameInfo, mChangeNameListener);
        super.onResume();
        updateView();
    }

    @Override
    public void onPause() {
        super.onPause();
        mChangeUserNameInfo.removeListener(mChangeNameListener, false);
    }

    @Override
    public void updateView() {
        mButtonSave.setEnabled(!mChangeUserNameInfo.isUpdating());
        mEditViewFirstName.setEnabled(!mChangeUserNameInfo.isUpdating());
        mEditViewLastName.setEnabled(!mChangeUserNameInfo.isUpdating());
        mProgressBar.setVisibility(!mChangeUserNameInfo.isUpdating() ? View.GONE : View.VISIBLE);
    }

    private ModelBase.Listener mChangeNameListener = new ModelBase.Listener<UserProfileData, Void>() {
        @Override
        public void onStateChanged(ModelData<UserProfileData, Void> data) {
            updateView();
            if (!mChangeUserNameInfo.isUpdating()) {
                if (data != null && data.getData() != null) {
                    App.getInstance().showToast(R.string.str_change_name_successful, true);
                }

            }
        }

        @Override
        public void onError(@NonNull ModelError error) {
            updateView();
            switch (error) {
                case BadNetworkConnection:
                    getApp().showToast(getString(R.string.str_connection_error), false);
                default:
                    getApp().showToast(getString(R.string.error_unknown), false);
                    break;
            }
        }
    };

    public void changeUserName() {
        String textFirstName = mEditViewFirstName.getText().toString().trim();
        String textLastName = mEditViewLastName.getText().toString().trim();

        if (TextUtils.isEmpty(textFirstName)) {
            shakeView(mViewFirstName);
            getApp().showToast(getString(R.string.error_field_is_empty), true);
        } else {
            mChangeUserNameInfo.editUserNameProfile(textFirstName, textLastName);
            AppUtils.hideSoftKeyboard(getActivity());
        }
    }
}

