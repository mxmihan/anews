package anews.com.model.likes.dto;

public enum LikeState {
    LIKE,
    UNLIKE;
}
