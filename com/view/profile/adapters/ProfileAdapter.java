package anews.com.views.profile.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import anews.com.R;
import anews.com.interfaces.OnProfileAdapterClickListener;
import anews.com.preferences.ProfilePreferences;

public class ProfileAdapter extends RecyclerView.Adapter<AbsProfileHolder> {
    private ArrayList<ProfileAdapterItem> mItemList;
    private OnProfileAdapterClickListener mListener;

    public ProfileAdapter(OnProfileAdapterClickListener listener) {
        mListener = listener;
        updateMenu();
    }

    private void updateMenu() {
        mItemList = new ArrayList<>();
        mItemList.add(new ProfileAdapterItem(ProfileAdapterItem.Type.HEADER_ITEM));
        mItemList.add(new ProfileAdapterItem(ProfileAdapterItem.Type.BUTTON_ITEM, R.string.str_edit_avatar, R.drawable.ic_edit_avatar));
        mItemList.add(new ProfileAdapterItem(ProfileAdapterItem.Type.BUTTON_ITEM, R.string.str_edit_user_name, R.drawable.ic_edit_name));
        if (ProfilePreferences.getInstance().isEmailProvider()) {
            mItemList.add(new ProfileAdapterItem(ProfileAdapterItem.Type.BUTTON_ITEM, R.string.str_edit_password, R.drawable.ic_edit_password));
        }
        mItemList.add(new ProfileAdapterItem(ProfileAdapterItem.Type.BUTTON_ITEM, R.string.str_logout_account, R.drawable.ic_logout));
    }

    @NonNull
    @Override
    public AbsProfileHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ProfileAdapterItem.Type.HEADER_ITEM.ordinal()) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_profile_item_header, parent, false);
            return new ProfileHeaderViewHolder(view, mListener);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_profile_item_button, parent, false);
            return new ProfileButtonVievHolder(view, mListener);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull AbsProfileHolder holder, int position) {
        holder.populate(mItemList.get(position));
    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position).getType().ordinal();
    }

    public void updateHeader() {
        updateMenu();
    }

}
