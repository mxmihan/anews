package anews.com.views.events.adapters;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.j256.ormlite.stmt.PreparedQuery;

import anews.com.R;
import anews.com.interfaces.OnHelpClickListener;
import anews.com.interfaces.OnPositionClickListener;
import anews.com.interfaces.OnPostDataClickListener;
import anews.com.model.events.dto.EventPushData;
import anews.com.preferences.GlobalPreferences;
import anews.com.utils.adapters.AbsOrmliteCursorAdapter;
import anews.com.utils.adapters.AbsSwipeableVH;

public class EventsCursorAdapter extends AbsOrmliteCursorAdapter<EventPushData, AbsSwipeableVH> implements OnPositionClickListener {

    private OnHelpClickListener mHelpClickListener;
    private OnPostDataClickListener mListener;
    private SparseArray<EventsEmptyVHItem> mHoldersOnScreen = new SparseArray<>();

    public static final int ITEM_HELP = 0;
    public static final int ITEM_POST = 1;
    public static final int ITEM_EMPTY = 2;

    public EventsCursorAdapter(Cursor cursor, PreparedQuery<EventPushData> pq) {
        super(cursor, pq);
    }

    @NonNull
    @Override
    public AbsSwipeableVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ITEM_HELP) {
            return new EventsHelpVHItem(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_events_help_item, parent, false), mHelpClickListener);
        } else if (viewType == ITEM_EMPTY) {
            return new EventsEmptyVHItem(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_events_place_holder_item, parent, false));
        } else {
            return new EventsVHItem(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_events_item, parent, false), this);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isNeedShowHelp() && position == 0) {
            return ITEM_HELP;
        } else {
            if (getTypedItem(position - (isNeedShowHelp() ? 1 : 0)).getPostData() == null) {
                return ITEM_EMPTY;
            }
            return ITEM_POST;
        }
    }

    @Override
    public void onBindViewHolder(final AbsSwipeableVH holder, EventPushData inspection) {
        if (holder instanceof EventsVHItem) {
            EventsVHItem viewHolder = (EventsVHItem) holder;
            viewHolder.bindView(inspection);
        } else if (holder instanceof EventsEmptyVHItem) {
            ((EventsEmptyVHItem) holder).setData(inspection);
        }
    }


    @Override
    public boolean isNeedShowHelp() {
        return !GlobalPreferences.getInstance().isEventHelpRemoved();
    }

    @Override
    public void onBindViewHolder(AbsSwipeableVH holder) {

    }

    @Override
    public void onPositionClick(int position) {
        mListener.onPostClicked(getTypedItem(position - (isNeedShowHelp() ? 1 : 0)).getPostData());

    }

    @Override
    public void onViewDetachedFromWindow(@NonNull AbsSwipeableVH holder) {
        super.onViewDetachedFromWindow(holder);
        if (holder.getAdapterPosition() >= 0) {
            mHoldersOnScreen.remove(holder.getAdapterPosition());
        }
    }

    @Override
    public void onViewAttachedToWindow(@NonNull AbsSwipeableVH holder) {
        super.onViewAttachedToWindow(holder);
        if (holder.getAdapterPosition() >= 0 && holder instanceof EventsEmptyVHItem) {
            mHoldersOnScreen.put(holder.getAdapterPosition(), (EventsEmptyVHItem) holder);
        }
    }

    public SparseArray<EventsEmptyVHItem> getVisibleVHItems() {
        return mHoldersOnScreen;

    }


    public void setOnHelpListener(OnHelpClickListener onHelpListener) {
        mHelpClickListener = onHelpListener;
    }

    public void setOnPostClickListener(OnPostDataClickListener listener) {
        mListener = listener;
    }
}
