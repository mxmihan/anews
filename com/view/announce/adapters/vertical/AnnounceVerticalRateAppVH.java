package anews.com.views.announce.adapters.vertical;

import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import java.lang.ref.WeakReference;

import anews.com.R;
import anews.com.interfaces.OnAnnouncesHelpActionListener;
import anews.com.model.announce.dto.AnnounceHelpVHType;
import anews.com.model.announce.dto.AnnounceVHItem;
import anews.com.utils.FontsUtils;
import anews.com.utils.StaticUIHelper;

public class AnnounceVerticalRateAppVH extends AbsAnnounceVerticalVH implements View.OnClickListener {
    private WeakReference<OnAnnouncesHelpActionListener> mOnAdapterClickListener;
    private TextView mHeaderTitle;
    private TextView mDescriptionTitle;
    private TextView mRemindLater;

    public AnnounceVerticalRateAppVH(View itemView, OnAnnouncesHelpActionListener listener) {
        super(itemView);
        itemView.setLayoutParams(StaticUIHelper.getReateAppRelativeParamsCards());
        mOnAdapterClickListener = new WeakReference<>(listener);
        mHeaderTitle = itemView.findViewById(R.id.text_view_header_title);
        mDescriptionTitle = itemView.findViewById(R.id.text_view_description_title);
        mRemindLater = itemView.findViewById(R.id.text_viev_remind_later);
        mRemindLater.setOnClickListener(this);
        mHeaderTitle.setTypeface(FontsUtils.getTypeFace(itemView.getContext(), FontsUtils.MONTSERRAT_SEMI_BOLD));
        mDescriptionTitle.setTypeface(FontsUtils.getTypeFace(itemView.getContext(), FontsUtils.MONTSERRAT_REGULAR));
        mHeaderTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, StaticUIHelper.getTextSizePx() +
                itemView.getContext().getResources().getDimension(R.dimen.announces_text_title_size));
        mDescriptionTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, StaticUIHelper.getTextSizePx() +
                itemView.getContext().getResources().getDimension(R.dimen.announces_text_source_size));
        itemView.findViewById(R.id.view_container).setOnClickListener(this);

    }

    @Override
    public void setData(AnnounceVHItem data) {
    }

    @Override
    public int getItemPosition() {
        return -1;
    }

    @Override
    public RecyclerView getRecyclerView() {
        return null;
    }

    @Override
    public void onClick(View v) {
        if (mOnAdapterClickListener.get() != null) {
            switch (v.getId()) {
                case R.id.view_container:
                    mOnAdapterClickListener.get().onAnnounceHelpAction(AnnounceHelpVHType.OPEN_GOOGLE_PLAY, getAdapterPosition());
                    break;
                case R.id.text_viev_remind_later:
                    mOnAdapterClickListener.get().onAnnounceHelpAction(AnnounceHelpVHType.RATE_REMIND_LATER, getAdapterPosition());
                    break;
                default:
                    break;
            }
        }
    }
}