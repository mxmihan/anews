package anews.com.views.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import anews.com.R;

public class BottomDialogFragment extends BottomSheetDialogFragment implements View.OnClickListener {
    public static final String TAG = "BottomDialogFragment";

    public static BottomDialogFragment getInstance() {
        return new BottomDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_button_sheet, container, false);
        view.findViewById(R.id.container_create_photo).setOnClickListener(this);
        view.findViewById(R.id.container_load_from_gallery).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        if (getParentFragment() instanceof View.OnClickListener) {
            ((View.OnClickListener) getParentFragment()).onClick(view);
        }
        dismiss();
    }

}