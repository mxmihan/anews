package anews.com.model.profile.dto;

public class ResetPasswordResponse {
    private String status;

    public String getStatus() {
        return status;
    }
}
