package anews.com.views.settings.adapters.horizontal;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import anews.com.R;
import anews.com.interfaces.OnSettingsSelectListener;
import anews.com.model.preferences.dto.AnnounceStyleType;

public class AnnounceStyleHorizontalAdapter extends RecyclerView.Adapter<AbsAnnounceStyleHorizontalVH> {
    private AnnounceStyleType mAnnounceStyleType;
    private OnSettingsSelectListener mOnSettingsSelectListener;

    public AnnounceStyleHorizontalAdapter(AnnounceStyleType announceStyleType, OnSettingsSelectListener listener) {
        mAnnounceStyleType = announceStyleType;
        mOnSettingsSelectListener = listener;
    }

    @NonNull
    @Override
    public AbsAnnounceStyleHorizontalVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (AnnounceStyleType.values()[viewType]) {
            case TWO_CARD_LARGE_IMAGE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_announce_style_horizontal_item, parent, false);
                return new TwoCardLargeImageVH(view, mOnSettingsSelectListener);
            case TWO_CARD_SMALL_IMAGE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_announce_style_horizontal_item, parent, false);
                return new TwoCardSmallImageVH(view, mOnSettingsSelectListener);
            case ONE_CARD_LARGE_IMAGE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_announce_style_horizontal_item, parent, false);
                return new OneCardLargeImageVH(view, mOnSettingsSelectListener);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_announce_style_horizontal_item, parent, false);
                return new TwoCardLargeImageVH(view, mOnSettingsSelectListener);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mAnnounceStyleType.ordinal();
    }

    @Override
    public void onBindViewHolder(@NonNull AbsAnnounceStyleHorizontalVH holder, int position) {
        holder.setData(mAnnounceStyleType);
    }

    @Override
    public int getItemCount() {
        return 6;
    }
}