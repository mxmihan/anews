package anews.com.model.announce.dto;

import anews.com.R;

public enum OnBoardingType {

    ANNOUNCE_STYLE_SETTINGS_CARD(R.string.str_onboarding_app_heder_title_01, R.string.str_onboarding_app_description_title_01, R.drawable.ic_onboarding_1),
    EVENTS_CARD(R.string.str_onboarding_app_heder_title_02, R.string.str_onboarding_app_description_title_02, R.drawable.ic_onboarding_2),
    CATALOG_CARD(R.string.str_onboarding_app_heder_title_03, R.string.str_onboarding_app_description_title_03, R.drawable.ic_onboarding_3),
    FEED_BACK_CARD(R.string.str_onboarding_app_heder_title_04, R.string.str_onboarding_app_description_title_04, R.drawable.ic_onboarding_4);
    private int headerTitleResId;
    private int descriptionResId;
    private int drawableResId;

    OnBoardingType(int title, int description, int image) {
        this.headerTitleResId = title;
        this.descriptionResId = description;
        this.drawableResId = image;
    }

    public int getHeaderTitleResId() {
        return headerTitleResId;
    }

    public int getDescriptionResId() {
        return descriptionResId;
    }

    public int getDrawableResId() {
        return drawableResId;
    }

}