package anews.com.utils;

import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Hashtable;

import anews.com.App;
import anews.com.model.preferences.dto.AnnounceStyleType;
import anews.com.preferences.ProfilePreferences;

public class StaticUIHelper {
    private static final Integer ANNOUNCE_LINES_COUNT = 1;
    private static final Integer ANNOUNCE_STYLE_LINES_COUNT = 111;
    private static final Integer ANNOUNCE_NEED_MARK_POSTS = 2;
    private static final Integer ANNOUNCE_COLOR_FILTER_MARK_POSTS = 3;
    private static final Integer ANNOUNCE_ITEM_LAYOUT_PARAMS = 4;
    private static final Integer ANNOUNCE_ITEM_LAYOUT_PARAMS_ONE_CARD = 444;
    private static final Integer ANNOUNCE_ITEM_LAYOUT_PARAMS_RATE_APP = 445;
    private static final Integer ANNOUNCE_TOP_ITEM_LAYOUT_PARAMS = 5;

    private static final Integer TITLE_TEXT_SIZE = 6;
    private static final Integer TITLE_TOP_TEXT_SIZE = 7;
    private static final Integer TITLE_EVENT_TEXT_SIZE = 8;
    private static final Integer TITLE_BOOKMARKS_TEXT_SIZE = 9;
    private static final Integer TEXT_SIZE = 10;
    private static final Integer TEXT_SIZE_HUGO = 11;
    private static final Integer ANNOUNCE_NEED_SHOW_IMAGES = 12;

    private static final Hashtable<Integer, Object> cache = new Hashtable<>();

    public static Integer getAnnounceLinesCount(TextView view) {
        synchronized (cache) {
            if (!cache.containsKey(ANNOUNCE_LINES_COUNT)) {
                int width = AppUtils.isPortrait() ? App.getInstance().getScreenWidth() : App.getInstance().getScreenHeight();
                cache.put(ANNOUNCE_LINES_COUNT, (width - view.getPaddingLeft() - view.getPaddingLeft() - view.getPaddingBottom()) / 2 / view.getLineHeight());
            }
            return (Integer) cache.get(ANNOUNCE_LINES_COUNT);
        }
    }
    public static Integer getAnnounceStyleLinesCount(TextView view) {
        synchronized (cache) {
            if (!cache.containsKey(ANNOUNCE_STYLE_LINES_COUNT)) {
                int width = getAnnounceRelativeParams().width;
                cache.put(ANNOUNCE_STYLE_LINES_COUNT, (width - view.getPaddingLeft() - view.getPaddingLeft() - view.getPaddingBottom()) / 2 / view.getLineHeight());
            }
            return (Integer) cache.get(ANNOUNCE_STYLE_LINES_COUNT);
        }
    }
    public static Boolean isNeedMarkPosts() {
        synchronized (cache) {
            if (!cache.containsKey(ANNOUNCE_NEED_MARK_POSTS)) {
                cache.put(ANNOUNCE_NEED_MARK_POSTS, ProfilePreferences.getInstance().isNeedMarkReadNews());
            }
            return (Boolean) cache.get(ANNOUNCE_NEED_MARK_POSTS);
        }
    }

    public static void removeMarkPosts() {
        synchronized (cache) {
            cache.remove(ANNOUNCE_NEED_MARK_POSTS);
        }
    }

    public static ColorMatrixColorFilter getColorFilterForMarkPosts() {
        synchronized (cache) {
            if (!cache.containsKey(ANNOUNCE_COLOR_FILTER_MARK_POSTS)) {
                ColorMatrix colorMatrix = new ColorMatrix();
                colorMatrix.setSaturation(0);
                cache.put(ANNOUNCE_COLOR_FILTER_MARK_POSTS, new ColorMatrixColorFilter(colorMatrix));
            }
            return (ColorMatrixColorFilter) cache.get(ANNOUNCE_COLOR_FILTER_MARK_POSTS);
        }
    }

    public static RelativeLayout .LayoutParams getAnnounceRelativeParamsByStyle(){
        if (ProfilePreferences.getInstance().getStyleAnnounce() == AnnounceStyleType.ONE_CARD_LARGE_IMAGE) {
            return getAnnounceRelativeParamsForOneCards();
        } else {
            return getAnnounceRelativeParams();
        }
    }

    public static RelativeLayout.LayoutParams getAnnounceRelativeParams() {
        synchronized (cache) {
            if (!cache.containsKey(ANNOUNCE_ITEM_LAYOUT_PARAMS)) {
                int width = AppUtils.isPortrait() ? App.getInstance().getScreenWidth() : App.getInstance().getScreenHeight();
                int itemWidth = width / 2 - AppUtils.dpToPx(8);
                int itemHeight = width / 2 + AppUtils.dpToPx(48);
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(itemWidth, itemHeight);
                cache.put(ANNOUNCE_ITEM_LAYOUT_PARAMS, lp);

            }
            return (RelativeLayout.LayoutParams) cache.get(ANNOUNCE_ITEM_LAYOUT_PARAMS);
        }
    }

    public static RelativeLayout.LayoutParams getAnnounceRelativeParamsForOneCards() {
        synchronized (cache) {
            if (!cache.containsKey(ANNOUNCE_ITEM_LAYOUT_PARAMS_ONE_CARD)) {
                int width = AppUtils.isPortrait() ? App.getInstance().getScreenWidth() : App.getInstance().getScreenHeight();
                int itemWidth = width - AppUtils.dpToPx(16);
                int itemHeight = width / 2 + AppUtils.dpToPx(48);
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(itemWidth, itemHeight);
                cache.put(ANNOUNCE_ITEM_LAYOUT_PARAMS_ONE_CARD, lp);

            }
            return (RelativeLayout.LayoutParams) cache.get(ANNOUNCE_ITEM_LAYOUT_PARAMS_ONE_CARD);
        }
    }

    public static RelativeLayout.LayoutParams getReateAppRelativeParamsCards() {
        synchronized (cache) {
            if (!cache.containsKey(ANNOUNCE_ITEM_LAYOUT_PARAMS_RATE_APP)) {
                int width = AppUtils.isPortrait() ? App.getInstance().getScreenWidth() : App.getInstance().getScreenHeight();
                int itemHeight = width / 2 + AppUtils.dpToPx(16);
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT ,itemHeight);
                cache.put(ANNOUNCE_ITEM_LAYOUT_PARAMS_RATE_APP, lp);

            }
            return (RelativeLayout.LayoutParams) cache.get(ANNOUNCE_ITEM_LAYOUT_PARAMS_RATE_APP);
        }
    }
    public static RelativeLayout.LayoutParams getTopAnnounceRelativeParams() {
        synchronized (cache) {
            if (!cache.containsKey(ANNOUNCE_COLOR_FILTER_MARK_POSTS)) {
                ColorMatrix colorMatrix = new ColorMatrix();
                colorMatrix.setSaturation(0);
                cache.put(ANNOUNCE_COLOR_FILTER_MARK_POSTS, new ColorMatrixColorFilter(colorMatrix));
            }
            return (RelativeLayout.LayoutParams) cache.get(ANNOUNCE_COLOR_FILTER_MARK_POSTS);
        }
    }

    public static Float getTextSizePx() {
        synchronized (cache) {
            if (!cache.containsKey(TEXT_SIZE)) {
                cache.put(TEXT_SIZE, AppUtils.spToPx(ProfilePreferences.getInstance().getFontsSize(false)));
            }
            return (Float) cache.get(TEXT_SIZE);
        }
    }

    public static Float getTextSizeHugoPx() {
        synchronized (cache) {
            if (!cache.containsKey(TEXT_SIZE_HUGO)) {
                cache.put(TEXT_SIZE_HUGO, AppUtils.spToPx(ProfilePreferences.getInstance().getFontsSize(true)));
            }
            return (Float) cache.get(TEXT_SIZE_HUGO);
        }
    }

    public static Float getTextTitleSize(TextView view) {
        synchronized (cache) {
            if (!cache.containsKey(TITLE_TEXT_SIZE)) {
                float size = view.getTextSize() + AppUtils.spToPx(ProfilePreferences.getInstance().getFontsSize(false));
                cache.put(TITLE_TEXT_SIZE, size);
            }
            return (Float) cache.get(TITLE_TEXT_SIZE);
        }
    }


    public static Float getTextTitleTopSize(TextView view) {
        synchronized (cache) {
            if (!cache.containsKey(TITLE_TOP_TEXT_SIZE)) {
                float size = view.getTextSize() + AppUtils.spToPx(ProfilePreferences.getInstance().getFontsSize(true));
                cache.put(TITLE_TOP_TEXT_SIZE, size);
            }
            return (Float) cache.get(TITLE_TOP_TEXT_SIZE);
        }
    }

    public static Float getTextTitleEventSize(TextView view) {
        synchronized (cache) {
            if (!cache.containsKey(TITLE_EVENT_TEXT_SIZE)) {
                float size = view.getTextSize() + AppUtils.spToPx(ProfilePreferences.getInstance().getFontsSize(false));
                cache.put(TITLE_EVENT_TEXT_SIZE, size);
            }
            return (Float) cache.get(TITLE_EVENT_TEXT_SIZE);
        }
    }

    public static Float getTextTitleBookmarksSize(TextView view) {
        synchronized (cache) {
            if (!cache.containsKey(TITLE_BOOKMARKS_TEXT_SIZE)) {
                float size = view.getTextSize() + AppUtils.spToPx(ProfilePreferences.getInstance().getFontsSize(true));
                cache.put(TITLE_BOOKMARKS_TEXT_SIZE, size);
            }
            return (Float) cache.get(TITLE_BOOKMARKS_TEXT_SIZE);
        }
    }

    public static void cleanTitleSize() {
        synchronized (cache) {
            cache.remove(TEXT_SIZE);
            cache.remove(TEXT_SIZE_HUGO);
            cache.remove(TITLE_TOP_TEXT_SIZE);
            cache.remove(TITLE_TEXT_SIZE);
            cache.remove(TITLE_EVENT_TEXT_SIZE);
            cache.remove(TITLE_BOOKMARKS_TEXT_SIZE);
            cache.remove(ANNOUNCE_LINES_COUNT);
            cache.remove(ANNOUNCE_STYLE_LINES_COUNT);
        }
    }

    public static Boolean isNeedShowImages() {
        synchronized (cache) {
            if (!cache.containsKey(ANNOUNCE_NEED_SHOW_IMAGES)) {
                cache.put(ANNOUNCE_NEED_SHOW_IMAGES, ProfilePreferences.getInstance().isNeedShowImages());
            }
            return (Boolean) cache.get(ANNOUNCE_NEED_SHOW_IMAGES);
        }
    }

    public static void removeShowImages() {
        synchronized (cache) {
            cache.remove(ANNOUNCE_NEED_SHOW_IMAGES);
        }
    }

}
