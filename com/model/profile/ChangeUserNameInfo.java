package anews.com.model.profile;

import android.text.TextUtils;
import java.util.HashMap;
import anews.com.App;
import anews.com.model.profile.dto.UserProfileData;
import anews.com.network.ModelError;
import anews.com.network.SimpleModel;
import anews.com.preferences.ProfilePreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangeUserNameInfo extends SimpleModel<UserProfileData, Void> {

    public void editUserNameProfile(final String firstName, final String lastName) {
        startNewRequest();
        HashMap<String, String> params = new HashMap<>();
        params.put("first_name", firstName);
        if (!TextUtils.isEmpty(lastName)) {
            params.put("last_name", lastName);
        }
        getRestApi().changeUserProfile(params).enqueue(new Callback<UserProfileData>() {
            @Override
            public void onResponse(Call<UserProfileData> call, Response<UserProfileData> response) {
                if (response.isSuccessful() && response.body() != null) {
                    UserProfileData profileData = response.body();
                    if (profileData.getCode() == null) {
                        App.getInstance().initRetrofit();
                        ProfilePreferences.getInstance().saveFirstName(firstName);
                        ProfilePreferences.getInstance().saveLastName(lastName);
                        setData(response.body());
                    } else {
                        setError(ModelError.Unknown);
                    }
                } else {
                    setError(ModelError.Unknown);
                }
            }

            @Override
            public void onFailure(Call<UserProfileData> call, Throwable t) {
                setError(ModelError.Unknown);
            }
        });

    }
}
