package anews.com.model.profile;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKServiceActivity;
import com.vk.sdk.api.VKError;

import java.util.ArrayList;
import java.util.Arrays;

import anews.com.network.SimpleModel;

public class VKAuthInfo extends SimpleModel<Boolean, Void> {

    public static final String[] VK_SCOPE = new String[]{
            VKScope.NOTIFY,
            VKScope.FRIENDS,
            VKScope.PHOTOS,
            VKScope.AUDIO,
            VKScope.VIDEO,
            VKScope.PAGES,
            VKScope.NOTES,
            VKScope.WALL,
            VKScope.OFFLINE,
            VKScope.DOCS,
            VKScope.GROUPS,
            VKScope.NOTIFICATIONS
    };

    public void auth(Fragment fromFragment, Activity fromActivity) {
        Intent intent = new Intent(fromActivity, VKServiceActivity.class);
        intent.putExtra("arg1", "Authorization");
        intent.putStringArrayListExtra("arg2", new ArrayList<>(Arrays.asList(VK_SCOPE)));
        intent.putExtra("arg4", VKSdk.isCustomInitialize());
        fromFragment.startActivityForResult(intent, VKServiceActivity.VKServiceType.Authorization.getOuterCode());
    }

    public void onResult(int requestCode, int resultCode, @Nullable Intent data) {
        VKSdk.onActivityResult(requestCode, resultCode, data, mVkCallback);
    }

    private VKCallback<VKAccessToken> mVkCallback = new VKCallback<VKAccessToken>() {
        @Override
        public void onResult(VKAccessToken res) {
            setData(true);
        }

        @Override
        public void onError(VKError error) {
            setData(false);
        }
    };
}
