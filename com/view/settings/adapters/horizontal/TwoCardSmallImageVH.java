package anews.com.views.settings.adapters.horizontal;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import anews.com.R;
import anews.com.interfaces.OnSettingsSelectListener;
import anews.com.model.preferences.dto.AnnounceStyleType;
import anews.com.utils.AppUtils;
import anews.com.utils.StaticUIHelper;

public class TwoCardSmallImageVH extends AbsAnnounceStyleHorizontalVH implements View.OnClickListener {


    private LinearLayout mDescriptionContainer;
    private AnnounceStyleType mAnnounceStyleType;
    private OnSettingsSelectListener mOnSettingsSelectListener;

    public TwoCardSmallImageVH(View itemView, OnSettingsSelectListener listener) {
        super(itemView);
        mOnSettingsSelectListener = listener;
        itemView.setLayoutParams(StaticUIHelper.getAnnounceRelativeParams());
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        ImageView imageView = itemView.findViewById(R.id.image_view_announce);
        imageView.setBackgroundColor(itemView.getResources().getColor(R.color.announces_style_gray));
        params.addRule(RelativeLayout.ABOVE, R.id.container_text);
        params.bottomMargin = AppUtils.dpToPx(8);
        imageView.setLayoutParams(params);
        mDescriptionContainer = itemView.findViewById(R.id.container_description_text);
        mDescriptionContainer.setVisibility(View.GONE);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        mOnSettingsSelectListener.onSettingsSelectItem(mAnnounceStyleType);
    }

    @Override
    public void setData(AnnounceStyleType data) {
        mAnnounceStyleType = data;
    }
}