package anews.com.views.profile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import anews.com.R;
import anews.com.utils.AppFragmentActivity;
import anews.com.views.auth.ResetPasswordFragment;

public class ProfileActivity extends AppFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected Fragment getContentFragment(String tag) {
        if (ProfileFragment.TAG.equals(tag)) {
            return ProfileFragment.newInstance();
        } else if (ProfilePasswordFragment.TAG.equals(tag)) {
            return ProfilePasswordFragment.newInstance();
        } else if (ProfileNameFragment.TAG.equals(tag)) {
            return ProfileNameFragment.newInstance();
        } else if (CorpImageFragment.TAG.equals(tag)) {
            return CorpImageFragment.newInstance();
        } else if (ResetPasswordFragment.TAG.equals(tag)) {
            return ResetPasswordFragment.newInstance();
        } else {
            return ProfileFragment.newInstance();
        }
    }

    @Override
    public void onBackPressed() {
        if ((getSelectedTag().equals(ProfileNameFragment.TAG)) || getSelectedTag().equals(CorpImageFragment.TAG)) {
            showFragmentByTag(ProfileFragment.TAG);
        } else if (getSelectedTag().equals(ProfilePasswordFragment.TAG)) {
            showFragmentByTag(ProfileFragment.TAG);
        } else if (getSelectedTag().equals(ResetPasswordFragment.TAG)) {
            showFragmentByTag(ProfilePasswordFragment.TAG);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected int getFragmentContainerId() {
        return R.id.profile_container;
    }

    @Override
    protected String getDefaultFragmentTag() {
        return ProfileFragment.TAG;
    }
}
