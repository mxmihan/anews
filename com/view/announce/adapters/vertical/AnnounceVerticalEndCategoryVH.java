package anews.com.views.announce.adapters.vertical;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.lang.ref.WeakReference;

import anews.com.R;
import anews.com.interfaces.OnAnnouncesHelpActionListener;
import anews.com.model.announce.dto.AnnounceHelpVHType;
import anews.com.model.announce.dto.AnnounceVHItem;

public class AnnounceVerticalEndCategoryVH extends AbsAnnounceVerticalVH implements View.OnClickListener {
    private WeakReference<OnAnnouncesHelpActionListener> mOnAdapterClickListener;
    private TextView textViewTitle;
    private TextView textViewAction;

    public AnnounceVerticalEndCategoryVH(View itemView, OnAnnouncesHelpActionListener listener) {
        super(itemView);
        mOnAdapterClickListener = new WeakReference<>(listener);
        textViewTitle = itemView.findViewById(R.id.text_view_finish_title);
        textViewAction = itemView.findViewById(R.id.text_view_open_catalog);
        textViewAction.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        if (mOnAdapterClickListener.get() != null) {
            mOnAdapterClickListener.get().onAnnounceHelpAction(AnnounceHelpVHType.SHOW_CATALOG,getAdapterPosition());
        }
    }

    @Override
    public int getItemPosition() {
        return -1;
    }

    @Override
    public RecyclerView getRecyclerView() {
        return null;
    }

    @Override
    public void setData(AnnounceVHItem data) {
        textViewTitle.setText(R.string.str_finish_title);
        textViewAction.setText(R.string.str_open_catalog);
    }
}