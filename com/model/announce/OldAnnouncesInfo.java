package anews.com.model.announce;

import java.util.ArrayList;
import java.util.HashSet;

import anews.com.model.DBHelperFactory;
import anews.com.model.announce.dto.AnnounceDataType;
import anews.com.model.announce.dto.AnnounceDataWrapper;
import anews.com.model.announce.dto.AnnounceVHItem;
import anews.com.model.news.dto.PostData;
import anews.com.network.SimpleModel;
import anews.com.network.exceptions.PaginationThrowException;
import anews.com.preferences.ProfilePreferences;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import retrofit2.Call;
import retrofit2.Response;

public class OldAnnouncesInfo extends SimpleModel<AnnounceDataWrapper, Void> {

    private PublishSubject<AnnounceVHItem> mPublishSubject = PublishSubject.create();

    private HashSet<Integer> mRequestedPosition = new HashSet<>();

    public OldAnnouncesInfo() {
        mPutObservable.subscribe(mResponseObserver);
    }

    private Observable<AnnounceVHItem> mPutObservable = Observable.<AnnounceVHItem>empty()
            .mergeWith(mPublishSubject)
            .concatMap(new Function<AnnounceVHItem, ObservableSource<AnnounceVHItem>>() {
                @Override
                public ObservableSource<AnnounceVHItem> apply(AnnounceVHItem o) throws Exception {
                    return Observable.just(o)
                            .subscribeOn(Schedulers.io())
                            .map(new Function<AnnounceVHItem, AnnounceVHItem>() {
                                @Override
                                public AnnounceVHItem apply(AnnounceVHItem announceVHItem) {
                                    try {
                                        int maxId = announceVHItem.getMaxId();
                                        Call<ArrayList<PostData>> request = null;
                                        if (announceVHItem.getCategorySourceData().isTop()) {
                                            request = getRestApi().getOldTop(ProfilePreferences.getInstance().getRegion(), maxId);
                                        } else if (announceVHItem.getCategorySourceData().isFeed()) {
                                            request = getRestApi().getOldPostsByFeedId(announceVHItem.getCategorySourceData().getSourceId(), maxId);
                                        } else if (announceVHItem.getCategorySourceData().isStream()) {
                                            request = getRestApi().getOldPostsByStreamId(announceVHItem.getCategorySourceData().getSourceId(), maxId);
                                        }

                                        if (request != null) {
                                            Response<ArrayList<PostData>> response = request.execute();
                                            if (response.isSuccessful() && response.body() != null) {
                                                if (response.body().size() == 0) {
                                                    return announceVHItem.setNoMorePosts();
                                                }
                                                if (announceVHItem.getCategorySourceData().isTop()) {
                                                    DBHelperFactory.getHelper().getPostDataDao().addOldPostsFromTop(response.body());
                                                    return announceVHItem;
                                                } else if (announceVHItem.getCategorySourceData().isFeed()) {
                                                    DBHelperFactory.getHelper().getPostDataDao().addPostsFromFeed(response.body());
                                                    return announceVHItem;
                                                } else if (announceVHItem.getCategorySourceData().isStream()) {
                                                    DBHelperFactory.getHelper().getPostDataDao().addPostsFromStream(response.body(), announceVHItem.getCategorySourceData().getSourceId());
                                                    return announceVHItem;
                                                }

                                            }
                                        }

                                    } catch (Exception ignored) {

                                    }
                                    throw new PaginationThrowException(announceVHItem.getAdapterPosition());
                                }
                            })
                            .onErrorResumeNext(new Function<Throwable, ObservableSource<? extends AnnounceVHItem>>() {
                                @Override
                                public ObservableSource<? extends AnnounceVHItem> apply
                                        (Throwable throwable) throws Exception {
                                    mResponseObserver.onError(throwable);
                                    return Observable.empty();
                                }
                            })
                            .observeOn(AndroidSchedulers.mainThread());

                }
            });

    private Observer<AnnounceVHItem> mResponseObserver = new Observer<AnnounceVHItem>() {

        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onNext(AnnounceVHItem announceVHItem) {
            mRequestedPosition.remove(announceVHItem.getAdapterPosition());
            setOnNextData(new AnnounceDataWrapper(AnnounceDataType.ON_NEXT, announceVHItem));
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            if (e instanceof PaginationThrowException) {
                mRequestedPosition.remove(((PaginationThrowException) e).getPosition());
            }
        }

        @Override
        public void onComplete() {

        }
    };

    public void getOldPosts(AnnounceVHItem announceVHItem) {
        if (!mRequestedPosition.contains(announceVHItem.getAdapterPosition())) {
            mRequestedPosition.add(announceVHItem.getAdapterPosition());
            mPublishSubject.onNext(announceVHItem);
        }
    }

//    public void updateCursor(AnnounceVHItem announceVHItem) {
//        if (announceVHItem.getCategorySourceData().isTop()) {
//            DBHelperFactory.getHelper().getTopPostsIdsDao().wrapNewCursor(announceVHItem);
//        } else if (announceVHItem.getCategorySourceData().isFeed()) {
//            DBHelperFactory.getHelper().getFeedPostsIds().wrapNewCursor(announceVHItem);
//        } else if (announceVHItem.getCategorySourceData().isStream()) {
//            DBHelperFactory.getHelper().getStreamPostsIds().wrapNewCursor(announceVHItem);
//        }
//    }

}
