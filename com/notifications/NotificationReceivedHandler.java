package anews.com.notifications;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import java.sql.SQLException;
import java.util.ArrayList;

import anews.com.App;
import anews.com.model.DBHelperFactory;
import anews.com.model.events.dto.EventPushData;
import anews.com.model.events.dto.EventPushType;
import anews.com.model.news.dto.PostData;
import anews.com.views.events.EventsFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Automatic OneSignal push notification Receiver.
 * What it does?
 * 1. receives push notification
 * 2. Creates EventPushData object from it and puts it in the corresponding table inside database via EventPushDataDao
 * 3. Sends GET request to API to get all info about the post as PostData object
 * 4. Updates PostData in the corresponding table inside database via PostDataDao
 */

public class NotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {

    @Override
    public void notificationReceived(OSNotification notification) {
        if (notification == null || notification.payload == null) {
            return;
        }

        final EventPushData eventPushData = new Gson().fromJson(notification.payload.additionalData.toString(), EventPushData.class);
        try {
            eventPushData.setPostData(PostData.createEmptyPost(eventPushData.getPostId()));
            DBHelperFactory.getHelper().getEventPushDataDao().createOrUpdate(eventPushData);
            App.getInstance().getRestApi().getPostDataById(eventPushData.getPostId()).enqueue(new Callback<PostData>() {
                @Override
                public void onResponse(Call<PostData> call, Response<PostData> response) {
                    ArrayList<PostData> listData = new ArrayList<>();
                    listData.add(response.body());
                    if (eventPushData.getType() == null) {
                        eventPushData.setType(EventPushType.TOP);
                    }
                    switch (eventPushData.getType()) {
                        case TOP:
                            DBHelperFactory.getHelper().getPostDataDao().addPostsFromTopPush(listData);
                            break;
                        case FEED:
                            DBHelperFactory.getHelper().getPostDataDao().addPostsFromFeed(listData);
                            break;
                        case STREAM:
                            DBHelperFactory.getHelper().getPostDataDao().addPostsFromStream(listData, 0);
                            break;
                        default:
                            DBHelperFactory.getHelper().getPostDataDao().addPostsFromTopPush(listData);
                            break;
                    }
                    Intent intent = new Intent();
                    intent.setAction(EventsFragment.EVENT_BROADCAST_MESSAGE);
                    LocalBroadcastManager.getInstance(App.getInstance()).sendBroadcast(intent);
                }

                @Override
                public void onFailure(Call<PostData> call, Throwable t) {

                }
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
