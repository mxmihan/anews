package anews.com.preferences;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

import anews.com.model.preferences.dto.AnnounceStyleType;
import anews.com.model.preferences.dto.FontsSize;
import anews.com.model.profile.dto.UserProfileData;
import anews.com.utils.AppUtils;

public class ProfilePreferences extends AbsPreferences {

    public static final String PROFILE_PREFERENCES_FILE = "profile_preferences";
    private static final String ID = "id";
    private static final String STARED = "stared";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String EMAIL = "email";
    private static final String AVATAR = "avatar";
    public static final String REGION = "region";

    public static final String SHOW_IMAGES = "show_images";
    private static final String HIDE_COMMENTS = "hide_comments";
    private static final String SHOW_PUSH = "show_push";

    private static final String PROVIDERS = "providers";
    public static final String SELECT_COUNTRY = "select_country";
    public static final String SHOW_COMMENTS = "show_comments";
    public static final String EXTERNAL_BROWSER = "external_browser";
    private static final String SUBSCRIPTIONS_SOURCES = "subscriptions_sources";
    private static final String SUBSCRIPTIONS_FEEDS = "subscriptions_feeds";
    private static final String SUBSCRIPTIONS_STREAMS = "subscriptions_streams";
    private static final String MODERATOR = "moderator";
    private static final String CODE = "code";
    private static final String ERROR = "error";
    private static final String LAST_SORT_TS = "last_sort_ts";
    private static final String LAST_SYNC_TS = "last_sync_ts";
    private static final String IS_NEED_FIRST_SUBSCRIBE = "is_need_first_subscribe";
    private static final String IS_NEED_LOAD_SUBSCRIBE = "is_need_load_subscribe";
    private static final String IS_NEED_SYNC_SUBSCRIBE = "is_need_sync_subscribe";
    public static final String IS_NEED_SHOW_TOP = "is_need_show_top";
    public static final String IS_NEED_MARK_READ_NEWS = "is_need_mark_read_news";

    public static final String FONT_SIZE = "font_size";
    public static final String STYLE_ANNOUNCE = "style_announce";
    public static final String USER_IS_NEED_SHOW_COMMENTS = "user_show_comments";
    private static final String IS_EMAIL_PROVIDER = "is_email_provider";
    private volatile static ProfilePreferences mInstance;

    private ProfilePreferences(Context context) {
        super(context, PROFILE_PREFERENCES_FILE);
    }

    public static void initialize(Context ctx) {
        if (mInstance == null) {
            synchronized (ProfilePreferences.class) {
                if (mInstance == null) {
                    mInstance = new ProfilePreferences(ctx);
                }
            }
        }
    }

    public static ProfilePreferences getInstance() {
        if (mInstance == null) {
            throw new IllegalStateException("Must Initialize ProfilePreferences before using getInstance()");
        } else {
            return mInstance;
        }
    }

    public void saveId(int id) {
        saveIntPrefs(ID, id);
    }

    public int getId() {
        return getIntPrefs(ID);
    }

    public void saveStared(List<Integer> stared) {
        Gson gson = new Gson();
        String jsonStared = gson.toJson(stared);
        saveStringPrefs(STARED, jsonStared);
    }

    public void saveStaredWithGson(List<Integer> stared, Gson gson) {
        String jsonStared = gson.toJson(stared);
        saveStringPrefs(STARED, jsonStared);
    }

    public List<Integer> getStared() {
        Gson gson = new Gson();
        String jsonStared = getStringPrefs(STARED);
        Integer[] ints = gson.fromJson(jsonStared, Integer[].class);
        return Arrays.asList(ints);
    }


    public void saveFirstName(String firstName) {
        saveStringPrefs(FIRST_NAME, firstName);
    }

    public String getFirstName() {
        return getStringPrefs(FIRST_NAME);
    }

    public void saveLastName(String lastName) {
        saveStringPrefs(LAST_NAME, lastName);
    }

    public String getLastName() {
        return getStringPrefs(LAST_NAME);
    }

    public void saveEmail(String email) {
        saveStringPrefs(EMAIL, email);
    }

    public String getEmail() {
        return getStringPrefs(EMAIL);
    }

    public void saveAvatar(String avatar) {
        saveStringPrefs(AVATAR, avatar);
    }

    public String getAvatar() {
        return getStringPrefs(AVATAR);
    }

    public void saveRegion(String region) {
        saveStringPrefs(REGION, region);
    }

    public String getRegion() {
        return getStringPrefs(REGION);
    }

    public void saveProviders(List<String> providers) {
        Gson gson = new Gson();
        String jsonProviders = gson.toJson(providers);
        saveStringPrefs(PROVIDERS, jsonProviders);
    }

    public void saveProvidersWithGson(List<String> providers, Gson gson) {
        String jsonProviders = gson.toJson(providers);
        saveStringPrefs(PROVIDERS, jsonProviders);
    }

    public List<String> getProviders() {
        Gson gson = new Gson();
        String jsonProviders = getStringPrefs(PROVIDERS);
        String[] strings = gson.fromJson(jsonProviders, String[].class);
        return Arrays.asList(strings);
    }

//    public void saveSubscriptions(UserSubscriptionsData subscriptions) {
//        Gson gson = new Gson();
//
//        String jsonSources = gson.toJson(subscriptions.getSources());
//        saveStringPrefs(SUBSCRIPTIONS_SOURCES, jsonSources);
//
//        String jsonFeeds = gson.toJson(subscriptions.getFeeds());
//        saveStringPrefs(SUBSCRIPTIONS_FEEDS, jsonFeeds);
//
//        String jsonStreams = gson.toJson(subscriptions.getStreams());
//        saveStringPrefs(SUBSCRIPTIONS_STREAMS, jsonStreams);
//    }

//    public void saveSubscriptionsWithGson(UserSubscriptionsData subscriptions, Gson gson) {
//        String jsonSources = gson.toJson(subscriptions.getSources());
//        saveStringPrefs(SUBSCRIPTIONS_SOURCES, jsonSources);
//
//        String jsonFeeds = gson.toJson(subscriptions.getFeeds());
//        saveStringPrefs(SUBSCRIPTIONS_FEEDS, jsonFeeds);
//
//        String jsonStreams = gson.toJson(subscriptions.getStreams());
//        saveStringPrefs(SUBSCRIPTIONS_STREAMS, jsonStreams);
//    }

//    public UserSubscriptionsData getSubscriptions() {
//        UserSubscriptionsData data = new UserSubscriptionsData();
//        Gson gson = new Gson();
//
//        String jsonSources = getStringPrefs(SUBSCRIPTIONS_SOURCES);
//        String jsonFeeds = getStringPrefs(SUBSCRIPTIONS_FEEDS);
//        String jsonStreams = getStringPrefs(SUBSCRIPTIONS_STREAMS);
//
//        Integer[] ints = gson.fromJson(jsonSources, Integer[].class);
//        List<Integer> list = Arrays.asList(ints);
//        data.setSources(list);
//
//        ints = gson.fromJson(jsonFeeds, Integer[].class);
//        list = Arrays.asList(ints);
//        data.setFeeds(list);
//
//        ints = gson.fromJson(jsonStreams, Integer[].class);
//        list = Arrays.asList(ints);
//        data.setStreams(list);
//
//        return data;
//    }

    public void saveModerator(boolean moderator) {
        saveBooleanPrefs(MODERATOR, moderator);
    }

    public boolean isModerator() {
        return getBooleanPrefs(MODERATOR);
    }

    public void saveCode(Integer code) {
        if (code != null) {
            saveIntPrefs(CODE, code);
        }
    }

    public Integer getCode() {
        return getIntPrefs(CODE);
    }

    public void saveError(String error) {
        saveStringPrefs(ERROR, error);
    }

    public String getError() {
        return getStringPrefs(ERROR);
    }

    public void saveUserProfileData(UserProfileData data) {

        Gson gson = new Gson();

        saveId(data.getId());
        saveStaredWithGson(data.getStared(), gson);
        saveFirstName(data.getFirstName());
        saveLastName(data.getLastName());
        saveEmail(data.getEmail());
        String avatar = data.getAvatar();
        if (!TextUtils.isEmpty(avatar)) {
            saveAvatar(AppUtils.getNormalizationLink(avatar));
        }
        saveRegion(data.getRegion());
        saveProvidersWithGson(data.getProviders(), gson);
//        saveSubscriptionsWithGson(data.getSubscriptions(), gson);
        saveModerator(data.isModerator());
        saveCode(data.getCode());
        saveError(data.getError());
    }

    public boolean getUserShowComments() {
        return getBooleanPrefs(USER_IS_NEED_SHOW_COMMENTS);
    }

    public void setIsNeedFirstSubscribe(boolean firstSubscribe) {
        saveBooleanPrefs(IS_NEED_FIRST_SUBSCRIBE, firstSubscribe);
    }

    public boolean isNeedFirstSubscribe() {
        return getBooleanPrefs(IS_NEED_FIRST_SUBSCRIBE);
    }

    public void setIsNeedLoadSubscribe(boolean needSync) {
        saveBooleanPrefs(IS_NEED_LOAD_SUBSCRIBE, needSync);
    }

    public boolean isNeedLoadSubscribe() {
        return getBooleanPrefs(IS_NEED_LOAD_SUBSCRIBE);
    }

    public void setIsNeedSyncSubscribe(boolean needSync) {
        saveBooleanPrefs(IS_NEED_SYNC_SUBSCRIBE, needSync);
    }

    public boolean isNeedSyncSubscribe() {
        return getBooleanPrefs(IS_NEED_SYNC_SUBSCRIBE);
    }


    public void setVisibleComments(boolean isVisible) {
        saveBooleanPrefs(SHOW_COMMENTS, isVisible);
    }

    public boolean isCommentsVisible() {
        return getPreferences().getBoolean(SHOW_COMMENTS, true);
    }

    public void setIsNeedShowInExternalBrowser(boolean isNeedShowInExternalBrowser) {
        saveBooleanPrefs(EXTERNAL_BROWSER, isNeedShowInExternalBrowser);
    }

    public boolean isNeedShowInExternalBrowser() {
        return getBooleanPrefs(EXTERNAL_BROWSER);
    }

    public boolean isNeedMarkReadNews() {
        return getBooleanPrefs(IS_NEED_MARK_READ_NEWS, true);
    }

    public void setLastSort(long ts) {
        saveLongPrefs(LAST_SORT_TS, ts);
    }


    public int getFontsSize(boolean isTopView) {
        return FontsSize.getTypeSize(getStringPrefs(FONT_SIZE, FontsSize.NORMAL.name()), isTopView);
    }

    public FontsSize getFontsSize() {
        return FontsSize.getFontsSize(getStringPrefs(FONT_SIZE));
    }

    public AnnounceStyleType getStyleAnnounce() {
        return AnnounceStyleType.getStyle(getStringPrefs(STYLE_ANNOUNCE, AnnounceStyleType.TWO_CARD_SMALL_IMAGE.name()));
    }
    public void setStyleAnnounce(AnnounceStyleType announceStyleType) {
        saveStringPrefs(STYLE_ANNOUNCE, announceStyleType.name());
    }


    public long getLastSort() {
        return getLongPrefs(LAST_SORT_TS);
    }

    public void setLastSync(long ts) {
        saveLongPrefs(LAST_SYNC_TS, ts);
    }

    public long getLastSync() {
        return getLongPrefs(LAST_SYNC_TS);
    }

    public void setShowImages(boolean isNeedShowed) {
        saveBooleanPrefs(SHOW_IMAGES, isNeedShowed);
    }


    public void setShowPush(boolean isNeedShowed) {
        saveBooleanPrefs(SHOW_PUSH, isNeedShowed);
    }

    public boolean isNeedShowImages() {
        return getBooleanPrefs(SHOW_IMAGES, true);
    }

    public boolean isNeedShowPush() {
        return getBooleanPrefs(SHOW_PUSH, true);
    }

    public boolean isNeedShowTop() {
        return getBooleanPrefs(IS_NEED_SHOW_TOP, true);
    }

    public void setIsEmailProvider(boolean isEmailProvider) {
        saveBooleanPrefs(IS_EMAIL_PROVIDER, isEmailProvider);
    }

    public boolean isEmailProvider() {
        return getBooleanPrefs(IS_EMAIL_PROVIDER, true);
    }




}
