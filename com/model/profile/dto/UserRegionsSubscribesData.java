package anews.com.model.profile.dto;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class UserRegionsSubscribesData {

    private LinkedHashMap<String, ArrayList<UserSourceSubscribesData>> sources;

    UserRegionsSubscribesData(LinkedHashMap<String, ArrayList<UserSourceSubscribesData>> sources) {
        this.sources = sources;
    }

    UserRegionsSubscribesData() {
    }

    public LinkedHashMap<String, ArrayList<UserSourceSubscribesData>> getSources() {
        return sources;
    }
}
