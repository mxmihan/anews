package anews.com.views.events.adapters;

import android.view.View;

import anews.com.model.events.dto.EventPushData;
import anews.com.utils.adapters.AbsSwipeableVH;

public class EventsEmptyVHItem extends AbsSwipeableVH<EventPushData> {
    private EventPushData eventPushData;

    public EventsEmptyVHItem(View itemView) {
        super(itemView);

    }

    public void setData(EventPushData eventPushData) {
        this.eventPushData = eventPushData;
    }

    @Override
    public boolean isSwipeable() {
        return true;
    }

    @Override
    public EventPushData getData() {
        return eventPushData;
    }


}