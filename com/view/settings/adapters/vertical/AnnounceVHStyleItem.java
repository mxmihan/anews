package anews.com.views.settings.adapters.vertical;

import anews.com.model.preferences.dto.AnnounceStyleType;

public class AnnounceVHStyleItem {
    private AnnounceStyleType announceStyleType;

    public AnnounceVHStyleItem(AnnounceStyleType announceStyleType) {
        this.announceStyleType = announceStyleType;
    }
    public AnnounceStyleType getAnnounceStyleType() {
        return announceStyleType;
    }

}