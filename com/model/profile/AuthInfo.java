package anews.com.model.profile;

import android.text.TextUtils;

import com.facebook.AccessToken;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterSession;
import com.vk.sdk.VKAccessToken;

import java.util.HashMap;
import java.util.Map;

import anews.com.App;
import anews.com.analytic.Analytics;
import anews.com.model.profile.dto.UserProfileData;
import anews.com.network.ModelError;
import anews.com.network.SimpleModel;
import anews.com.preferences.ProfilePreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthInfo extends SimpleModel<UserProfileData, Void> {

    public static final String VK = "vk-oauth2";
    public static final String GOOGLE = "google-oauth2";

    public void authByPassword(String username, String password) {
        startNewRequest();
        final HashMap<String, String> params = new HashMap<>();
        params.put("username", username);
        params.put("password", password);
        getRestApi().authByPassword(params).enqueue(new Callback<UserProfileData>() {
            @Override
            public void onResponse(Call<UserProfileData> call, Response<UserProfileData> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getCode() == null) {
                    App.getInstance().initRetrofit();
                    ProfilePreferences.getInstance().saveUserProfileData(response.body());
                    ProfilePreferences.getInstance().setIsNeedFirstSubscribe(false);
                    ProfilePreferences.getInstance().setIsEmailProvider(true);
                    setData(response.body());
                    sendAnalytics(Analytics.ACTION_AUTH_EMAIL);
                } else {
                    setError(ModelError.AuthError);
                }
            }

            @Override
            public void onFailure(Call<UserProfileData> call, Throwable t) {
                setError(ModelError.Unknown);
            }
        });

    }

    public void authByFaceBook() {
        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", AccessToken.getCurrentAccessToken().getToken());
        authByToken(params, "facebook");

    }

    public void authByVk() {
        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", VKAccessToken.currentToken().accessToken);
        params.put("user_id", VKAccessToken.currentToken().userId);
        authByToken(params, "vk-oauth2");
    }

    public void authByGoogle(String token) {
        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", token);
        authByToken(params, "google-oauth2");
    }

    public void authByTwitter() {
        TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
        TwitterAuthToken authToken = session.getAuthToken();
        String token = "oauth_token_secret=" + authToken.secret + "&oauth_token=" + authToken.token;
        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", token);
        authByToken(params, "twitter");

    }

    private void authByToken(Map<String, String> token, final String provider) {
        startNewRequest();
        getRestApi().authByToken(provider, token).enqueue(new Callback<UserProfileData>() {
            @Override
            public void onResponse(Call<UserProfileData> call, Response<UserProfileData> response) {
                if (response.isSuccessful()) {
                    App.getInstance().initRetrofit();
                    ProfilePreferences.getInstance().saveUserProfileData(response.body());
                    ProfilePreferences.getInstance().setIsNeedFirstSubscribe(response.body().getSubscriptions().isEmpty());
                    ProfilePreferences.getInstance().setIsEmailProvider(false);
                    setData(response.body());
                    sendAnalytics(provider);
                } else {
                    setError(ModelError.AuthError);
                }
            }

            @Override
            public void onFailure(Call<UserProfileData> call, Throwable t) {
                setError(ModelError.Unknown);
            }
        });
    }

    public void sendAnalytics(final String provider) {
        String value = null;
        switch (provider) {
            case Analytics.ACTION_AUTH_FACEBOOK:
                value = Analytics.ACTION_AUTH_FACEBOOK;
                break;
            case Analytics.ACTION_AUTH_TWITTER:
                value = Analytics.ACTION_AUTH_TWITTER;
                break;
            case VK:
                value = Analytics.ACTION_AUTH_VK;
                break;
            case GOOGLE:
                value = Analytics.ACTION_AUTH_GOOGLE;
                break;
            case Analytics.ACTION_AUTH_EMAIL:
                value = Analytics.ACTION_AUTH_EMAIL;
                break;
        }
        if (!TextUtils.isEmpty(value)) {
            Analytics.trackEvent(App.getInstance(), Analytics.ACTION_AUTHORIZATION, Analytics.CATEGORY_ACTIVITY, Analytics.ACTION_AUTHORIZATION, value);
        }
    }

}
