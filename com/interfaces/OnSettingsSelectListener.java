package anews.com.interfaces;

import anews.com.model.preferences.dto.AnnounceStyleType;

public interface OnSettingsSelectListener {

    void onSettingsSelectItem(AnnounceStyleType announceStyleType);
}
