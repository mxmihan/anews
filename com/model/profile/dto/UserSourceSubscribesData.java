package anews.com.model.profile.dto;

import java.util.ArrayList;

import anews.com.model.news.dto.FeedData;
import anews.com.model.news.dto.StreamData;

public class UserSourceSubscribesData {

    private int count;
    private int id;
    private boolean top;
    private String title;
    private String img;
    private ArrayList<StreamData> streams;
    private ArrayList<FeedData> feeds;
    private int pos = 0;


    public int getCount() {
        return count;
    }

    public int getId() {
        return id;
    }

    public boolean isTop() {
        return top;
    }

    public String getTitle() {
        return title;
    }

    public String getImg() {
        return img;
    }

    public int getPos() {
        return pos;
    }

    public ArrayList<StreamData> getStreams() {
        return streams;
    }

    public ArrayList<FeedData> getFeeds() {
        return feeds;
    }
}
