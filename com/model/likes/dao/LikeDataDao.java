﻿package anews.com.model.likes.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import anews.com.model.likes.dto.LikeData;
import anews.com.model.likes.dto.LikeState;

public class LikeDataDao extends BaseDaoImpl<LikeData, Integer> implements Dao<LikeData, Integer> {
    private static final long SERVER_CACHE_TIME = 5 * 60 * 1000;

    public LikeDataDao(ConnectionSource connectionSource, Class<LikeData> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public boolean isLikedDB(int postId) {
        try {
            LikeData likeData = queryForId(postId);
            if (likeData != null) {
                return likeData.getLikeState() == LikeState.LIKE;
            } else {
                return false;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    public void addLikedMetaInfo(final int postId, final boolean isLiked) {
        try {
            LikeData likeDataList = queryForId(postId);
            if (likeDataList == null) {
                if (isLiked) {
                    setLike(postId, isLiked);
                }
            } else {
                if (System.currentTimeMillis() - likeDataList.getLikeTime() > SERVER_CACHE_TIME) {
                               setLike(postId, isLiked);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setLike(int postId, boolean isLiked) {
        try {
            createOrUpdate(new LikeData(postId, System.currentTimeMillis(), isLiked ? LikeState.LIKE : LikeState.UNLIKE));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
