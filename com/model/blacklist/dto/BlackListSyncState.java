package anews.com.model.blacklist.dto;

public enum BlackListSyncState {
    NEW,
    SYNCHRONIZED,
    NEED_SYNCHRONIZED,
    DELETE,
    DELETED
}
