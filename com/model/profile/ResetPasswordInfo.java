package anews.com.model.profile;

import java.util.HashMap;

import anews.com.App;
import anews.com.analytic.Analytics;
import anews.com.model.profile.dto.ResetPasswordResponse;
import anews.com.network.ModelError;
import anews.com.network.SimpleModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordInfo extends SimpleModel<ResetPasswordResponse, Void> {

    public void resetPassword(String email) {
        startNewRequest();
        HashMap<String, String> params = new HashMap<>();
        params.put("email", email);
        getRestApi().resetPassword(params).enqueue(new Callback<ResetPasswordResponse>() {
            @Override
            public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {
                if (response.isSuccessful() && response.body() != null && "ok".equals(response.body().getStatus())) {
                    setData(response.body());
                    Analytics.trackEvent(App.getInstance(), Analytics.ACTION_AUTH_RESET_PASSWORD, Analytics.CATEGORY_ACTIVITY, Analytics.ACTION_AUTH_RESET_PASSWORD);
                } else {
                    setError(ModelError.Unknown);
                }
            }

            @Override
            public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {
                setError(t);
            }
        });

    }
}
