package anews.com.views.settings.adapters.vertical;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import anews.com.R;
import anews.com.interfaces.OnSettingsSelectListener;
import anews.com.model.preferences.dto.AnnounceStyleType;
import anews.com.preferences.ProfilePreferences;
import anews.com.utils.ui.HorizontalLinearLayoutManager;
import anews.com.views.settings.adapters.horizontal.AnnounceStyleHorizontalAdapter;

public class AnnounceStyleVerticalVH extends AbsAnnounceStyleVerticalVH implements View.OnClickListener {
    private int mAdapterPosition;
    private TextView mHederTitle;
    private TextView mSummaryTitle;
    private RadioButton mRadioButton;
    private RecyclerView mRecyclerView;
    private OnSettingsSelectListener mOnSettingsSelectListener;
    private AnnounceStyleType mAnnounceStyleType;
    private AnnounceStyleHorizontalAdapter mStyleHorizontalAdapter;


    public AnnounceStyleVerticalVH(@NonNull View itemView, OnSettingsSelectListener listenerSelect) {
        super(itemView);
        mOnSettingsSelectListener = listenerSelect;
        mHederTitle = itemView.findViewById(R.id.text_view_header_title);
        mSummaryTitle = itemView.findViewById(R.id.text_view_summary_title);
        mRadioButton = itemView.findViewById(R.id.radio_button);
        mRecyclerView = itemView.findViewById(R.id.recycler_view_horizontal);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new HorizontalLinearLayoutManager(itemView.getContext(), LinearLayoutManager.HORIZONTAL, false));
        mRecyclerView.setNestedScrollingEnabled(true);
        mRadioButton.setOnClickListener(this);
        itemView.setOnClickListener(this);

    }

    @Override
    public void setData(AnnounceVHStyleItem data) {
        mAnnounceStyleType = data.getAnnounceStyleType();
        mAdapterPosition = data.getAnnounceStyleType().ordinal();
        mHederTitle.setText(data.getAnnounceStyleType().getTitle());
        mSummaryTitle.setText(data.getAnnounceStyleType().getDescription());
        mStyleHorizontalAdapter = new AnnounceStyleHorizontalAdapter(data.getAnnounceStyleType(), mOnSettingsSelectListener);
        mRecyclerView.setAdapter(mStyleHorizontalAdapter);
        mRadioButton.setChecked(ProfilePreferences.getInstance().getStyleAnnounce() == data.getAnnounceStyleType());
    }

    @Override
    public int getItemPosition() {
        return mAdapterPosition;
    }

    @Override
    public void onClick(View view) {
        mOnSettingsSelectListener.onSettingsSelectItem(mAnnounceStyleType);
    }


}
