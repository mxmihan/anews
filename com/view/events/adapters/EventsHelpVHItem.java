package anews.com.views.events.adapters;


import android.view.View;

import anews.com.R;
import anews.com.interfaces.OnHelpClickListener;
import anews.com.model.events.dto.EventPushData;
import anews.com.utils.adapters.AbsSwipeableVH;

public class EventsHelpVHItem extends AbsSwipeableVH<EventPushData> implements View.OnClickListener {
    private OnHelpClickListener listener;

    public EventsHelpVHItem(View itemView, OnHelpClickListener listener) {
        super(itemView);
        this.listener = listener;
        itemView.findViewById(R.id.text_view_help_ok).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        listener.onHideHelp();
    }

    @Override
    public boolean isSwipeable() {
        return false;
    }

    @Override
    public EventPushData getData() {
        return null;
    }
}